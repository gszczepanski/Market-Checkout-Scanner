package szczepanski.gerard.market.checkout.domain.basket;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.domain.promotion.BoughtTogetherPromotionRepository;
import szczepanski.gerard.market.checkout.domain.promotion.DiscountFromTotalBasketPriceRepository;
import szczepanski.gerard.market.checkout.domain.promotion.ItemQuantityPromotionRepository;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class BasketFactoryTest {

    ItemQuantityPromotionRepository itemQuantityPromotionRepository;
    BoughtTogetherPromotionRepository boughtTogetherPromotionRepository;
    DiscountFromTotalBasketPriceRepository discountFromTotalBasketPriceRepository;
    BasketFactory basketFactory;

    @BeforeMethod
    public void beforeMethod() {
        itemQuantityPromotionRepository = mock(ItemQuantityPromotionRepository.class);
        boughtTogetherPromotionRepository = mock(BoughtTogetherPromotionRepository.class);
        discountFromTotalBasketPriceRepository = mock(DiscountFromTotalBasketPriceRepository.class);

        basketFactory = new BasketFactory(itemQuantityPromotionRepository, boughtTogetherPromotionRepository, discountFromTotalBasketPriceRepository);
    }

    @Test
    public void createValidBasket() {
        // Act
        Basket checkout = basketFactory.create();

        // Assert
        assertNotNull(checkout.getCode());

        verify(itemQuantityPromotionRepository).findAllActivePromotions();
        verify(boughtTogetherPromotionRepository).findAllActivePromotions();
        verify(discountFromTotalBasketPriceRepository).findAllActiveDiscounts();
    }

}
