package szczepanski.gerard.market.checkout.domain.basket;

import org.testng.Assert;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;

import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class RecipeTest {

    @Test
    public void whenRecipeLinesAreCorrectThenBuildRecipe() {
        // Arrange
        String firstLine = "RECIPE 0978";
        String secondLine = "Milk 13.99 USD";
        String thirdLine = "---------------";
        String fourthLine = "Total: 13.99 USD";

        // Act
        Recipe recipe = Recipe.BasketRecipeBuilder.recipe()
                .line(firstLine)
                .line(secondLine)
                .line(thirdLine)
                .line(fourthLine)
                .print();

        // Assert
        String recipeText = recipe.getRecipeText();

        assertTrue(recipeText.contains(firstLine));
        assertTrue(recipeText.contains(secondLine));
        assertTrue(recipeText.contains(thirdLine));
        assertTrue(recipeText.contains(fourthLine));
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenAnyOfTheLineIsNullThenTrowParamAssertionException() {
        // Arrange
        String NULL_LINE = null;

        // Act
        Recipe.BasketRecipeBuilder.recipe()
                .line(NULL_LINE)
                .print();
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenAnyOfTheLineIsBlankThenTrowParamAssertionException() {
        // Arrange
        String BLANK_LINE = "";

        // Act
        Recipe.BasketRecipeBuilder.recipe()
                .line(BLANK_LINE)
                .print();
    }

}
