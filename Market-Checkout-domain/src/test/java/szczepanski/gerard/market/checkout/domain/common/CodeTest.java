package szczepanski.gerard.market.checkout.domain.common;

import org.testng.Assert;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;

import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class CodeTest {

    @Test
    public void whenStringValueOfCodeIsValidDuringCodeCreationThenCreateCode() {
        // Arrange
        String rawCode = "ITEM-0001";

        // Act
        Code code = Code.of(rawCode);

        // Assert
        assertEquals(code.toString(), rawCode);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenStringValueOfCodeIsNullThenThrowParamAssertionException() {
        // Arrange
        String NULL_CODE = null;

        // Act
        Code.of(NULL_CODE);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenStringValueOfCodeIsBlankThenThrowParamAssertionException() {
        // Arrange
        String BLANK_CODE = "";

        // Act
        Code.of(BLANK_CODE);
    }

}
