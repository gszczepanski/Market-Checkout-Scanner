package szczepanski.gerard.market.checkout.domain.promotion;

import org.testng.Assert;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;

import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class DiscountFromTotalBasketPriceTest {

    @Test
    public void whenDiscountConditionsMetWithHigherValueThanExpectedThenReturnTrue() {
        // Arrange
        MonetaryValue requiredBasketTotalValue = MonetaryValue.of("600.00", Currency.USD);

        MonetaryValue higherTotalValue = MonetaryValue.of("601.00", Currency.USD);

        DiscountFromTotalBasketPrice discountFromTotal = DiscountFromTotalBasketPrice.builder()
                .requiredBasketTotalValue(requiredBasketTotalValue)
                .build();

        // Act
        Boolean discountConditionsMet = discountFromTotal.areDiscountConditionsMet(higherTotalValue);

        // Assert
        assertTrue(discountConditionsMet);
    }

    @Test
    public void whenDiscountConditionsMetWithEqualValueThenReturnTrue() {
        // Arrange
        MonetaryValue requiredBasketTotalValue = MonetaryValue.of("600.00", Currency.USD);

        MonetaryValue equalToRequiredTotalValue = requiredBasketTotalValue;

        DiscountFromTotalBasketPrice discountFromTotal = DiscountFromTotalBasketPrice.builder()
                .requiredBasketTotalValue(requiredBasketTotalValue)
                .build();

        // Act
        Boolean discountConditionsMet = discountFromTotal.areDiscountConditionsMet(equalToRequiredTotalValue);

        // Assert
        assertTrue(discountConditionsMet);
    }

    @Test
    public void whenDiscountConditionsAreNotMetThenReturnFalse() {
        // Arrange
        MonetaryValue requiredBasketTotalValue = MonetaryValue.of("600.00", Currency.USD);

        MonetaryValue lowerTotalValue = MonetaryValue.of("599.00", Currency.USD);

        DiscountFromTotalBasketPrice discountFromTotal = DiscountFromTotalBasketPrice.builder()
                .requiredBasketTotalValue(requiredBasketTotalValue)
                .build();

        // Act
        Boolean discountConditionsMet = discountFromTotal.areDiscountConditionsMet(lowerTotalValue);

        // Assert
        assertFalse(discountConditionsMet);
    }

}
