package szczepanski.gerard.market.checkout.domain.money;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;

import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class SubtractMonetaryValueCalculationTest {

    @Test
    public void whenSubtractTwoValidMonetaryValuesThenReturnNewValueObjectResult() {
        // Arrange
        MonetaryValue firstValue = MonetaryValue.of("21.00", Currency.USD);
        MonetaryValue secondValue = MonetaryValue.of("11.00", Currency.USD);

        MonetaryValue expectedSubtractResultValue = MonetaryValue.of("10.00", Currency.USD);

        // Act
        Calculation subtractCalculation = new SubtractMonetaryValueCalculation(firstValue, secondValue);
        MonetaryValue resultValue = subtractCalculation.calculate();

        // Assert
        assertEquals(resultValue, expectedSubtractResultValue);
    }

    @Test(expectedExceptions = ParamAssertionException.class, dataProvider = "nullMonetaryValuesDataProvider")
    public void whenValuesAreNulltDuringSubtractMonetaryValuesThenThrowParamAssertionException(MonetaryValue firstValue, MonetaryValue secondValue) {
        // Act
        new SubtractMonetaryValueCalculation(firstValue, secondValue);
    }

    @DataProvider(name = "nullMonetaryValuesDataProvider")
    public Object[][] nullMonetaryValuesDataProvider() {
        return new Object[][]{
                {null, MonetaryValue.of("21.00", Currency.USD)},
                {MonetaryValue.of("21.00", Currency.USD), null},
        };
    }

}
