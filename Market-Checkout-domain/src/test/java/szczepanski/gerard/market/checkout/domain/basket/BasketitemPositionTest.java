package szczepanski.gerard.market.checkout.domain.basket;

import org.testng.Assert;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.exception.MarketCheckoutRuntimeException;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;
import szczepanski.gerard.market.checkout.domain.promotion.ItemQuantityPromotion;

import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class BasketitemPositionTest {

    @Test
    public void createBasketItemPositionSuccess() {
        // Arrange
        Integer expectedQuantity = 1;
        BasketItemPositionType expectedType = BasketItemPositionType.ITEM;

        Basket basket = new Basket();
        Item item = BasketTestItemProvider.milk();

        // Act
        BasketItemPosition milkBasketItemPosition = BasketItemPosition.create(basket, item);

        // Assert
        assertNotNull(milkBasketItemPosition);
        assertEquals(milkBasketItemPosition.basket, basket);
        assertEquals(milkBasketItemPosition.item, item);
        assertEquals(milkBasketItemPosition.quantity, expectedQuantity);
        assertEquals(milkBasketItemPosition.type, expectedType);
    }

    @Test
    public void absorbBasketItemPositionForPromotionSuccess() {
        // Arrange
        Integer requiredPromotionItemQuantity = 2;
        Integer expectedQuantityAfterAbsorb = 5;

        Basket basket = new Basket();
        Item item = BasketTestItemProvider.milk();

        ItemQuantityPromotion promotionForMilk = ItemQuantityPromotion.builder()
                .promotedItem(item)
                .requiredItemQuantity(requiredPromotionItemQuantity)
                .promotionPrice(MonetaryValue.ZERO_USD)
                .build();

        BasketItemPosition milkBasketItemPromotionPosition = BasketItemPosition.create(basket, item);
        milkBasketItemPromotionPosition.promotion = promotionForMilk;
        milkBasketItemPromotionPosition.type = BasketItemPositionType.MULTIPRICED_PROMOTION;
        milkBasketItemPromotionPosition.quantity = 4;

        BasketItemPosition milkBasketItemPositionForAbsorb = BasketItemPosition.create(basket, item);
        milkBasketItemPositionForAbsorb.quantity = requiredPromotionItemQuantity;

        // Act
        milkBasketItemPromotionPosition.absorbBasketItemPositionForPromotion(milkBasketItemPositionForAbsorb);

        // Assert
        assertEquals(milkBasketItemPromotionPosition.quantity, expectedQuantityAfterAbsorb);
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void whenAbsorbingItemPositionIsNotFOrPromotionThenThrowException() {
        // Arrange
        Integer requiredPromotionItemQuantity = 2;

        Basket basket = new Basket();
        Item item = BasketTestItemProvider.milk();

        ItemQuantityPromotion promotionForMilk = ItemQuantityPromotion.builder()
                .promotedItem(item)
                .requiredItemQuantity(requiredPromotionItemQuantity)
                .promotionPrice(MonetaryValue.ZERO_USD)
                .build();

        BasketItemPositionType WRONG_TYPE_FOR_ABSORBING_POSITION = BasketItemPositionType.ITEM;

        BasketItemPosition milkBasketItemPromotionPosition = BasketItemPosition.create(basket, item);
        milkBasketItemPromotionPosition.promotion = promotionForMilk;
        milkBasketItemPromotionPosition.type = WRONG_TYPE_FOR_ABSORBING_POSITION;

        BasketItemPosition milkBasketItemPositionForAbsorb = BasketItemPosition.create(basket, item);
        milkBasketItemPositionForAbsorb.quantity = requiredPromotionItemQuantity;

        // Act
        milkBasketItemPromotionPosition.absorbBasketItemPositionForPromotion(milkBasketItemPositionForAbsorb);
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void whenAbsorbedItemPositionIsNotForItemThenThrowException() {
        // Arrange
        Integer requiredPromotionItemQuantity = 2;

        Basket basket = new Basket();
        Item item = BasketTestItemProvider.milk();

        ItemQuantityPromotion promotionForMilk = ItemQuantityPromotion.builder()
                .promotedItem(item)
                .requiredItemQuantity(requiredPromotionItemQuantity)
                .promotionPrice(MonetaryValue.ZERO_USD)
                .build();

        BasketItemPositionType WRONG_TYPE_FOR_ABSORBED_POSITION = BasketItemPositionType.MULTIPRICED_PROMOTION;

        BasketItemPosition milkBasketItemPromotionPosition = BasketItemPosition.create(basket, item);
        milkBasketItemPromotionPosition.promotion = promotionForMilk;
        milkBasketItemPromotionPosition.type = BasketItemPositionType.MULTIPRICED_PROMOTION;

        BasketItemPosition milkBasketItemPositionForAbsorb = BasketItemPosition.create(basket, item);
        milkBasketItemPositionForAbsorb.quantity = requiredPromotionItemQuantity;
        milkBasketItemPositionForAbsorb.type = WRONG_TYPE_FOR_ABSORBED_POSITION;

        // Act
        milkBasketItemPromotionPosition.absorbBasketItemPositionForPromotion(milkBasketItemPositionForAbsorb);
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void whenAbsorbedAndAbsorbingItemDoNotMatchThenThrowException() {
        // Arrange
        Integer requiredPromotionItemQuantity = 2;

        Basket basket = new Basket();
        Item item = BasketTestItemProvider.milk();
        Item ITEM_THAT_DO_NOT_MATCH = BasketTestItemProvider.orangeJuice();

        ItemQuantityPromotion promotionForMilk = ItemQuantityPromotion.builder()
                .promotedItem(item)
                .requiredItemQuantity(requiredPromotionItemQuantity)
                .promotionPrice(MonetaryValue.ZERO_USD)
                .build();

        BasketItemPosition milkBasketItemPromotionPosition = BasketItemPosition.create(basket, item);
        milkBasketItemPromotionPosition.promotion = promotionForMilk;
        milkBasketItemPromotionPosition.type = BasketItemPositionType.MULTIPRICED_PROMOTION;

        BasketItemPosition milkBasketItemPositionForAbsorb = BasketItemPosition.create(basket, ITEM_THAT_DO_NOT_MATCH);
        milkBasketItemPositionForAbsorb.quantity = requiredPromotionItemQuantity;

        // Act
        milkBasketItemPromotionPosition.absorbBasketItemPositionForPromotion(milkBasketItemPositionForAbsorb);
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void whenAbsorbedItemDoNotMetItemQuantityPromotionRequirementsThenThrowException() {
        // Arrange
        Integer requiredPromotionItemQuantity = 2;

        Basket basket = new Basket();
        Item item = BasketTestItemProvider.milk();

        ItemQuantityPromotion promotionForMilk = ItemQuantityPromotion.builder()
                .promotedItem(item)
                .requiredItemQuantity(requiredPromotionItemQuantity)
                .promotionPrice(MonetaryValue.ZERO_USD)
                .build();

        BasketItemPosition milkBasketItemPromotionPosition = BasketItemPosition.create(basket, item);
        milkBasketItemPromotionPosition.promotion = promotionForMilk;
        milkBasketItemPromotionPosition.type = BasketItemPositionType.MULTIPRICED_PROMOTION;

        Integer QUANTITY_THET_NOT_MET_REQUIREMENTS = 1;
        BasketItemPosition milkBasketItemPositionForAbsorb = BasketItemPosition.create(basket, item);
        milkBasketItemPositionForAbsorb.quantity = QUANTITY_THET_NOT_MET_REQUIREMENTS;

        // Act
        milkBasketItemPromotionPosition.absorbBasketItemPositionForPromotion(milkBasketItemPositionForAbsorb);
    }

    @Test
    public void convertForPromotionBasketItemPositionSuccess() {
        // Arrange
        Integer requiredPromotionItemQuantity = 2;
        Integer expectedQuantityAfterConversionToPromotion = 1;

        Basket basket = new Basket();
        Item item = BasketTestItemProvider.milk();

        ItemQuantityPromotion promotionForMilk = ItemQuantityPromotion.builder()
                .promotedItem(item)
                .requiredItemQuantity(requiredPromotionItemQuantity)
                .promotionPrice(MonetaryValue.ZERO_USD)
                .build();

        BasketItemPosition milkBasketItemPosition = BasketItemPosition.create(basket, item);
        milkBasketItemPosition.quantity = requiredPromotionItemQuantity;

        // Act
        milkBasketItemPosition.convertForPromotionBasketItemPosition(promotionForMilk);

        // Assert
        assertEquals(milkBasketItemPosition.quantity, expectedQuantityAfterConversionToPromotion);
        assertEquals(milkBasketItemPosition.promotion, promotionForMilk);
        assertTrue(milkBasketItemPosition.isMultipricedPromotionPosition());
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void whanPositionForConvertingIsActualyForPromotionThenThrowException() {
        // Arrange
        Integer requiredPromotionItemQuantity = 2;

        Basket basket = new Basket();
        Item item = BasketTestItemProvider.milk();

        ItemQuantityPromotion promotionForMilk = ItemQuantityPromotion.builder()
                .promotedItem(item)
                .requiredItemQuantity(requiredPromotionItemQuantity)
                .promotionPrice(MonetaryValue.ZERO_USD)
                .build();

        BasketItemPositionType FOR_PROMTION_TYPE = BasketItemPositionType.MULTIPRICED_PROMOTION;

        BasketItemPosition milkBasketItemPosition = BasketItemPosition.create(basket, item);
        milkBasketItemPosition.quantity = requiredPromotionItemQuantity;
        milkBasketItemPosition.type = FOR_PROMTION_TYPE;

        // Act
        milkBasketItemPosition.convertForPromotionBasketItemPosition(promotionForMilk);
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void whanPositionForConvertingIsForAnotherItemThanPromotionThenThrowException() {
        // Arrange
        Integer requiredPromotionItemQuantity = 2;

        Basket basket = new Basket();
        Item item = BasketTestItemProvider.milk();
        Item ANOTHER_ITEM = BasketTestItemProvider.orangeJuice();

        ItemQuantityPromotion promotionForMilk = ItemQuantityPromotion.builder()
                .promotedItem(ANOTHER_ITEM)
                .requiredItemQuantity(requiredPromotionItemQuantity)
                .promotionPrice(MonetaryValue.ZERO_USD)
                .build();

        BasketItemPosition milkBasketItemPosition = BasketItemPosition.create(basket, item);
        milkBasketItemPosition.quantity = requiredPromotionItemQuantity;

        // Act
        milkBasketItemPosition.convertForPromotionBasketItemPosition(promotionForMilk);
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void whanPositionForConvertingQuantityIsNotEnoughThenThrowException() {
        // Arrange
        Integer requiredPromotionItemQuantity = 2;
        Integer NOT_ENOUGH_QUANTITY_FOR_POSITION = 1;

        Basket basket = new Basket();
        Item item = BasketTestItemProvider.milk();

        ItemQuantityPromotion promotionForMilk = ItemQuantityPromotion.builder()
                .promotedItem(item)
                .requiredItemQuantity(requiredPromotionItemQuantity)
                .promotionPrice(MonetaryValue.ZERO_USD)
                .build();

        BasketItemPosition milkBasketItemPosition = BasketItemPosition.create(basket, item);
        milkBasketItemPosition.quantity = NOT_ENOUGH_QUANTITY_FOR_POSITION;

        // Act
        milkBasketItemPosition.convertForPromotionBasketItemPosition(promotionForMilk);
    }

}
