package szczepanski.gerard.market.checkout.domain.promotion;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;

@Test(suiteName = "unit-tests")
public class DiscountTest {

    @Test
    public void whenDiscountValueIsValidPercentageValueThenCreateDicountValueObject() {
        // Arrange
        String rawDiscountValue = "13%";

        // Act
        Discount discountValue = Discount.of(rawDiscountValue);

        // Assert
        Assert.assertEquals(discountValue.toString(), rawDiscountValue);
    }

    @Test(expectedExceptions = ParamAssertionException.class, dataProvider = "invalidDiscountValuesDataProvider")
    public void whenDiscountValueIsInvalidThenThrowParamAssertionException(String invalidDiscountValue) {
        Discount.of(invalidDiscountValue);
    }

    @DataProvider(name = "invalidDiscountValuesDataProvider")
    public Object[][] invalidDiscountValuesDataProvider() {
        return new Object[][]{
                {null},
                {""},
                {"%15%"},
                {"-15%"},
                {"not percent"}
        };
    }

}
