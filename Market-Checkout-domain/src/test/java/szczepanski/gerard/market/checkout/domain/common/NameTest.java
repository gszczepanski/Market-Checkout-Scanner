package szczepanski.gerard.market.checkout.domain.common;

import org.apache.commons.lang3.StringUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;

import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class NameTest {

    @Test
    public void whenNameValueDuringNameCreationIsValidThenCreateNameValueObject() {
        // Arrange
        String rawNameValue = "Milk";

        // Act
        Name name = Name.of(rawNameValue);

        // Assert
        assertEquals(name.toString(), rawNameValue);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenStringValueOfNameIsNullThenThrowParamAssertionException() {
        // Arrange
        String NULL_NAME = null;

        // Act
        Name.of(NULL_NAME);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenStringValueOfNameIsBlankThenThrowParamAssertionException() {
        // Arrange
        String BLANK_NAME = "";

        // Act
        Name.of(BLANK_NAME);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenStringValueOfNameIsTooLongThenThrowParamAssertionException() {
        // Arrange
        int exceededLenght = Name.MAX_LENGTH + 1;

        String TOO_LONG_NAME = StringUtils.repeat('a', exceededLenght);

        // Act
        Name.of(TOO_LONG_NAME);
    }


}
