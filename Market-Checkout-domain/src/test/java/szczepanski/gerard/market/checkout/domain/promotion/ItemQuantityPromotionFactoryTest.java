package szczepanski.gerard.market.checkout.domain.promotion;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.item.ItemRepository;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;

import java.util.Optional;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class ItemQuantityPromotionFactoryTest {

    ItemQuantityPromotionCodeGenerator codeGenerator;
    ItemRepository itemRepository;
    ItemQuantityPromotionFactory factory;

    @BeforeMethod
    public void beforeMethod() {
        codeGenerator = mock(ItemQuantityPromotionCodeGenerator.class);
        itemRepository = mock(ItemRepository.class);
        factory = new ItemQuantityPromotionFactory(codeGenerator, itemRepository);
    }

    @Test
    public void createValidItemQuantityPromotionSuccess() {
        // Arrange
        Name nameOfThePromotion = Name.of("Buy 3 tunas pay for 2");
        Integer requiredItemQuantity = 2;
        MonetaryValue priceForPromotion = MonetaryValue.of("8.99", Currency.USD);
        Code promotedItemCode = Code.of("item-0055");

        Code generatedCode = Code.of("iqp-0004");
        when(codeGenerator.generateNextCode()).thenReturn(generatedCode);

        Optional<Item> tuna = Optional.of(Item.builder().build());
        when(itemRepository.findOneByCode(promotedItemCode)).thenReturn(tuna);

        // Act
        ItemQuantityPromotion itemQuantityPromotion = factory.createItemQuantityPromotion(nameOfThePromotion, promotedItemCode, requiredItemQuantity, priceForPromotion);

        // Assert
        assertNotNull(itemQuantityPromotion);
        assertEquals(itemQuantityPromotion.getCode(), generatedCode);
        assertEquals(itemQuantityPromotion.getName(), nameOfThePromotion);
        assertEquals(itemQuantityPromotion.getPromotedItem(), tuna.get());
        assertEquals(itemQuantityPromotion.getRequiredItemQuantity(), requiredItemQuantity);
        assertEquals(itemQuantityPromotion.getPromotionPrice(), priceForPromotion);
        assertTrue(itemQuantityPromotion.getIsActive());

        verify(codeGenerator).generateNextCode();
        verify(itemRepository).findOneByCode(promotedItemCode);
    }


    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenNameParamIsNullThenThrowParamAssertionException() {
        // Arrange
        Name NULL_NAME = null;
        Integer requiredItemQuantity = 2;
        MonetaryValue priceForPromotion = MonetaryValue.of("8.99", Currency.USD);
        Code promotedItemCode = Code.of("item-0055");

        // Act
        factory.createItemQuantityPromotion(NULL_NAME, promotedItemCode, requiredItemQuantity, priceForPromotion);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenItemParamIsNullThenThrowParamAssertionException() {
        // Arrange
        Name nameOfThePromotion = Name.of("Buy 3 tunas pay for 2");
        Integer requiredItemQuantity = 2;
        MonetaryValue priceForPromotion = MonetaryValue.of("8.99", Currency.USD);
        Code NULL_PROMOTED_ITEM_CODE = null;


        // Act
        factory.createItemQuantityPromotion(nameOfThePromotion, NULL_PROMOTED_ITEM_CODE, requiredItemQuantity, priceForPromotion);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenRequiredItemQuantityParamIsNullThenThrowParamAssertionException() {
        // Arrange
        Name nameOfThePromotion = Name.of("Buy 3 tunas pay for 2");
        Integer NULL_REQUIRED_ITEM_QUANTITY = null;
        MonetaryValue priceForPromotion = MonetaryValue.of("8.99", Currency.USD);
        Code promotedItemCode = Code.of("item-0055");

        // Act
        factory.createItemQuantityPromotion(nameOfThePromotion, promotedItemCode, NULL_REQUIRED_ITEM_QUANTITY, priceForPromotion);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenPriceForPromotionParamIsNullThenThrowParamAssertionException() {
        // Arrange
        Name nameOfThePromotion = Name.of("Buy 3 tunas pay for 2");
        Integer requiredItemQuantity = 2;
        MonetaryValue NULL_PRICE_FOR_PROMOTION = null;
        Code promotedItemCode = Code.of("item-0055");

        // Act
        factory.createItemQuantityPromotion(nameOfThePromotion, promotedItemCode, requiredItemQuantity, NULL_PRICE_FOR_PROMOTION);
    }


}
