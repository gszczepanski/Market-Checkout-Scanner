package szczepanski.gerard.market.checkout.domain.promotion;

import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.data.domain.Pageable;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class ItemQuantityPromotionCodeGeneratorTest {

    ItemQuantityPromotionRepository itemQuantityPromotionRepository;
    ItemQuantityPromotionCodeGenerator itemQuantityPromotionCodeGenerator;

    @BeforeMethod
    public void beforeMethod() {
        itemQuantityPromotionRepository = mock(ItemQuantityPromotionRepository.class);
        itemQuantityPromotionCodeGenerator = new ItemQuantityPromotionCodeGenerator(itemQuantityPromotionRepository);
    }

    @Test
    public void whenPromotionIsInDbThenGetItsCodeAndReturnCodeIncrementedForNewPromotion() {
        // Arrange

        String rawLatestCode = "iqp-0997";
        Code latestCode = Code.of(rawLatestCode);
        List<Code> foundCodes = Arrays.asList(latestCode);
        when(itemQuantityPromotionRepository.getLatestCode(Matchers.any(Pageable.class))).thenReturn(foundCodes);

        Code expectedNextCode = Code.of("iqp-0998");

        // Act
        Code nextCode = itemQuantityPromotionCodeGenerator.generateNextCode();

        // Assert
        assertEquals(nextCode, expectedNextCode);
        verify(itemQuantityPromotionRepository).getLatestCode(Matchers.any(Pageable.class));
    }

    @Test
    public void whenPromotionIsNotInDbThenReturnFirstCodeForPromotion() {
        // Arrange

        List<Code> foundCodes = new ArrayList<>();
        when(itemQuantityPromotionRepository.getLatestCode(Matchers.any(Pageable.class))).thenReturn(foundCodes);

        Code expectedNextCode = Code.of("iqp-0001");

        // Act
        Code nextCode = itemQuantityPromotionCodeGenerator.generateNextCode();

        // Assert
        assertEquals(nextCode, expectedNextCode);
        verify(itemQuantityPromotionRepository).getLatestCode(Matchers.any(Pageable.class));
    }


}
