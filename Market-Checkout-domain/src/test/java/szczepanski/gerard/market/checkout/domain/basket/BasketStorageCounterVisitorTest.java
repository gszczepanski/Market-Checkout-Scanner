package szczepanski.gerard.market.checkout.domain.basket;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.exception.MarketCheckoutRuntimeException;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.promotion.*;
import szczepanski.gerard.market.checkout.domain.warehouse.SalesInformation;
import szczepanski.gerard.market.checkout.domain.warehouse.SalesInformationType;

import java.util.HashSet;
import java.util.Map;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class BasketStorageCounterVisitorTest {

    ItemQuantityPromotionRepository itemQuantityPromotionRepository;
    BoughtTogetherPromotionRepository boughtTogetherPromotionRepository;
    DiscountFromTotalBasketPriceRepository discountFromTotalBasketPriceRepository;
    BasketFactory basketFactory;

    @BeforeMethod
    public void beforeMethod() {
        itemQuantityPromotionRepository = mock(ItemQuantityPromotionRepository.class);
        boughtTogetherPromotionRepository = mock(BoughtTogetherPromotionRepository.class);
        discountFromTotalBasketPriceRepository = mock(DiscountFromTotalBasketPriceRepository.class);

        basketFactory = new BasketFactory(itemQuantityPromotionRepository, boughtTogetherPromotionRepository, discountFromTotalBasketPriceRepository);

        when(itemQuantityPromotionRepository.findAllActivePromotions()).thenReturn(new HashSet<>());
        when(boughtTogetherPromotionRepository.findAllActivePromotions()).thenReturn(new HashSet<>());
        when(discountFromTotalBasketPriceRepository.findAllActiveDiscounts()).thenReturn(new HashSet<>());
    }

    @Test
    public void visitAndCreateSalesInformationsBasedOnBasketState() {
        // Arrange
        Basket basket = basketFactory.create();

        Item milk = BasketTestItemProvider.milk();
        Item orangeJuice = BasketTestItemProvider.orangeJuice();

        Code codeForMilkPromo = Code.of("iqp-0001");
        ItemQuantityPromotion promotionForMilk = ItemQuantityPromotion.builder()
                .code(codeForMilkPromo)
                .isActive(true)
                .promotedItem(milk)
                .build();

        Integer expectedMilkQuantity = 2;
        BasketItemPosition positionForMilk = BasketItemPosition.create(basket, milk);
        positionForMilk.quantity = expectedMilkQuantity;

        Integer expectedJuiceQuantity = 1;
        BasketItemPosition positionForOrangeJuice = BasketItemPosition.create(basket, orangeJuice);

        Integer expectedMilkPromoQuantity = 3;
        BasketItemPosition positionForMilkItemQuantityPromotion = BasketItemPosition.create(basket, milk);
        positionForMilkItemQuantityPromotion.promotion = promotionForMilk;
        positionForMilkItemQuantityPromotion.type = BasketItemPositionType.MULTIPRICED_PROMOTION;
        positionForMilkItemQuantityPromotion.quantity = expectedMilkPromoQuantity;

        basket.basketPositions.add(positionForMilk);
        basket.basketPositions.add(positionForOrangeJuice);
        basket.basketPositions.add(positionForMilkItemQuantityPromotion);

        Integer expectedTotalDiscountQuantity = 1;
        Code totalDiscountCode = Code.of("dftp-0001");
        DiscountFromTotalBasketPrice totalDiscount = DiscountFromTotalBasketPrice.builder()
                .code(totalDiscountCode)
                .build();
        basket.appliedTotaPriceDiscount = totalDiscount;


        Integer expectedVoughtTogetherPromotionForMilkAndJuiceQuantity = 1;
        BoughtTogetherPromotion boughtTogetherPromotionForMilkAndJuice = BasketTestPromotionProvider.milkAndJuiceBoughtTogetherPromotion();
        basket.appliedBoughtTogetherPromotions.add(boughtTogetherPromotionForMilkAndJuice);

        basket.state = BasketState.CLOSED;

        BasketStorageCounterVisitor visitor = new BasketStorageCounterVisitor();

        // Act
        visitor.visit(basket);
        Map<Code, SalesInformation> salesInfoBasedOnBasketMap = visitor.getSalesInfoBasedOnBasketMap();

        // Assert
        assertNotNull(salesInfoBasedOnBasketMap);

        SalesInformation milkSalesInformation = salesInfoBasedOnBasketMap.get(milk.getCode());
        assertEquals(milkSalesInformation.getItem(), milk);
        assertEquals(milkSalesInformation.getQuantity(), expectedMilkQuantity);
        assertEquals(milkSalesInformation.getType(), SalesInformationType.ITEM);

        SalesInformation juiceSalesInformation = salesInfoBasedOnBasketMap.get(orangeJuice.getCode());
        assertEquals(juiceSalesInformation.getItem(), orangeJuice);
        assertEquals(juiceSalesInformation.getQuantity(), expectedJuiceQuantity);
        assertEquals(juiceSalesInformation.getType(), SalesInformationType.ITEM);

        SalesInformation milkPromoSalesInformation = salesInfoBasedOnBasketMap.get(codeForMilkPromo);
        assertEquals(milkPromoSalesInformation.getItemQuantityPromotion(), promotionForMilk);
        assertEquals(milkPromoSalesInformation.getQuantity(), expectedMilkPromoQuantity);
        assertEquals(milkPromoSalesInformation.getType(), SalesInformationType.ITEM_QUANTITY_PROMOTION);

        SalesInformation boughtTogetherPromotionForMilkAndJuiceSalesInformation = salesInfoBasedOnBasketMap.get(boughtTogetherPromotionForMilkAndJuice.getCode());
        assertEquals(boughtTogetherPromotionForMilkAndJuiceSalesInformation.getBoughtTogetherPromotion(), boughtTogetherPromotionForMilkAndJuice);
        assertEquals(boughtTogetherPromotionForMilkAndJuiceSalesInformation.getQuantity(), expectedVoughtTogetherPromotionForMilkAndJuiceQuantity);
        assertEquals(boughtTogetherPromotionForMilkAndJuiceSalesInformation.getType(), SalesInformationType.BOUGHT_TOGETHER_PROMOTION);

        SalesInformation totalDiscountSalesInformation = salesInfoBasedOnBasketMap.get(totalDiscountCode);
        assertEquals(totalDiscountSalesInformation.getDiscountFromTotalBasketPrice(), totalDiscount);
        assertEquals(totalDiscountSalesInformation.getQuantity(), expectedTotalDiscountQuantity);
        assertEquals(totalDiscountSalesInformation.getType(), SalesInformationType.TOTAL_BASKET_VALUE_DISCOUNT);
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void whenBasketIsNullForVisitThenThrowRuntimeException() {
        // Arrange
        Basket NULL_BASKET = null;

        BasketStorageCounterVisitor visitor = new BasketStorageCounterVisitor();

        // Act
        visitor.visit(NULL_BASKET);
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void whenBasketForVisitIsNotClosedThenThrowRuntimeException() {
        // Arrange
        Basket notClosedBasket = basketFactory.create();

        BasketStorageCounterVisitor visitor = new BasketStorageCounterVisitor();

        // Act
        visitor.visit(notClosedBasket);
    }

}
