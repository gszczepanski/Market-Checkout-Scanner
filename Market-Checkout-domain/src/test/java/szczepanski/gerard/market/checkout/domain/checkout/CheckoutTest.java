package szczepanski.gerard.market.checkout.domain.checkout;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;
import szczepanski.gerard.market.checkout.component.common.exception.MarketCheckoutRuntimeException;
import szczepanski.gerard.market.checkout.domain.basket.Basket;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;

import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class CheckoutTest {

    @Test
    public void createCheckoutWithNumberAssignThisNumberToCheckout() {
        // Arrange
        Integer checkoutNumber = 4;

        // Act
        Checkout checkoutNumberFour = Checkout.createWithNumber(checkoutNumber);

        // Assert
        assertNotNull(checkoutNumberFour);
        assertEquals(checkoutNumberFour.getNumber(), checkoutNumber);
    }

    @Test
    public void createdCheckoutHasNoBasketAtTheBeginning() {
        // Arrange
        Integer checkoutNumber = 4;

        // Act
        Checkout checkoutNumberFour = Checkout.createWithNumber(checkoutNumber);

        // Asserts
        assertNotNull(checkoutNumberFour);
        assertFalse(checkoutNumberFour.hasOpenedBasket());
    }

    @Test
    public void setBasketHandleToNewCheckoutAssignThatBasketToCheckout() {
        // Arrange
        Integer checkoutNumber = 4;
        Checkout checkout = Checkout.createWithNumber(checkoutNumber);

        Basket basket = Mockito.mock(Basket.class);
        Mockito.when(basket.isOpened()).thenReturn(true);

        // Act
        checkout.startHandlingNewBasket(basket);

        // Assert
        assertTrue(checkout.hasOpenedBasket());
        assertEquals(checkout.currentBasket, basket);
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void whenCheckoutHasOpenedBasketAndAnotherOneTryToBeAssignedThenThrowException() {
        // Arrange
        Integer checkoutNumber = 4;
        Checkout checkout = Checkout.createWithNumber(checkoutNumber);

        Basket basket = Mockito.mock(Basket.class);
        Mockito.when(basket.isOpened()).thenReturn(true);
        Basket anotherBasket = Mockito.mock(Basket.class);

        // Act
        checkout.startHandlingNewBasket(basket);
        checkout.startHandlingNewBasket(anotherBasket);
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void whenTryToAssignClosedBasketToCheckoutThenThrowException() {
        // Arrange
        Integer checkoutNumber = 4;
        Checkout checkout = Checkout.createWithNumber(checkoutNumber);

        Basket basket = Mockito.mock(Basket.class);
        Mockito.when(basket.isOpened()).thenReturn(false);

        // Act
        checkout.startHandlingNewBasket(basket);
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void scanItemWithoutAssignedBasketThrowsException() {
        // Arrange
        Integer checkoutNumber = 4;
        Checkout checkout = Checkout.createWithNumber(checkoutNumber);

        Item item = Mockito.mock(Item.class);

        // Act
        checkout.scanItem(item);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void scanNullItemThrowsParamAssertionException() {
        // Arrange
        Integer checkoutNumber = 4;
        Checkout checkout = Checkout.createWithNumber(checkoutNumber);

        Item NULL_ITEM = null;

        // Act
        checkout.scanItem(NULL_ITEM);
    }

    @Test
    public void scanValidItemAddsItemToBasket() {
        // Arrange
        Integer checkoutNumber = 4;
        Checkout checkout = Checkout.createWithNumber(checkoutNumber);

        Basket basket = Mockito.mock(Basket.class);
        Mockito.when(basket.isOpened()).thenReturn(true);
        Item item = Mockito.mock(Item.class);

        checkout.startHandlingNewBasket(basket);

        // Act
        checkout.scanItem(item);

        // Assert
        Mockito.verify(basket).addItem(item);
    }

    @Test
    public void askForTotalValueTriggersFetchingTotalPiceFromBasket() {
        // Arrange
        Integer checkoutNumber = 4;
        Checkout checkout = Checkout.createWithNumber(checkoutNumber);

        Basket basket = Mockito.mock(Basket.class);
        Mockito.when(basket.isOpened()).thenReturn(true);

        checkout.startHandlingNewBasket(basket);

        MonetaryValue totalValue = MonetaryValue.ZERO_USD;

        Mockito.when(basket.totalPrice()).thenReturn(totalValue);

        // Act
        MonetaryValue checkoutTotalValue = checkout.totalValue();

        // Assert
        assertEquals(checkoutTotalValue, totalValue);
        Mockito.verify(basket).totalPrice();
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void closeCheckoutWithoutAssignedBasketThrowsException() {
        // Arrange
        Integer checkoutNumber = 4;
        Checkout checkout = Checkout.createWithNumber(checkoutNumber);

        // Act
        checkout.closeThenGetCurrentBasket();
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void closeCheckoutCloseCurrentBasketAndStopHandlingIt() {
        // Arrange
        Integer checkoutNumber = 4;
        Checkout checkout = Checkout.createWithNumber(checkoutNumber);

        Basket basket = Mockito.mock(Basket.class);
        Mockito.when(basket.isOpened()).thenReturn(true);

        // Act
        Basket closedBasket = checkout.closeThenGetCurrentBasket();

        // Assert
        assertNull(checkout.currentBasket);
        assertEquals(closedBasket, basket);

        Mockito.verify(closedBasket).closeBasket();
    }

}
