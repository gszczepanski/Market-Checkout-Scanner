package szczepanski.gerard.market.checkout.domain.promotion;

import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.data.domain.Pageable;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class BoughtTogetherPromotionCodeGeneratorTest {

    BoughtTogetherPromotionRepository boughtTogetherPromotionRepository;
    BoughtTogetherPromotionCodeGenerator boughtTogetherPromotionCodeGenerator;

    @BeforeMethod
    public void beforeMethod() {
        boughtTogetherPromotionRepository = mock(BoughtTogetherPromotionRepository.class);
        boughtTogetherPromotionCodeGenerator = new BoughtTogetherPromotionCodeGenerator(boughtTogetherPromotionRepository);
    }

    @Test
    public void whenPromotionIsInDbThenGetItsCodeAndReturnCodeIncrementedForNewPromotion() {
        // Arrange

        String rawLatestCode = "btp-1997";
        Code latestCode = Code.of(rawLatestCode);
        List<Code> foundCodes = Arrays.asList(latestCode);
        when(boughtTogetherPromotionRepository.getLatestCode(Matchers.any(Pageable.class))).thenReturn(foundCodes);

        Code expectedNextCode = Code.of("btp-1998");

        // Act
        Code nextCode = boughtTogetherPromotionCodeGenerator.generateNextCode();

        // Assert
        assertEquals(nextCode, expectedNextCode);
        verify(boughtTogetherPromotionRepository).getLatestCode(Matchers.any(Pageable.class));
    }

    @Test
    public void whenPromotionIsNotInDbThenReturnFirstCodeForPromotion() {
        // Arrange

        List<Code> foundCodes = new ArrayList<>();
        when(boughtTogetherPromotionRepository.getLatestCode(Matchers.any(Pageable.class))).thenReturn(foundCodes);

        Code expectedNextCode = Code.of("btp-0001");

        // Act
        Code nextCode = boughtTogetherPromotionCodeGenerator.generateNextCode();

        // Assert
        assertEquals(nextCode, expectedNextCode);
        verify(boughtTogetherPromotionRepository).getLatestCode(Matchers.any(Pageable.class));
    }


}
