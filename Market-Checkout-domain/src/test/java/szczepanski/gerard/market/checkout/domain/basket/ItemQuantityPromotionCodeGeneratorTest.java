package szczepanski.gerard.market.checkout.domain.basket;

import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.data.domain.Pageable;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class ItemQuantityPromotionCodeGeneratorTest {

    BasketRepository basketRepository;
    BasketCodeGenerator basketCodeGenerator;

    @BeforeMethod
    public void beforeMethod() {
        basketRepository = mock(BasketRepository.class);
        basketCodeGenerator = new BasketCodeGenerator(basketRepository);
    }

    @Test
    public void whenBasketIsInDbThenGetItsCodeAndReturnCodeIncrementedForNewPromotion() {
        // Arrange

        String rawLatestCode = "b-1234";
        Code latestCode = Code.of(rawLatestCode);
        List<Code> foundCodes = Arrays.asList(latestCode);
        when(basketRepository.getLatestCode(Matchers.any(Pageable.class))).thenReturn(foundCodes);

        Code expectedNextCode = Code.of("b-1235");

        // Act
        Code nextCode = basketCodeGenerator.generateNextCode();

        // Assert
        assertEquals(nextCode, expectedNextCode);
        verify(basketRepository).getLatestCode(Matchers.any(Pageable.class));
    }

    @Test
    public void whenBasketIsNotInDbThenReturnFirstCodeForBasket() {
        // Arrange

        List<Code> foundCodes = new ArrayList<>();
        when(basketRepository.getLatestCode(Matchers.any(Pageable.class))).thenReturn(foundCodes);

        Code expectedNextCode = Code.of("b-0001");

        // Act
        Code nextCode = basketCodeGenerator.generateNextCode();

        // Assert
        assertEquals(nextCode, expectedNextCode);
        verify(basketRepository).getLatestCode(Matchers.any(Pageable.class));
    }


}
