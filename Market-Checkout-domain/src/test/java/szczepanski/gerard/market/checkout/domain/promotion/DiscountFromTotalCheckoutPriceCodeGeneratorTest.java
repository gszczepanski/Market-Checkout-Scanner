package szczepanski.gerard.market.checkout.domain.promotion;

import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.data.domain.Pageable;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class DiscountFromTotalCheckoutPriceCodeGeneratorTest {

    DiscountFromTotalBasketPriceRepository discountFromTotalCheckoutPriceRepository;
    DiscountFromTotalBasketPriceCodeGenerator discountCodeGenerator;

    @BeforeMethod
    public void beforeMethod() {
        discountFromTotalCheckoutPriceRepository = mock(DiscountFromTotalBasketPriceRepository.class);
        discountCodeGenerator = new DiscountFromTotalBasketPriceCodeGenerator(discountFromTotalCheckoutPriceRepository);
    }

    @Test
    public void whenDiscountIsInDbThenGetItsCodeAndReturnCodeIncrementedForNewDiscount() {
        // Arrange

        String rawLatestCode = "dftp-0973";
        Code latestCode = Code.of(rawLatestCode);
        List<Code> foundCodes = Arrays.asList(latestCode);
        when(discountFromTotalCheckoutPriceRepository.getLatestCode(Matchers.any(Pageable.class))).thenReturn(foundCodes);

        Code expectedNextCode = Code.of("dftp-0974");

        // Act
        Code nextCode = discountCodeGenerator.generateNextCode();

        // Assert
        assertEquals(nextCode, expectedNextCode);
        verify(discountFromTotalCheckoutPriceRepository).getLatestCode(Matchers.any(Pageable.class));
    }

    @Test
    public void whenDiscountIsNotInDbThenReturnFirstCodeForDiscount() {
        // Arrange

        List<Code> foundCodes = new ArrayList<>();
        when(discountFromTotalCheckoutPriceRepository.getLatestCode(Matchers.any(Pageable.class))).thenReturn(foundCodes);

        Code expectedNextCode = Code.of("dftp-0001");

        // Act
        Code nextCode = discountCodeGenerator.generateNextCode();

        // Assert
        assertEquals(nextCode, expectedNextCode);
        verify(discountFromTotalCheckoutPriceRepository).getLatestCode(Matchers.any(Pageable.class));
    }

}
