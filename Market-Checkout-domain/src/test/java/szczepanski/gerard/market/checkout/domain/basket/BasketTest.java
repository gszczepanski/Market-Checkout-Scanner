package szczepanski.gerard.market.checkout.domain.basket;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;
import szczepanski.gerard.market.checkout.component.common.exception.MarketCheckoutRuntimeException;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;
import szczepanski.gerard.market.checkout.domain.promotion.*;

import java.util.HashSet;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class BasketTest {

    ItemQuantityPromotionRepository itemQuantityPromotionRepository;
    BoughtTogetherPromotionRepository boughtTogetherPromotionRepository;
    DiscountFromTotalBasketPriceRepository discountFromTotalBasketPriceRepository;
    BasketFactory basketFactory;

    @BeforeMethod
    public void beforeMethod() {
        itemQuantityPromotionRepository = mock(ItemQuantityPromotionRepository.class);
        boughtTogetherPromotionRepository = mock(BoughtTogetherPromotionRepository.class);
        discountFromTotalBasketPriceRepository = mock(DiscountFromTotalBasketPriceRepository.class);

        basketFactory = new BasketFactory(itemQuantityPromotionRepository, boughtTogetherPromotionRepository, discountFromTotalBasketPriceRepository);

        when(itemQuantityPromotionRepository.findAllActivePromotions()).thenReturn(new HashSet<>());
        when(boughtTogetherPromotionRepository.findAllActivePromotions()).thenReturn(new HashSet<>());
        when(discountFromTotalBasketPriceRepository.findAllActiveDiscounts()).thenReturn(new HashSet<>());
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenAddItemToBasketAndThatItemIsNullThenThrowParamAssertionException() {
        // Arrange
        Basket basket = basketFactory.create();
        Item NULL_ITEM = null;

        // Act
        basket.addItem(NULL_ITEM);
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void whenBasketIsNotInOpenStateDuruingAddItemToBasketThenThrowRuntimeException() {
        // Arrange
        Basket basket = basketFactory.create();
        basket.state = BasketState.CLOSED;

        Item milk = BasketTestItemProvider.milk();

        // Act
        basket.addItem(milk);
    }

    @Test
    public void whenAddItemToBasketAndThatItemIsNotInBasketThenCreateBasketPositionForAddedItem() {
        // Arrange
        int expectedBasketPositionsSize = 1;
        Integer expectedMilkQuantity = 1;

        Basket basket = basketFactory.create();
        Item milk = BasketTestItemProvider.milk();

        // Act
        basket.addItem(milk);

        // Assert
        List<BasketItemPosition> basketPositions = basket.basketPositions;
        assertEquals(basketPositions.size(), expectedBasketPositionsSize);

        BasketItemPosition basketItemPosition = basketPositions.get(0);
        assertEquals(basketItemPosition.item, milk);
        assertEquals(basketItemPosition.basket, basket);
        assertEquals(basketItemPosition.quantity, expectedMilkQuantity);
        assertEquals(basketItemPosition.price, milk.getRegularPrice());
    }

    @Test
    public void whenAddItemsToBasketThenCreateBasketPositionsForAddedItems() {
        // Arrange
        int expectedBasketPositionsSize = 2;
        Integer expectedMilkBasketPositionQuantity = 2;
        Integer expectedJuiceBasketPositionQuantity = 1;

        Basket basket = basketFactory.create();
        Item milk = BasketTestItemProvider.milk();
        Item orangeJuice = BasketTestItemProvider.orangeJuice();

        // Act
        basket.addItem(milk);
        basket.addItem(orangeJuice);
        basket.addItem(milk);

        // Assert
        List<BasketItemPosition> basketPositions = basket.basketPositions;
        assertEquals(basketPositions.size(), expectedBasketPositionsSize);

        BasketItemPosition basketMilkPosition = basketPositions.get(0);
        assertEquals(basketMilkPosition.item, milk);
        assertEquals(basketMilkPosition.quantity, expectedMilkBasketPositionQuantity);

        BasketItemPosition basketJuicePosition = basketPositions.get(1);
        assertEquals(basketJuicePosition.item, orangeJuice);
        assertEquals(basketJuicePosition.quantity, expectedJuiceBasketPositionQuantity);
    }

    @Test
    public void checkIfTotalPriceOfBasketIsCalculatedProperlyAfterEveryAddItem() {
        // Arrange
        Basket basket = basketFactory.create();
        Item milk = BasketTestItemProvider.milk();
        Item orangeJuice = BasketTestItemProvider.orangeJuice();

        MonetaryValue expectedTotalValueOnInitializedBasket = MonetaryValue.of("0.00", Currency.USD);
        MonetaryValue expectedTotalValueAfterFirstMilkAdded = MonetaryValue.of("3.99", Currency.USD);
        MonetaryValue expectedTotalValueAfterFirstJuiceAdded = MonetaryValue.of("9.49", Currency.USD);
        MonetaryValue expectedTotalValueAfterSecondMilkAdded = MonetaryValue.of("13.48", Currency.USD);

        // Act
        MonetaryValue totalPriceOnInitializedBasket = basket.totalPrice();

        basket.addItem(milk);
        MonetaryValue totalPriceAfterFirstMilkAdded = basket.totalPrice();

        basket.addItem(orangeJuice);
        MonetaryValue totalPriceAfterFirstJuiceAdded = basket.totalPrice();

        basket.addItem(milk);
        MonetaryValue totalPriceAfterSecondMilkAdded = basket.totalPrice();

        // Assert
        assertEquals(totalPriceOnInitializedBasket, expectedTotalValueOnInitializedBasket);
        assertEquals(totalPriceAfterFirstMilkAdded, expectedTotalValueAfterFirstMilkAdded);
        assertEquals(totalPriceAfterFirstJuiceAdded, expectedTotalValueAfterFirstJuiceAdded);
        assertEquals(totalPriceAfterSecondMilkAdded, expectedTotalValueAfterSecondMilkAdded);
    }

    @Test
    public void whenAddItemsToBasketAndBoughtTogetherPromotionIsActiveAndFitsWithItemsThenApplyDiscountToItems() {
        // Arrange
        int expectedAppliedBoughtTogetherPromotionsSizeAfterMilkAdded = 0;
        int expectedAppliedBoughtTogetherPromotionsSizeAfterJuiceAdded = 1;

        MonetaryValue expectedTotalValueAfterMilkAdded = MonetaryValue.of("3.99", Currency.USD);
        MonetaryValue expectedTotalValueAfterPromotionApplied = MonetaryValue.of("7.11", Currency.USD);
        MonetaryValue expectedMilkPriceAfterPromotionApplied = MonetaryValue.of("2.99", Currency.USD);
        MonetaryValue expectedJuicePriceAfterPromotionApplied = MonetaryValue.of("4.12", Currency.USD);

        Basket basket = basketFactory.create();
        BoughtTogetherPromotion milkAndJuiceBoughtTogetherPromotion = BasketTestPromotionProvider.milkAndJuiceBoughtTogetherPromotion();
        basket.activeMarketBoughtTogetherPromotions.add(milkAndJuiceBoughtTogetherPromotion);

        Item milk = BasketTestItemProvider.milk();
        Item orangeJuice = BasketTestItemProvider.orangeJuice();

        // Act
        basket.addItem(milk);
        MonetaryValue totalPriceAfterMilkAdded = basket.totalPrice();
        int appliedBoughtTogetherPromotionsSizeAfterMilkAdded = basket.appliedBoughtTogetherPromotions.size();

        basket.addItem(orangeJuice);
        MonetaryValue totalPriceAfterJuiceAdded = basket.totalPrice();
        int appliedBoughtTogetherPromotionsSizeAfterJuiceAdded = basket.appliedBoughtTogetherPromotions.size();

        // Assert
        assertEquals(appliedBoughtTogetherPromotionsSizeAfterMilkAdded, expectedAppliedBoughtTogetherPromotionsSizeAfterMilkAdded);
        assertEquals(appliedBoughtTogetherPromotionsSizeAfterJuiceAdded, expectedAppliedBoughtTogetherPromotionsSizeAfterJuiceAdded);
        assertEquals(totalPriceAfterMilkAdded, expectedTotalValueAfterMilkAdded);
        assertEquals(totalPriceAfterJuiceAdded, expectedTotalValueAfterPromotionApplied);

        BasketItemPosition milkBasketPosition = basket.basketPositions.get(0);
        assertEquals(milkBasketPosition.price, expectedMilkPriceAfterPromotionApplied);

        BasketItemPosition juiceBasketPosition = basket.basketPositions.get(1);
        assertEquals(juiceBasketPosition.price, expectedJuicePriceAfterPromotionApplied);
    }

    @Test
    public void whenPriceOfBasketReachTotalBasketPriceDiscountThenDiscountIsAppliedToTotalPrice() {
        // Arrange
        MonetaryValue expectedTotalPriceAfterFirstCoffeAdded = MonetaryValue.of("75.00", Currency.USD);
        MonetaryValue expectedTotalPriceAfterDicountAdded = MonetaryValue.of("90.00", Currency.USD);

        Basket basket = basketFactory.create();
        DiscountFromTotalBasketPrice discountFromTotalBasketPrice = BasketTestPromotionProvider.discountFromTotalBasket100UsdPrice();
        basket.activeTotaPriceDiscounts.add(discountFromTotalBasketPrice);

        Item coffee = BasketTestItemProvider.exclusiveCoffee();

        //Act
        basket.addItem(coffee);
        MonetaryValue totalPriceAfterFirstCoffeAdded = basket.totalPrice();
        DiscountFromTotalBasketPrice discountAppliedAfterFirstCoffeAdded = basket.appliedTotaPriceDiscount;

        basket.addItem(coffee);
        MonetaryValue totalPriceAfterSecondCoffeAdded = basket.totalPrice();
        DiscountFromTotalBasketPrice discountAppliedAfterSecondCoffeAdded = basket.appliedTotaPriceDiscount;

        // Assert
        assertEquals(totalPriceAfterFirstCoffeAdded, expectedTotalPriceAfterFirstCoffeAdded);
        assertEquals(totalPriceAfterSecondCoffeAdded, expectedTotalPriceAfterDicountAdded);
        assertNull(discountAppliedAfterFirstCoffeAdded);
        assertEquals(discountAppliedAfterSecondCoffeAdded, discountFromTotalBasketPrice);
    }

    @Test(expectedExceptions = MarketCheckoutRuntimeException.class)
    public void fetchRecipeOnOpenedBasketThrowsException() {
        // Arrange
        Basket basket = basketFactory.create();

        // Act
        basket.recipe();
    }

    @Test
    public void closeBasketSetStateOnclosedAndGenerateRecipe() {
        // Arrange
        Basket basket = basketFactory.create();
        BasketState basketStateBeforeClose = basket.state;
        Recipe NULL_RECIPE_FROM_OPENED_BASKET = basket.recipe;

        //Act
        basket.closeBasket();

        // Assert
        assertEquals(basketStateBeforeClose, BasketState.OPEN);
        assertNull(NULL_RECIPE_FROM_OPENED_BASKET);

        assertEquals(basket.state, BasketState.CLOSED);
        assertNotNull(basket.recipe());
    }

}
