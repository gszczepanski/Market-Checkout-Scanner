package szczepanski.gerard.market.checkout.domain.basket;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class BasketTestItemProvider {

    public static Item milk() {
        return item("ITEM-0001", "Milk", "3.99");
    }

    public static Item orangeJuice() {
        return item("ITEM-0002", "Orange Juice", "5.50");
    }

    public static Item exclusiveCoffee() {
        return item("ITEM-0003", "Cafe luwak", "75.00");
    }

    private static Item item(String code, String name, String price) {
        return Item.builder()
                .code(Code.of(code))
                .name(Name.of(name))
                .regularPrice(MonetaryValue.of(price, Currency.USD))
                .build();
    }

}
