package szczepanski.gerard.market.checkout.domain.item;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class ItemFactoryTest {

    ItemCodeGenerator itemCodeGenerator;
    ItemFactory itemFactory;

    @BeforeMethod
    public void beforeMethod() {
        itemCodeGenerator = mock(ItemCodeGenerator.class);
        itemFactory = new ItemFactory(itemCodeGenerator);
    }

    @Test
    public void createValidItemSuccess() {
        // Arrange
        Name name = Name.of("Lipton Tea 100 bags");
        MonetaryValue regularPrice = MonetaryValue.of("2.99", Currency.USD);

        Code generatedCode = Code.of("ITEM-1256");
        when(itemCodeGenerator.generateNextCode()).thenReturn(generatedCode);

        // Act
        Item item = itemFactory.createItem(name, regularPrice);

        // Assert
        assertNotNull(item);
        assertEquals(item.getCode(), generatedCode);
        assertEquals(item.getName(), name);
        assertEquals(item.getRegularPrice(), regularPrice);

        verify(itemCodeGenerator).generateNextCode();
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenNameParamIsNullThenThrowParamAssertionException() {
        // Arrange
        Name NULL_NAME = null;
        MonetaryValue regularPrice = MonetaryValue.of("2.99", Currency.USD);

        // Act
        itemFactory.createItem(NULL_NAME, regularPrice);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenRegularPriceParamIsNullThenThrowParamAssertionException() {
        // Arrange
        Name name = Name.of("Lipton Tea 100 bags");
        MonetaryValue NULL_REGULAR_PRICE = null;

        // Act
        itemFactory.createItem(name, NULL_REGULAR_PRICE);
    }
}
