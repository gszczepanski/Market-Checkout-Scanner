package szczepanski.gerard.market.checkout.domain.money;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;

import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class MultiplyMonetaryValueCalculationTest {

    @Test(dataProvider = "multiplyValidValuesDataProvider")
    public void whenMultiplyParamsAreValidThenMultiplyMonetaryValue(MonetaryValue monetaryValue, Integer times, MonetaryValue expectedMonetaryValueAfterMultiply) {
        // Act
        Calculation multiplyMonetaryValueCalculation = new MultiplyMonetaryValueCalculation(monetaryValue, times);
        MonetaryValue multipliedMonetaryValue = multiplyMonetaryValueCalculation.calculate();

        // Assert
        assertEquals(multipliedMonetaryValue, expectedMonetaryValueAfterMultiply);
    }

    @DataProvider(name = "multiplyValidValuesDataProvider")
    public Object[][] multiplyValidValuesDataProvider() {
        return new Object[][]{
                {MonetaryValue.of("2.00", Currency.USD), 0, MonetaryValue.of("0.00", Currency.USD)},
                {MonetaryValue.of("2.00", Currency.USD), 1, MonetaryValue.of("2.00", Currency.USD)},
                {MonetaryValue.of("2.00", Currency.USD), 2, MonetaryValue.of("4.00", Currency.USD)},
                {MonetaryValue.of("2.00", Currency.USD), 5, MonetaryValue.of("10.00", Currency.USD)}
        };
    }

    @Test(expectedExceptions = ParamAssertionException.class, dataProvider = "invalidValuesDataProvider")
    public void whenValuesAreInvalidDuringMultiplyMonetaryValuesThenThrowParamAssertionException(MonetaryValue monetaryValue, Integer times) {
        // Act
        new MultiplyMonetaryValueCalculation(monetaryValue, times);
    }

    @DataProvider(name = "invalidValuesDataProvider")
    public Object[][] invalidValuesDataProvider() {
        return new Object[][]{
                {null, 1},
                {MonetaryValue.of("21.00", Currency.USD), null},
                {MonetaryValue.of("21.00", Currency.USD), -1},
        };
    }

}
