package szczepanski.gerard.market.checkout.domain.promotion;

import org.mockito.Mockito;
import org.mockito.internal.util.collections.Sets;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.item.ItemRepository;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class BoughtTogetherPromotionFactoryTest {

    BoughtTogetherPromotionCodeGenerator codeGenerator;
    ItemRepository itemRepository;
    BoughtTogetherPromotionFactory factory;

    @BeforeMethod
    public void beforeMethod() {
        codeGenerator = mock(BoughtTogetherPromotionCodeGenerator.class);
        itemRepository = mock(ItemRepository.class);
        factory = new BoughtTogetherPromotionFactory(codeGenerator, itemRepository);
    }

    @Test
    public void createValidBoughtTogetherPromotionSuccess() {
        // Arrange
        Name nameOfThePromotion = Name.of("Boy Coca cola and Sprite together and save 20%");
        Discount discount = Discount.of("20%");
        Set<Code> promotionItemCodes = Sets.newSet(Code.of("item-230"), Code.of("item-232"));

        Code generatedCode = Code.of("btp-0017");
        when(codeGenerator.generateNextCode()).thenReturn(generatedCode);

        Set<Item> foundItems = new HashSet<>();
        when(itemRepository.findAllForCodes(promotionItemCodes)).thenReturn(foundItems);

        // Act
        BoughtTogetherPromotion boughtTogetherPromotion = factory.createBoughtTogetherPromotion(nameOfThePromotion, discount, promotionItemCodes);

        // Assert
        assertNotNull(boughtTogetherPromotion);
        assertEquals(boughtTogetherPromotion.getCode(), generatedCode);
        assertEquals(boughtTogetherPromotion.getName(), nameOfThePromotion);
        assertEquals(boughtTogetherPromotion.getDiscount(), discount);
        assertEquals(boughtTogetherPromotion.getRequiredItems(), foundItems);
        assertTrue(boughtTogetherPromotion.getIsActive());

        verify(codeGenerator).generateNextCode();
        verify(itemRepository).findAllForCodes(promotionItemCodes);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenNameParamIsNullThenThrowParamAssertionException() {
        // Arrange
        Name NULL_NAME = null;
        Discount discount = Discount.of("20%");
        Set<Code> promotionItemCodes = Sets.newSet(Code.of("item-230"), Code.of("item-232"));

        // Act
        factory.createBoughtTogetherPromotion(NULL_NAME, discount, promotionItemCodes);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenDiscountParamIsNullThenThrowParamAssertionException() {
        // Arrange
        Name nameOfThePromotion = Name.of("Boy Coca cola and Sprite together and save 20%");
        Discount NULL_DISCOUNT = null;
        Set<Code> promotionItemCodes = Sets.newSet(Code.of("item-230"), Code.of("item-232"));

        // Act
        factory.createBoughtTogetherPromotion(nameOfThePromotion, NULL_DISCOUNT, promotionItemCodes);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenItemCodesParamIsNullThenThrowParamAssertionException() {
        // Arrange
        Name nameOfThePromotion = Name.of("Boy Coca cola and Sprite together and save 20%");
        Discount discount = Discount.of("20%");
        Set<Code> NULL_ITEM_CODES = null;

        // Act
        factory.createBoughtTogetherPromotion(nameOfThePromotion, discount, NULL_ITEM_CODES);
    }


}
