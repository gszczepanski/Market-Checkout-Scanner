package szczepanski.gerard.market.checkout.domain.money;

import org.testng.Assert;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class MonetaryValueJpaConverterTest {

    @Test
    public void whenConvertToDatabaseColumnThenReturnStringDBRepresentation() {
        // Arrange
        MonetaryValueJpaConverter converter = new MonetaryValueJpaConverter();

        MonetaryValue monetaryValue = MonetaryValue.of("14.00", Currency.USD);
        String expectedConvertedString = "14.00 USD";

        // Act
        String convertedDbString = converter.convertToDatabaseColumn(monetaryValue);

        // Assert
        assertEquals(convertedDbString, expectedConvertedString);
    }


    @Test
    public void whenConvertToEntityFromDatabaseColumnThenReturnMonetaryValue() {
        // Arrange
        MonetaryValueJpaConverter converter = new MonetaryValueJpaConverter();

        String dbString = "14.00 USD";
        MonetaryValue expectedMonetaryValue = MonetaryValue.of("14.00", Currency.USD);

        // Act
        MonetaryValue convertedMonetaryValue = converter.convertToEntityAttribute(dbString);

        // Assert
        assertEquals(convertedMonetaryValue, expectedMonetaryValue);
    }


}
