package szczepanski.gerard.market.checkout.domain.basket;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.promotion.BoughtTogetherPromotionRepository;
import szczepanski.gerard.market.checkout.domain.promotion.DiscountFromTotalBasketPriceRepository;
import szczepanski.gerard.market.checkout.domain.promotion.ItemQuantityPromotionRepository;

import java.util.HashSet;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class BasketBasicRecipeCreationVisitorTest {

    ItemQuantityPromotionRepository itemQuantityPromotionRepository;
    BoughtTogetherPromotionRepository boughtTogetherPromotionRepository;
    DiscountFromTotalBasketPriceRepository discountFromTotalCheckoutPriceRepository;
    BasketFactory basketFactory;

    @BeforeMethod
    public void beforeMethod() {
        itemQuantityPromotionRepository = mock(ItemQuantityPromotionRepository.class);
        boughtTogetherPromotionRepository = mock(BoughtTogetherPromotionRepository.class);
        discountFromTotalCheckoutPriceRepository = mock(DiscountFromTotalBasketPriceRepository.class);

        basketFactory = new BasketFactory(itemQuantityPromotionRepository, boughtTogetherPromotionRepository, discountFromTotalCheckoutPriceRepository);

        when(itemQuantityPromotionRepository.findAllActivePromotions()).thenReturn(new HashSet<>());
        when(boughtTogetherPromotionRepository.findAllActivePromotions()).thenReturn(new HashSet<>());
        when(discountFromTotalCheckoutPriceRepository.findAllActiveDiscounts()).thenReturn(new HashSet<>());
    }

    @Test
    public void generateCorrectRecipeWithoutAnyPromotionsFromBasketTest() {
        // Arrange
        Basket basket = basketFactory.create();

        Item milk = BasketTestItemProvider.milk();
        Item orangeJuice = BasketTestItemProvider.orangeJuice();

        basket.addItem(milk);
        basket.addItem(orangeJuice);
        basket.addItem(orangeJuice);

        String expectedMilkTextOnRecipe = "Milk x1 | 3.99 USD\nITEM-0001";
        String expectedOrangeJuiceTextOnRecipe = "Orange Juice x2 | 11.00 USD\nITEM-0002";
        String expectedTotalPriceOnRecipe = "TOTAL: " + basket.totalPrice().toString();

        // Act
        basket.closeBasket(); // This method triggers visitor
        Recipe recipe = basket.recipe();

        // Assert
        String recipeText = recipe.getRecipeText();

        System.out.println(recipeText);

        assertTrue(recipeText.contains(expectedMilkTextOnRecipe));
        assertTrue(recipeText.contains(expectedOrangeJuiceTextOnRecipe));
        assertTrue(recipeText.contains(expectedTotalPriceOnRecipe));
    }

    @Test
    public void generateCorrectRecipeWithSomeDiscountsFromBasketTest() {
        // Arrange
        Basket basket = basketFactory.create();

        basket.activeMarketBoughtTogetherPromotions.add(BasketTestPromotionProvider.milkAndJuiceBoughtTogetherPromotion());
        basket.activeTotaPriceDiscounts.add(BasketTestPromotionProvider.discountFromTotalBasket100UsdPrice());

        Item milk = BasketTestItemProvider.milk();
        Item orangeJuice = BasketTestItemProvider.orangeJuice();
        Item exclusiveCoffe = BasketTestItemProvider.exclusiveCoffee();

        basket.addItem(milk);
        basket.addItem(orangeJuice);
        basket.addItem(orangeJuice);
        basket.addItem(exclusiveCoffe);
        basket.addItem(exclusiveCoffe);

        String expectedMilkTextOnRecipe = "Milk x1 | 1.79 USD\nITEM-0001 discount: -2.20 USD";
        String expectedOrangeJuiceTextOnRecipe = "Orange Juice x2 | 4.95 USD\nITEM-0002 discount: -6.05 USD";
        String expectedCoffeTextOnRecipe = "Cafe luwak x2 | 90.00 USD\nITEM-0003 discount: -60.00 USD";

        String expectedMilkAndJuiceDiscountLine = "Milk and Orange Juice bought together discount 25%";
        String expectedTotalDiscountLine = "Purchase above 100.00 USD discount 40%";

        String expectedTotalPriceOnRecipe = "TOTAL: " + basket.totalPrice().toString();

        // Act
        basket.closeBasket(); // This method triggers visitor
        Recipe recipe = basket.recipe();

        // Assert
        String recipeText = recipe.getRecipeText();

        System.out.println(recipeText);

        assertTrue(recipeText.contains(expectedMilkTextOnRecipe));
        assertTrue(recipeText.contains(expectedOrangeJuiceTextOnRecipe));
        assertTrue(recipeText.contains(expectedCoffeTextOnRecipe));
        assertTrue(recipeText.contains(expectedMilkAndJuiceDiscountLine));
        assertTrue(recipeText.contains(expectedTotalDiscountLine));
        assertTrue(recipeText.contains(expectedTotalPriceOnRecipe));
    }

}
