package szczepanski.gerard.market.checkout.domain.promotion;

import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class DiscountFromTotalCheckoutPriceFactoryTest {

    DiscountFromTotalBasketPriceCodeGenerator codeGenerator;
    DiscountFromTotalBasketPriceFactory factory;

    @BeforeMethod
    public void beforeMethod() {
        codeGenerator = mock(DiscountFromTotalBasketPriceCodeGenerator.class);
        factory = new DiscountFromTotalBasketPriceFactory(codeGenerator);
    }

    @Test
    public void createValidDiscountFromTotalBasketPriceSuccess() {
        // Arrange

        MonetaryValue fromBasketPrice = MonetaryValue.of("600.00", Currency.USD);
        Discount discount = Discount.of("10%");

        Code generatedCode = Code.of("dftp-1256");
        when(codeGenerator.generateNextCode()).thenReturn(generatedCode);

        // Act
        DiscountFromTotalBasketPrice discountFromTotalCheckoutPrice = factory.createTotalDiscount(fromBasketPrice, discount);

        // Assert
        assertNotNull(discountFromTotalCheckoutPrice);
        assertEquals(discountFromTotalCheckoutPrice.getCode(), generatedCode);
        assertEquals(discountFromTotalCheckoutPrice.getDiscount(), discount);
        assertEquals(discountFromTotalCheckoutPrice.getRequiredBasketTotalValue(), fromBasketPrice);
        assertTrue(discountFromTotalCheckoutPrice.getIsActive());

        verify(codeGenerator).generateNextCode();
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenPriceParamIsNullThenThrowParamAssertionException() {
        // Arrange
        MonetaryValue NULL_PRICE = null;
        Discount discount = Discount.of("11%");

        // Act
        factory.createTotalDiscount(NULL_PRICE, discount);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenDiscountParamIsNullThenThrowParamAssertionException() {
        // Arrange
        MonetaryValue fromBasketPrice = MonetaryValue.of("600.00", Currency.USD);
        Discount NULL_DISCOUNT = null;

        // Act
        factory.createTotalDiscount(fromBasketPrice, NULL_DISCOUNT);
    }

}
