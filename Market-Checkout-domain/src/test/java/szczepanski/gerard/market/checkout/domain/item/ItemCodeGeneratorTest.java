package szczepanski.gerard.market.checkout.domain.item;

import org.mockito.Matchers;
import org.mockito.Mockito;
import org.springframework.data.domain.Pageable;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.*;
import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class ItemCodeGeneratorTest {

    ItemRepository itemRepository;
    ItemCodeGenerator itemCodeGenerator;

    @BeforeMethod
    public void beforeMethod() {
        itemRepository = mock(ItemRepository.class);
        itemCodeGenerator = new ItemCodeGenerator(itemRepository);
    }

    @Test
    public void whenItemIsInDbThenGetItsCodeAndReturnCodeIncrementedForNewItem() {
        // Arrange

        String rawLatestCode = "item-0973";
        Code latestCode = Code.of(rawLatestCode);
        List<Code> foundCodes = Arrays.asList(latestCode);
        when(itemRepository.getLatestCode(Matchers.any(Pageable.class))).thenReturn(foundCodes);

        Code expectedNextCode = Code.of("item-0974");

        // Act
        Code nextCode = itemCodeGenerator.generateNextCode();

        // Assert
        assertEquals(nextCode, expectedNextCode);
        verify(itemRepository).getLatestCode(Matchers.any(Pageable.class));
    }

    @Test
    public void whenItemIsNotInDbThenReturnFirstCodeForItem() {
        // Arrange

        List<Code> foundCodes = new ArrayList<>();
        when(itemRepository.getLatestCode(Matchers.any(Pageable.class))).thenReturn(foundCodes);

        Code expectedNextCode = Code.of("item-0001");

        // Act
        Code nextCode = itemCodeGenerator.generateNextCode();

        // Assert
        assertEquals(nextCode, expectedNextCode);
        verify(itemRepository).getLatestCode(Matchers.any(Pageable.class));
    }

}
