package szczepanski.gerard.market.checkout.domain.money;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;

import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class AddMonetaryValueCalculationTest {

    @Test
    public void whenAddTwoValidMonetaryValuesThenReturnNewValueObjectResult() {
        // Arrange
        MonetaryValue firstValue = MonetaryValue.of("21.00", Currency.USD);
        MonetaryValue secondValue = MonetaryValue.of("9.50", Currency.USD);

        MonetaryValue expectedAddResultValue = MonetaryValue.of("30.50", Currency.USD);

        // Act
        Calculation addCalculation = new AddMonetaryValueCalculation(firstValue, secondValue);
        MonetaryValue resultValue = addCalculation.calculate();

        // Assert
        assertEquals(resultValue, expectedAddResultValue);
    }

    @Test(expectedExceptions = ParamAssertionException.class, dataProvider = "nullMonetaryValuesDataProvider")
    public void whenValuesAreNulltDuringAddMonetaryValuesThenThrowParamAssertionException(MonetaryValue firstValue, MonetaryValue secondValue) {
        // Act
        new AddMonetaryValueCalculation(firstValue, secondValue);
    }

    @DataProvider(name = "nullMonetaryValuesDataProvider")
    public Object[][] nullMonetaryValuesDataProvider() {
        return new Object[][]{
                {null, MonetaryValue.of("21.00", Currency.USD)},
                {MonetaryValue.of("21.00", Currency.USD), null},
        };
    }

}
