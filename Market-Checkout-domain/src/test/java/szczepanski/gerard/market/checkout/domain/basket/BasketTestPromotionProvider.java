package szczepanski.gerard.market.checkout.domain.basket;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.mockito.internal.util.collections.Sets;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;
import szczepanski.gerard.market.checkout.domain.promotion.BoughtTogetherPromotion;
import szczepanski.gerard.market.checkout.domain.promotion.Discount;
import szczepanski.gerard.market.checkout.domain.promotion.DiscountFromTotalBasketPrice;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class BasketTestPromotionProvider {

    public static BoughtTogetherPromotion milkAndJuiceBoughtTogetherPromotion() {
        return BoughtTogetherPromotion.builder()
                .code(Code.of("BTP-0001"))
                .name(Name.of("Milk and Orange Juice bought together"))
                .requiredItems(Sets.newSet(
                        BasketTestItemProvider.milk(),
                        BasketTestItemProvider.orangeJuice()
                ))
                .discount(Discount.of("25%"))
                .isActive(true)
                .build();
    }

    public static DiscountFromTotalBasketPrice discountFromTotalBasket100UsdPrice() {
        return DiscountFromTotalBasketPrice.builder()
                .code(Code.of("DFTCP-0001"))
                .requiredBasketTotalValue(MonetaryValue.of("100.00", Currency.USD))
                .discount(Discount.of("40%"))
                .isActive(true)
                .build();
    }

}
