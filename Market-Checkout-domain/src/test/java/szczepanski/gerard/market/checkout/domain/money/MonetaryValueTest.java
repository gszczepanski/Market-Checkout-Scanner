package szczepanski.gerard.market.checkout.domain.money;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;

import java.math.BigDecimal;

import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class MonetaryValueTest {

    @Test
    public void whenProperParamsAreGivenDuringCreateMonetaryValueThenCreateNewMonetaryValue() {
        // Arrange
        String rawMonetaryValue = "30.00";
        Currency PLN_CURRENCY = Currency.USD;

        BigDecimal expectedValueAsBigDecimal = new BigDecimal(rawMonetaryValue);

        // Act
        MonetaryValue monetaryValue = MonetaryValue.of(rawMonetaryValue, PLN_CURRENCY);

        // Assert
        assertNotNull(monetaryValue);
        assertEquals(monetaryValue.getValue(), expectedValueAsBigDecimal);
        assertEquals(monetaryValue.getCurrency(), PLN_CURRENCY);
    }

    @Test(expectedExceptions = ParamAssertionException.class, dataProvider = "incorrectValuesDataProvider")
    public void whenValuesAreIncorrectDuringCreateMonetaryValueThenThrowParamAssertionException(String monetaryValue, Currency currency) {
        // Act
        MonetaryValue.of(monetaryValue, currency);
    }

    @DataProvider(name = "incorrectValuesDataProvider")
    public Object[][] incorrectValuesDataProvider() {
        return new Object[][]{
                {null, Currency.USD},
                {"", Currency.USD},
                {"incorrectValue", Currency.USD},
                {"12notCorrectValue", Currency.USD},
                {"12,888", Currency.USD},
                {"12.00", null}
        };
    }

}
