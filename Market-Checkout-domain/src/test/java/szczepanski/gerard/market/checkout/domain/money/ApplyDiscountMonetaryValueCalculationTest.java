package szczepanski.gerard.market.checkout.domain.money;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;
import szczepanski.gerard.market.checkout.domain.promotion.Discount;

import static org.testng.Assert.*;

@Test(suiteName = "unit-tests")
public class ApplyDiscountMonetaryValueCalculationTest {

    @Test(dataProvider = "discountValidValuesDataProvider")
    public void whenDiscountPramsAreValidThenCalculateDiscount(MonetaryValue monetaryValue, Discount discount, MonetaryValue expectedMonetaryValueWithAppliedDiscount) {
        // Act
        Calculation applyDiscountCalculation = new ApplyDiscountMonetaryValueCalculation(monetaryValue, discount);
        MonetaryValue monetaryValueWithAppliedDiscount = applyDiscountCalculation.calculate();

        // Assert
        assertEquals(monetaryValueWithAppliedDiscount, expectedMonetaryValueWithAppliedDiscount);
    }

    @DataProvider(name = "discountValidValuesDataProvider")
    public Object[][] discountValidValuesDataProvider() {
        return new Object[][]{
                {MonetaryValue.of("120.00", Currency.USD), Discount.of("10%"), MonetaryValue.of("108.00", Currency.USD)},
                {MonetaryValue.of("599.00", Currency.USD), Discount.of("60%"), MonetaryValue.of("239.60", Currency.USD)},
                {MonetaryValue.of("200.00", Currency.USD), Discount.of("10.50%"), MonetaryValue.of("179.00", Currency.USD)},
                {MonetaryValue.of("9.99", Currency.USD), Discount.of("17.99%"), MonetaryValue.of("8.19", Currency.USD)},
        };
    }

    @Test(expectedExceptions = ParamAssertionException.class, dataProvider = "invalidValuesDataProvider")
    public void whenValuesAreInvalidDuringApplyDiscountOnMonetaryValuesThenThrowParamAssertionException(MonetaryValue monetaryValue, Discount discount) {
        // Act
        new ApplyDiscountMonetaryValueCalculation(monetaryValue, discount);
    }

    @DataProvider(name = "invalidValuesDataProvider")
    public Object[][] invalidValuesDataProvider() {
        return new Object[][]{
                {null, Discount.of("10%")},
                {MonetaryValue.of("21.00", Currency.USD), null}
        };
    }

}
