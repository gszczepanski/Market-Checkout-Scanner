package szczepanski.gerard.market.checkout.domain.basket;

/**
 * CheckoutState represents current state of the checkout.
 * <br><br>
 * There are two states:
 * <br><br>
 * 1) OPEN - Checkout was created, products are being scanned and added to checkout.
 * <br>
 * 2) CLOSED - Checkout is closed and saved into DB. No product can be added, but receipt can be
 * fetched. Checkout is being closed after payment operation.
 *
 * @author Gerard Szczepa�ski
 */
enum BasketState {

    OPEN,
    CLOSED;

}
