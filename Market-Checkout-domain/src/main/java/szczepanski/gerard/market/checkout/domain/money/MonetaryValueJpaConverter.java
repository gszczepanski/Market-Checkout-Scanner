package szczepanski.gerard.market.checkout.domain.money;

import szczepanski.gerard.market.checkout.component.common.utils.MarketCheckoutStringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class MonetaryValueJpaConverter implements AttributeConverter<MonetaryValue, String> {

    @Override
    public String convertToDatabaseColumn(MonetaryValue value) {
        return value.getValue().toString() + MarketCheckoutStringUtils.SPACE + value.getCurrency().toString();
    }

    @Override
    public MonetaryValue convertToEntityAttribute(String valueFromDb) {
        String[] values = valueFromDb.split(MarketCheckoutStringUtils.REGEXP_SPACE);

        String value = values[0];
        Currency currency = Currency.valueOf(values[1]);

        return MonetaryValue.of(value, currency);
    }

}
