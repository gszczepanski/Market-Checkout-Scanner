package szczepanski.gerard.market.checkout.domain.basket;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.exception.MarketCheckoutRuntimeException;
import szczepanski.gerard.market.checkout.component.common.lock.AutoReleaseLock;
import szczepanski.gerard.market.checkout.domain.common.AbstractEntity;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValueCalculator;
import szczepanski.gerard.market.checkout.domain.promotion.BoughtTogetherPromotion;
import szczepanski.gerard.market.checkout.domain.promotion.Discount;
import szczepanski.gerard.market.checkout.domain.promotion.DiscountFromTotalBasketPrice;
import szczepanski.gerard.market.checkout.domain.promotion.ItemQuantityPromotion;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.*;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

/**
 * Aggregate that holds information about one shopping order.
 * <br>
 * <br>
 * Basket can be in two states. After creation is in Open state. This state
 * allows Checkout to add item to basket. Second basket state is
 * closed state. This state doesn't allow to add any Item to Basket.
 * <br><br>
 * <p>
 * Basket main operation is adding items. During each add item operation
 * Basket is checking Item, looks for any promotion for that item, looks for any promotion
 * after Item is added and Basket state is changed. Basket also recalculates itself after any Item added/promotion applied.
 * <br>
 * Promotion can be applied only during addItem operation. Basket has information
 * about any active promotions in Market. This information is obtained from BasketFactory during Basket creation time.
 * <br><br>
 * Promotions:
 * <br>
 * 1) ItemQuantityPromotion - applied as BasketItemPosition when Item reach required quantity. If there are many
 * ItemQuantityPromotions for single item (Milk for example), then first, lowest is being taken. Basket do not support multiple
 * ItemQuantityPromotion applying for single item.
 * <br>
 * 2) BoughtTogetherPromotion - is being applied when Basket has required Items. Discount from that promotion is
 * being applied for each BasketItemPosition for that items.
 * <br>
 * 3) DiscountFromTotalBasketPrice - discount applied, when total Basket price (after all promotions and discounts) reach
 * required MonetaryValue. Only one total discount can be applied for Basket. Basket looks for closest reached MonetaryValue required
 * total discount and apply it for Basket. So for example if there are two total discounts, from 200 USD and from 400 USD, nd Basket has 234 USD total
 * price, then first discount is applied. However, when more Items are being added, and Basket total value is nw 540 USD, then first discount is not applied,
 * but second discount is being applied.
 *
 * @author Gerard Szczepa�ski
 */
@Entity
@Table(name = "BASKET")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class Basket extends AbstractEntity {
    private static final long serialVersionUID = -9029389305732579649L;

    private static final Lock ADD_ITEM_LOCK = new ReentrantLock();
    private static final Lock CLOSE_BASKET_LOCK = new ReentrantLock();

    @Enumerated(EnumType.STRING)
    @JoinColumn(name = "STATE")
    protected BasketState state = BasketState.OPEN;
    @Column(name = "DATE", nullable = false)
    protected LocalDateTime date;
    @OneToMany(mappedBy = "basket", cascade = CascadeType.ALL)
    protected List<BasketItemPosition> basketPositions = new ArrayList<>();
    @ManyToMany
    @JoinTable(name = "BASKET_BOUGHT_TOGETHER_PROMOTION_JOIN_TABLE",
            joinColumns = {@JoinColumn(name = "BASKET_ID")},
            inverseJoinColumns = {
                    @JoinColumn(name = "BOUGHT_TOGETHER_PROMO_ID")})
    protected Set<BoughtTogetherPromotion> appliedBoughtTogetherPromotions = new HashSet<>();
    @ManyToOne
    @JoinColumn(name = "APPLIED_TOTAL_PRICE_DISCOUNT_ID")
    protected DiscountFromTotalBasketPrice appliedTotaPriceDiscount;
    @Column(name = "RECIPE", length = 6000)
    protected Recipe recipe;
    /**
     * Transient fields only for Open Basket calculations.
     */
    protected transient Set<ItemQuantityPromotion> activeItemQuantityPromotions;
    protected transient Set<BoughtTogetherPromotion> activeMarketBoughtTogetherPromotions;
    protected transient Set<DiscountFromTotalBasketPrice> activeTotaPriceDiscounts;
    @Getter
    @Column(name = "CODE", nullable = false, unique = true)
    private Code code;
    @Column(name = "TOTAL_PRICE")
    private MonetaryValue totalPrice = MonetaryValue.ZERO_USD;

    Basket(Code code, Set<ItemQuantityPromotion> activeItemQuantityPromotions, Set<BoughtTogetherPromotion> activeMarketBoughtTogetherPromotions, Set<DiscountFromTotalBasketPrice> activeTotaPriceDiscounts) {
        this.code = code;
        this.activeItemQuantityPromotions = activeItemQuantityPromotions;
        this.activeMarketBoughtTogetherPromotions = activeMarketBoughtTogetherPromotions;
        this.activeTotaPriceDiscounts = activeTotaPriceDiscounts;
    }

    /**
     * Add scanned item to Basket.
     * <br>
     * If item actually exist in Basket, then its quantity will be increased.
     * <br><br>
     * If any ItemQuantityPromotion quantity requirements are met during add to Basket, then
     * BasketItemPosition is created for that promotion. For example,
     * if we have 2 Milks added, and there is active ItemQuantityPromotion Buy 3 milks for 10.00 USD, then
     * when next Milk is being added, BasketItemPosition for promotion is created, and BasketitemPosition for
     * milks is removed.
     *
     * @param item - Item instance.
     */
    public void addItem(Item item) {
        try (AutoReleaseLock lock = new AutoReleaseLock(ADD_ITEM_LOCK)) {
            lock.lock();
            doAddItem(item);
        }
    }

    private void doAddItem(Item item) {
        ParamAssertion.guardIsNotNull(item);
        permitOpenBasketState();

        addItemToBasketPositions(item);
        applyItemQuantityPromotionForItem(item);
        recalculateBasketItemPrices();

        applyBoughtTogetherPromotions();
        recalculateTotalPriceOfBasket();

        boolean wasTotalDiscountApplied = applyTotalPriceDiscount();
        if (wasTotalDiscountApplied) {
            recalculateTotalPriceOfBasket();
        }
    }

    private void addItemToBasketPositions(Item item) {
        Optional<BasketItemPosition> optionalExistingPositionForItem = findExistingPositionForItem(item);

        if (optionalExistingPositionForItem.isPresent()) {
            BasketItemPosition basketPositionForItem = optionalExistingPositionForItem.get();
            basketPositionForItem.increaseItemQuantity();
        } else {
            BasketItemPosition basketPositionForItem = BasketItemPosition.create(this, item);
            basketPositions.add(basketPositionForItem);
        }
    }

    private Optional<BasketItemPosition> findExistingPositionForItem(Item item) {
        for (BasketItemPosition basketPosition : basketPositions) {
            if (basketPosition.isForItem(item) && basketPosition.isItemPosition()) {
                return Optional.of(basketPosition);
            }
        }
        return Optional.empty();
    }

    private void applyItemQuantityPromotionForItem(Item item) {
        BasketItemPosition basketItemPosition = findExistingPositionForItem(item).get();
        Optional<BasketItemPosition> optionalAppliedPromotionBasketItemPosition = findBasketItemPositionWithAppliedItemQuantityPromotionForItem(item);

        if (optionalAppliedPromotionBasketItemPosition.isPresent()) {
            BasketItemPosition appliedPromotionBasketItemPosition = optionalAppliedPromotionBasketItemPosition.get();
            absorbBasketItemPositionIfQuantityMatchRequiredItemQuantityPromotion(basketItemPosition, appliedPromotionBasketItemPosition);
        } else {
            Optional<ItemQuantityPromotion> optionalQuantityPromotionForItem = findItemQuantityPromotionWithSmallestRequiredQuantity(item);
            if (optionalQuantityPromotionForItem.isPresent()) {
                ItemQuantityPromotion itemQuantityPromotion = optionalQuantityPromotionForItem.get();
                createNewBasketItemPositionForItemQuantityPromotionIfRequirementsMet(basketItemPosition, itemQuantityPromotion);
            }
        }
    }

    private Optional<BasketItemPosition> findBasketItemPositionWithAppliedItemQuantityPromotionForItem(Item item) {
        for (BasketItemPosition basketPosition : basketPositions) {
            if (basketPosition.isForItem(item) && basketPosition.isMultipricedPromotionPosition()) {
                return Optional.of(basketPosition);
            }
        }
        return Optional.empty();
    }

    private void absorbBasketItemPositionIfQuantityMatchRequiredItemQuantityPromotion(BasketItemPosition basketItemPosition, BasketItemPosition appliedPromotionBasketItemPosition) {
        ItemQuantityPromotion promotion = appliedPromotionBasketItemPosition.getPromotion();
        Boolean quantityRequirementsMetForPromotion = promotion.areQuantityRequirementsMet(basketItemPosition.getQuantity());

        if (quantityRequirementsMetForPromotion) {
            appliedPromotionBasketItemPosition.absorbBasketItemPositionForPromotion(basketItemPosition);
            this.basketPositions.remove(basketItemPosition);
        }
    }

    private void createNewBasketItemPositionForItemQuantityPromotionIfRequirementsMet(BasketItemPosition basketItemPosition, ItemQuantityPromotion promotion) {
        Boolean quantityRequirementsMetForPromotion = promotion.areQuantityRequirementsMet(basketItemPosition.getQuantity());

        if (quantityRequirementsMetForPromotion) {
            basketItemPosition.convertForPromotionBasketItemPosition(promotion);
        }
    }

    private Optional<ItemQuantityPromotion> findItemQuantityPromotionWithSmallestRequiredQuantity(Item item) {
        return activeItemQuantityPromotions.stream()
                .filter(i -> i.isForItem(item))
                .sorted((i1, i2) -> i1.getRequiredItemQuantity().compareTo(i2.getRequiredItemQuantity()))
                .findFirst();
    }

    private void recalculateBasketItemPrices() {
        this.basketPositions.forEach(BasketItemPosition::recalcuate);
    }

    private void applyBoughtTogetherPromotions() {
        if (this.activeMarketBoughtTogetherPromotions.isEmpty()) {
            return;
        }
        appliedBoughtTogetherPromotions.clear();

        Set<Item> itemsInBasket = this.basketPositions
                .stream()
                .map(c -> c.item)
                .collect(Collectors.toSet());

        applyMetBoughtTogetherPromotions(itemsInBasket);
    }

    private void applyMetBoughtTogetherPromotions(Set<Item> itemsInBasket) {
        for (BoughtTogetherPromotion boughtTogetherPromotion : activeMarketBoughtTogetherPromotions) {
            boolean requiredItemsMet = boughtTogetherPromotion.requiredItemsMet(itemsInBasket);

            if (requiredItemsMet) {
                appliedBoughtTogetherPromotions.add(boughtTogetherPromotion);
                Discount discountToApply = boughtTogetherPromotion.getDiscount();

                for (BasketItemPosition checkoutPosition : basketPositions) {
                    boolean isPromotionForItem = boughtTogetherPromotion.isForItem(checkoutPosition.item);
                    if (isPromotionForItem) {
                        checkoutPosition.applyDiscount(discountToApply);
                    }
                }
            }
        }
    }

    private void recalculateTotalPriceOfBasket() {
        MonetaryValue recalculatedTotalPrice = MonetaryValue.ZERO_USD;
        for (BasketItemPosition basketPosition : basketPositions) {
            recalculatedTotalPrice = MonetaryValueCalculator.add(recalculatedTotalPrice, basketPosition.price);
        }

        this.totalPrice = recalculatedTotalPrice;
    }

    private boolean applyTotalPriceDiscount() {
        this.appliedTotaPriceDiscount = null;

        Optional<DiscountFromTotalBasketPrice> optionalTotalPriceDiscount =
                activeTotaPriceDiscounts.stream()
                        .sorted((d1, d2) -> d2.getRequiredBasketTotalValue().compareTo(d1.getRequiredBasketTotalValue()))
                        .filter(d -> d.areDiscountConditionsMet(totalPrice))
                        .findFirst();

        if (optionalTotalPriceDiscount.isPresent()) {
            applyTotalPriceDiscountToBasketItems(optionalTotalPriceDiscount.get());
            return true;
        }

        return false;
    }

    private void applyTotalPriceDiscountToBasketItems(DiscountFromTotalBasketPrice discount) {
        this.appliedTotaPriceDiscount = discount;
        this.basketPositions.forEach(c -> c.applyDiscount(discount.getDiscount()));
    }

    /**
     * Get total price of the Basket.
     *
     * @return total price of Basket.
     */
    public MonetaryValue totalPrice() {
        return totalPrice;
    }

    private void permitOpenBasketState() {
        if (state == BasketState.CLOSED) {
            throw new MarketCheckoutRuntimeException("Operation not allowed with closed Basket!");
        }
    }

    private void permitClosedBasketState() {
        if (state == BasketState.OPEN) {
            throw new MarketCheckoutRuntimeException("Operation not allowed with opened Basket!");
        }
    }

    /**
     * This method closes opened basket and creates Recipe object.
     * <br>
     * Since Basket is closed it remains unchangeable.
     */
    public void closeBasket() {
        try (AutoReleaseLock lock = new AutoReleaseLock(CLOSE_BASKET_LOCK)) {
            lock.lock();
            doCloseBasket();
        }
    }

    private void doCloseBasket() {
        permitOpenBasketState();

        this.date = LocalDateTime.now();
        this.state = BasketState.CLOSED;
        generateRecipeFromBasket();
    }

    private void generateRecipeFromBasket() {
        BasketBasicRecipeCreationVisitor recipeCreationVisitor = new BasketBasicRecipeCreationVisitor();
        accept(recipeCreationVisitor);
        this.recipe = recipeCreationVisitor.getGeneratedRecipe();
    }

    /**
     * Recipe can be fetched only if Basket was closed.
     * <br>
     *
     * @return Recipe object.
     */
    public Recipe recipe() {
        permitClosedBasketState();
        return this.recipe;
    }

    Boolean wasTotaPriceDiscountApplied() {
        return this.appliedTotaPriceDiscount != null;
    }

    public Boolean isOpened() {
        return this.state == BasketState.OPEN;
    }

    public Boolean isClosed() {
        return this.state == BasketState.CLOSED;
    }

    public Boolean wasTotalPriceDiscountApplied() {
        return this.appliedTotaPriceDiscount != null;
    }

    public void accept(BasketVisitor visitor) {
        ParamAssertion.guardIsNotNull(visitor);
        visitor.visit(this);
    }
}
