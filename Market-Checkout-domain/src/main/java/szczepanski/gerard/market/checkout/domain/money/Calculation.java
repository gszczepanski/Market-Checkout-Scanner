package szczepanski.gerard.market.checkout.domain.money;

/**
 * Strategy pattern interface that represents monetary calculation.
 * <br>
 * Result of the calculation is new MonetaryValue object.
 * <p>
 * <br><br>
 * Calculation interface helps to follow Open Closed principle. If any new Calculation
 * type appear in future, then just create new class that implements Calculation interface.
 *
 * @author Gerard Szczepa�ski
 */

@FunctionalInterface
public interface Calculation {

    /**
     * Perform calculation and then return MonetaryValue object as result of the
     * performed operation.
     *
     * @return new MonetaryValue object.
     */
    MonetaryValue calculate();

}
