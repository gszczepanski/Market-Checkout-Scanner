package szczepanski.gerard.market.checkout.domain.basket;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.List;

public interface BasketRepository extends JpaRepository<Basket, Long> {

    @Query("SELECT b.code "
            + "FROM Basket b "
            + "ORDER BY b.id DESC")
    List<Code> getLatestCode(Pageable pageable);

}
