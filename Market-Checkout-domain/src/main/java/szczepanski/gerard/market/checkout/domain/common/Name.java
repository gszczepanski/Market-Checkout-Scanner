package szczepanski.gerard.market.checkout.domain.common;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;

import java.io.Serializable;

/**
 * Value object that represents Item name.
 * <br>
 * Item names may have max 50 chars length.
 *
 * @author Gerard Szczepa�ski
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class Name implements Serializable {
    public static final int MAX_LENGTH = 50;
    private static final long serialVersionUID = 2984494418382643487L;
    private final String value;

    public static Name of(String value) {
        ParamAssertion.guardIsNotBlank(value);
        ParamAssertion.guardMaxLength(value, MAX_LENGTH);

        return new Name(value);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Name other = (Name) obj;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return value;
    }

}
