package szczepanski.gerard.market.checkout.domain.common;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;

import java.io.Serializable;

/**
 * Value object that represents code.
 * <br>
 * Example codes are: ITEM-0001, CHECKOUT-1974.
 *
 * @author Gerard Szczepa�ski
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class Code implements Serializable {
    private static final long serialVersionUID = 2984494418382643487L;

    private final String value;

    public static Code of(String value) {
        ParamAssertion.guardIsNotBlank(value);
        return new Code(value);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Code other = (Code) obj;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return value;
    }

}
