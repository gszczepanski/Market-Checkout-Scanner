package szczepanski.gerard.market.checkout.domain.money;

/**
 * Currency as enum. Since Checkout component handle only one currency, enum
 * value should be enough.
 * <p>
 * This currency do not support exchange to other currency.
 *
 * @author Gerard Szczepa�ski
 */
public enum Currency {

    USD;

    public static boolean isValidCurrencyRepresentation(String currency) {
        try {
            Currency.valueOf(currency);
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }

}
