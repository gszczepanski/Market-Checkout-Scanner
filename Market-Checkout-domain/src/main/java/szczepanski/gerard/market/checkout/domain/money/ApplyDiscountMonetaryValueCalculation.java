package szczepanski.gerard.market.checkout.domain.money;

import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.domain.promotion.Discount;

import java.math.BigDecimal;

/**
 * This Calculation implementor encapsulates applying discount operation.
 * <br><br>
 * Use this Calculation instance if you want to get MonetaryValue result of applied discount.
 * <br>
 * For example if you put MonetaryValue 100.00 USD with discount 10%, then you get
 * MonetaryValue 90.00 USD.
 *
 * @author Gerard Szczepa�ski
 */
class ApplyDiscountMonetaryValueCalculation implements Calculation {

    private final MonetaryValue monetaryValue;
    private final Discount discount;

    /**
     * @param monetaryValue - MonetaryValue to apply discount.
     * @param discount      - Discount value object.
     */
    public ApplyDiscountMonetaryValueCalculation(MonetaryValue monetaryValue, Discount discount) {
        ParamAssertion.guardIsNotNull(monetaryValue, discount);

        this.monetaryValue = monetaryValue;
        this.discount = discount;
    }

    public MonetaryValue calculate() {
        BigDecimal value = monetaryValue.getValue();
        BigDecimal discountValue = value.multiply(discount.toDecimalValue());

        BigDecimal valueWithAppliedDiscount = value.subtract(discountValue);
        Currency currency = monetaryValue.getCurrency();

        return MonetaryValue.of(valueWithAppliedDiscount, currency);
    }

}
