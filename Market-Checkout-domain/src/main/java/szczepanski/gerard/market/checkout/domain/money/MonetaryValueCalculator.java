package szczepanski.gerard.market.checkout.domain.money;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.market.checkout.domain.promotion.Discount;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MonetaryValueCalculator {

    public static MonetaryValue add(MonetaryValue firstMonetaryValue, MonetaryValue secondMonetaryValue) {
        Calculation calculation = new AddMonetaryValueCalculation(firstMonetaryValue, secondMonetaryValue);
        return calculation.calculate();
    }

    public static MonetaryValue subtract(MonetaryValue firstMonetaryValue, MonetaryValue secondMonetaryValue) {
        Calculation calculation = new SubtractMonetaryValueCalculation(firstMonetaryValue, secondMonetaryValue);
        return calculation.calculate();
    }

    public static MonetaryValue multiply(MonetaryValue monetaryValue, Integer times) {
        Calculation calculation = new MultiplyMonetaryValueCalculation(monetaryValue, times);
        return calculation.calculate();
    }

    public static MonetaryValue applyDiscount(MonetaryValue monetaryValue, Discount discount) {
        Calculation calculation = new ApplyDiscountMonetaryValueCalculation(monetaryValue, discount);
        return calculation.calculate();
    }

}
