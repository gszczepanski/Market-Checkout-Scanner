package szczepanski.gerard.market.checkout.domain.warehouse;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.lock.AutoReleaseLock;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.domain.basket.Basket;
import szczepanski.gerard.market.checkout.domain.basket.BasketStorageCounterVisitor;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class WarehouseUpdater {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(WarehouseUpdater.class.getName());

    private static final Lock UPDATE_STORAGE_LOCK = new ReentrantLock();

    private final SalesInformationRepository salesInformationRepository;

    @Transactional
    public void updateForClosedBasket(Basket basket) {
        ParamAssertion.guardIsNotNull(basket);
        ParamAssertion.guardIsTrue(basket.isClosed());
        LOG.info("Update warehouse SalesInformation info for closed basket with code: %s", basket.getCode());

        Map<Code, SalesInformation> salesInformationFromBasket = getSalesInformationFromBasket(basket);
        try (AutoReleaseLock lock = new AutoReleaseLock(UPDATE_STORAGE_LOCK)) {
            lock.lock();
            upateSalesInformation(salesInformationFromBasket);
        }
    }

    private Map<Code, SalesInformation> getSalesInformationFromBasket(Basket basket) {
        BasketStorageCounterVisitor storageCounterVisitor = new BasketStorageCounterVisitor();
        basket.accept(storageCounterVisitor);
        return storageCounterVisitor.getSalesInfoBasedOnBasketMap();
    }

    private void upateSalesInformation(Map<Code, SalesInformation> salesInformationFromBasket) {
        Set<Code> codes = salesInformationFromBasket.keySet();

        Set<SalesInformation> existingSalesInformations = this.salesInformationRepository.findAllByCodes(codes);
        updateExistingSalesInformations(existingSalesInformations, salesInformationFromBasket);

        Set<Code> codesForExistingSalesInformations = existingSalesInformations.stream().map(SalesInformation::getCode).collect(Collectors.toSet());
        codes.removeAll(codesForExistingSalesInformations);

        addNewSalesInformations(codes, salesInformationFromBasket);
    }

    private void updateExistingSalesInformations(Set<SalesInformation> existingSalesInformations, Map<Code, SalesInformation> salesInformationFromBasket) {
        for (SalesInformation existingSalesInformation : existingSalesInformations) {
            Code code = existingSalesInformation.getCode();
            SalesInformation countedSalesInformationFromBasket = salesInformationFromBasket.get(code);
            if (countedSalesInformationFromBasket != null) {
                existingSalesInformation.updateQuantity(countedSalesInformationFromBasket.getQuantity());
            }
        }

        this.salesInformationRepository.save(existingSalesInformations);
        this.salesInformationRepository.flush();
    }

    private void addNewSalesInformations(Set<Code> notExistingSalesInformationCodes, Map<Code, SalesInformation> salesInformationFromBasket) {
        Set<SalesInformation> newSalesInformations = new HashSet<>();

        notExistingSalesInformationCodes.forEach(code -> {
            SalesInformation salesInformation = salesInformationFromBasket.get(code);
            newSalesInformations.add(salesInformation);
        });

        this.salesInformationRepository.save(newSalesInformations);
        this.salesInformationRepository.flush();
    }

}
