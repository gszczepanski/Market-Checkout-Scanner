package szczepanski.gerard.market.checkout.domain.promotion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import szczepanski.gerard.market.checkout.domain.common.AbstractCodeGenerator;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.List;
import java.util.Optional;

@Component
class ItemQuantityPromotionCodeGenerator extends AbstractCodeGenerator {

    public static final String PROMOTION_CODE_PREFIX = "iqp-";
    public static final String PROMOTION_CODE_PATTERN = "%04d";
    public static final Code INIT_PROMOTION_CODE = Code.of("iqp-0000");

    private final ItemQuantityPromotionRepository itemQuantityPromotionRepository;

    @Autowired
    public ItemQuantityPromotionCodeGenerator(ItemQuantityPromotionRepository itemQuantityPromotionRepository) {
        super(INIT_PROMOTION_CODE, PROMOTION_CODE_PREFIX, PROMOTION_CODE_PATTERN);
        this.itemQuantityPromotionRepository = itemQuantityPromotionRepository;
    }

    @Override
    protected Optional<Code> getLatestCodeFromRepository() {
        List<Code> latestCode = itemQuantityPromotionRepository.getLatestCode(new PageRequest(0, 1));

        if (latestCode.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(latestCode.get(0));
    }

}
