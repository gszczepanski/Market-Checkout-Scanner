package szczepanski.gerard.market.checkout.domain.basket;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class RecipeJpaConverter implements AttributeConverter<Recipe, String> {

    @Override
    public String convertToDatabaseColumn(Recipe recipe) {
        return recipe.getRecipeText().toString();
    }

    @Override
    public Recipe convertToEntityAttribute(String value) {
        return Recipe.of(value);
    }

}
