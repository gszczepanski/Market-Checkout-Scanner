package szczepanski.gerard.market.checkout.domain.promotion;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import szczepanski.gerard.market.checkout.domain.common.AbstractCodeGenerator;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.List;
import java.util.Optional;

@Component
class DiscountFromTotalBasketPriceCodeGenerator extends AbstractCodeGenerator {

    public static final String DISCOUNT_CODE_PREFIX = "dftp-";
    public static final String DISCOUNT_CODE_PATTERN = "%04d";
    public static final Code INIT_DISCOUNT_CODE = Code.of("dftp-0000");

    private final DiscountFromTotalBasketPriceRepository discountFromTotalCheckoutPriceRepository;

    @Autowired
    public DiscountFromTotalBasketPriceCodeGenerator(DiscountFromTotalBasketPriceRepository discountFromTotalCheckoutPriceRepository) {
        super(INIT_DISCOUNT_CODE, DISCOUNT_CODE_PREFIX, DISCOUNT_CODE_PATTERN);
        this.discountFromTotalCheckoutPriceRepository = discountFromTotalCheckoutPriceRepository;
    }

    @Override
    protected Optional<Code> getLatestCodeFromRepository() {
        List<Code> latestCode = discountFromTotalCheckoutPriceRepository.getLatestCode(new PageRequest(0, 1));

        if (latestCode.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(latestCode.get(0));
    }

}
