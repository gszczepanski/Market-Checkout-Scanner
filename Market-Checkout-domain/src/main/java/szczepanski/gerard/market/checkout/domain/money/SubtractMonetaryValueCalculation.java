package szczepanski.gerard.market.checkout.domain.money;

import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;

import java.math.BigDecimal;

/**
 * This class represents subtract operation of two MonetaryValue values.
 * <br><br>
 * Currency of the values is not being validated because Market Checkout component supports only
 * one currency.
 * <p>
 * If this will change in the future, then proper currency validation should be added before
 * calculation.
 *
 * @author Gerard Szczepa�ski
 */
class SubtractMonetaryValueCalculation implements Calculation {

    private final MonetaryValue firstMonetaryValue;
    private final MonetaryValue secondMonetaryValue;

    public SubtractMonetaryValueCalculation(MonetaryValue firstMonetaryValue, MonetaryValue secondMonetaryValue) {
        ParamAssertion.guardIsNotNull(firstMonetaryValue, secondMonetaryValue);
        this.firstMonetaryValue = firstMonetaryValue;
        this.secondMonetaryValue = secondMonetaryValue;
    }

    @Override
    public MonetaryValue calculate() {
        BigDecimal firstValue = firstMonetaryValue.getValue();
        BigDecimal secondValue = secondMonetaryValue.getValue();

        BigDecimal resultValue = firstValue.subtract(secondValue);
        Currency resultCurrency = firstMonetaryValue.getCurrency();

        return MonetaryValue.of(resultValue, resultCurrency);
    }

}
