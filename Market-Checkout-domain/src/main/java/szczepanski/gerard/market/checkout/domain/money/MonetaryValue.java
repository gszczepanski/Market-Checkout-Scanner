package szczepanski.gerard.market.checkout.domain.money;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;

import java.io.Serializable;
import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Monetary value object, that has information about
 * currency and value.
 *
 * @author Gerard Szczepa�ski
 */
@Getter
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class MonetaryValue implements Serializable, Comparable<MonetaryValue> {

    public static final MonetaryValue ZERO_USD = MonetaryValue.of("0.00", Currency.USD);

    private final Currency currency;
    private final BigDecimal value;

    /**
     * Static factory method. Creates monetary value from value as String and currency supported by component.
     *
     * @param value    - decimal value as String.
     * @param currency - currency type supported by component.
     * @return MonetaryValue instance as value object.
     */
    public static MonetaryValue of(String value, Currency currency) {
        ParamAssertion.guardIsNotBlank(value);
        ParamAssertion.guardIsNumberRepresentation(value);
        ParamAssertion.guardIsNotNull(currency);

        BigDecimal bigDecimalValue = new BigDecimal(value);
        return of(bigDecimalValue, currency);
    }

    /**
     * Static factory method. Creates monetary value from value as BigDecimal and currency supported by component.
     *
     * @param value    - decimal value as BigDecimal.
     * @param currency - currency type supported by component.
     * @return MonetaryValue instance as value object.
     */
    public static MonetaryValue of(BigDecimal value, Currency currency) {
        ParamAssertion.guardIsNotNull(value);
        ParamAssertion.guardIsNotNull(currency);

        BigDecimal roundedValue = value.setScale(2, RoundingMode.HALF_DOWN);
        return new MonetaryValue(currency, roundedValue);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((currency == null) ? 0 : currency.hashCode());
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        MonetaryValue other = (MonetaryValue) obj;
        if (currency != other.currency)
            return false;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return String.format("%s %s", value, currency);
    }

    @Override
    public int compareTo(MonetaryValue o) {
        return this.getValue().compareTo(o.getValue());
    }

}
