package szczepanski.gerard.market.checkout.domain.promotion;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class DiscountJpaConverter implements AttributeConverter<Discount, String> {

    @Override
    public String convertToDatabaseColumn(Discount discount) {
        return discount.toString();
    }

    @Override
    public Discount convertToEntityAttribute(String value) {
        return Discount.of(value);
    }

}
