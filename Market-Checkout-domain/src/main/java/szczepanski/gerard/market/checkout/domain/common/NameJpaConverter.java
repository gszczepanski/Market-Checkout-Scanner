package szczepanski.gerard.market.checkout.domain.common;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class NameJpaConverter implements AttributeConverter<Name, String> {

    @Override
    public String convertToDatabaseColumn(Name name) {
        return name.toString();
    }

    @Override
    public Name convertToEntityAttribute(String value) {
        return Name.of(value);
    }

}
