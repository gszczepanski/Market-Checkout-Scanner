package szczepanski.gerard.market.checkout.domain.common;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

@Converter(autoApply = true)
public class CodeJpaConverter implements AttributeConverter<Code, String> {

    @Override
    public String convertToDatabaseColumn(Code code) {
        return code.toString();
    }

    @Override
    public Code convertToEntityAttribute(String value) {
        return Code.of(value);
    }

}
