package szczepanski.gerard.market.checkout.domain.promotion;

import lombok.*;
import szczepanski.gerard.market.checkout.domain.common.AbstractEntity;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;

import javax.persistence.*;

/**
 * Promotion for items bought in required quantity.
 * <br>
 * "buy N of them, and they�ll cost you Y cents".
 * <br><br>
 * Once promotion is created it cannot be modified.
 * Market can only activate/deactivate promotion.
 *
 * @author Gerard Szczepa�ski
 */
@Entity
@Table(name = "ITEM_QUANTITY_PROMOTION")
@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class ItemQuantityPromotion extends AbstractEntity {
    private static final long serialVersionUID = 8812088294414173839L;

    @Column(name = "CODE", nullable = false, unique = true)
    private Code code;

    @Column(name = "NAME", nullable = false)
    private Name name;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive = true;

    @ManyToOne
    @JoinColumn(name = "PROMOTED_ITEM_ID", nullable = false)
    private Item promotedItem;

    @Column(name = "REQUIRED_ITEM_QUANTITY", nullable = false)
    private Integer requiredItemQuantity;

    @Column(name = "PROMOTION_PRICE", nullable = false)
    private MonetaryValue promotionPrice;

    public Boolean isForItem(Item item) {
        return this.promotedItem.equals(item);
    }

    public Boolean areQuantityRequirementsMet(Integer itemQuantity) {
        return this.requiredItemQuantity.equals(itemQuantity);
    }

}
