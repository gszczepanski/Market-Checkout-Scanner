package szczepanski.gerard.market.checkout.domain.checkout;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.exception.MarketCheckoutRuntimeException;
import szczepanski.gerard.market.checkout.component.common.lock.AutoReleaseLock;
import szczepanski.gerard.market.checkout.domain.basket.Basket;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class Checkout {

    private final Lock checkoutOperationLock = new ReentrantLock();

    @Getter
    private final Integer number;

    protected Basket currentBasket;

    public static Checkout createWithNumber(Integer checkoutNumber) {
        ParamAssertion.guardIsNotNull(checkoutNumber);
        return new Checkout(checkoutNumber);
    }

    public void startHandlingNewBasket(Basket basket) {
        ParamAssertion.guardIsNotNull(basket);
        if (hasOpenedBasket()) {
            throw new MarketCheckoutRuntimeException("Cannot start handle new basket! Checkout already handle one. Close current basket to start handling new one.");
        }
        if (!basket.isOpened()) {
            throw new MarketCheckoutRuntimeException("Basket cannot be assigned! Basked is not in open state!");
        }

        this.currentBasket = basket;
    }

    public void scanItem(Item item) {
        ParamAssertion.guardIsNotNull(item);
        checkIfBasketAssigned();

        try (AutoReleaseLock lock = new AutoReleaseLock(checkoutOperationLock)) {
            lock.lock();
            this.currentBasket.addItem(item);
        }
    }

    public MonetaryValue totalValue() {
        checkIfBasketAssigned();
        return this.currentBasket.totalPrice();
    }

    public Basket closeThenGetCurrentBasket() {
        try (AutoReleaseLock lock = new AutoReleaseLock(checkoutOperationLock)) {
            lock.lock();
            return doCloseCurrentBasket();
        }
    }

    private Basket doCloseCurrentBasket() {
        checkIfBasketAssigned();

        Basket basket = this.currentBasket;
        basket.closeBasket();

        this.currentBasket = null;
        return basket;
    }

    public Boolean hasOpenedBasket() {
        return currentBasket != null;
    }

    private void checkIfBasketAssigned() {
        if (!hasOpenedBasket()) {
            throw new MarketCheckoutRuntimeException("Fatal error. No basket assigned to Checkout.");
        }
    }

}
