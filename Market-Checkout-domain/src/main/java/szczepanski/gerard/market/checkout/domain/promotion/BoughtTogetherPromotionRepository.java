package szczepanski.gerard.market.checkout.domain.promotion;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.List;
import java.util.Set;

public interface BoughtTogetherPromotionRepository extends JpaRepository<BoughtTogetherPromotion, Long> {

    @Query("SELECT b.code "
            + "FROM BoughtTogetherPromotion b "
            + "ORDER BY b.id DESC")
    List<Code> getLatestCode(Pageable pageable);

    @Query("SELECT b "
            + "FROM BoughtTogetherPromotion b "
            + "WHERE b.isActive = TRUE")
    Set<BoughtTogetherPromotion> findAllActivePromotions();
}
