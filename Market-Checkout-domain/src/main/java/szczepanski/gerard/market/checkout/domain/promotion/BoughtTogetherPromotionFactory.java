package szczepanski.gerard.market.checkout.domain.promotion;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.item.ItemRepository;

import java.util.Set;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BoughtTogetherPromotionFactory {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(BoughtTogetherPromotionFactory.class.getName());

    private final BoughtTogetherPromotionCodeGenerator codeGenerator;
    private final ItemRepository itemRepository;

    /**
     * Create BoughtTogetherPromotion object for persist into DB.
     * This promotion is active by default.
     *
     * @param name               - Name of the promotion.
     * @param discount           - Discount for promoted items when promotion is applied.
     * @param promotionItemCodes - Items codes required for promotion.
     * @return BoughtTogetherPromotion object for persist into DB.
     */
    public BoughtTogetherPromotion createBoughtTogetherPromotion(Name name, Discount discount, Set<Code> promotionItemCodes) {
        ParamAssertion.guardIsNotNull(name, discount, promotionItemCodes);
        LOG.debug("Create BoughtTogetherPromotion with name: %s, discount %s. Promotion require items with codes: %s", name, discount, promotionItemCodes);

        Code code = codeGenerator.generateNextCode();
        Set<Item> itemsForPromotion = itemRepository.findAllForCodes(promotionItemCodes);

        return BoughtTogetherPromotion.builder()
                .code(code)
                .name(name)
                .discount(discount)
                .requiredItems(itemsForPromotion)
                .isActive(true)
                .build();
    }

}
