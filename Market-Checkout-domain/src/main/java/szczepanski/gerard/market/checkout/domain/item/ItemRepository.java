package szczepanski.gerard.market.checkout.domain.item;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.List;
import java.util.Optional;
import java.util.Set;


public interface ItemRepository extends JpaRepository<Item, Long> {

    @Query("SELECT i.code "
            + "FROM Item i "
            + "ORDER BY i.id DESC")
    List<Code> getLatestCode(Pageable pageable);

    @Query("SELECT i "
            + "FROM Item i "
            + "WHERE i.code = :code")
    Optional<Item> findOneByCode(@Param("code") Code code);

    @Query("SELECT CASE WHEN COUNT(i.id) != 0 THEN TRUE ELSE FALSE END "
            + "FROM Item i "
            + "WHERE i.code = :code")
    Boolean isItemWithCodeInMarket(@Param("code") Code code);

    @Query("SELECT i "
            + "FROM Item i "
            + "WHERE i.code IN (:itemCodes)")
    Set<Item> findAllForCodes(@Param("itemCodes") Set<Code> itemCodes);
}
