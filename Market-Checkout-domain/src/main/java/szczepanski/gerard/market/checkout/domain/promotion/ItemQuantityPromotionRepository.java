package szczepanski.gerard.market.checkout.domain.promotion;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.List;
import java.util.Set;

public interface ItemQuantityPromotionRepository extends JpaRepository<ItemQuantityPromotion, Long> {

    @Query("SELECT i.code "
            + "FROM ItemQuantityPromotion i "
            + "ORDER BY i.id DESC")
    List<Code> getLatestCode(Pageable pageable);

    @Query("SELECT i "
            + "FROM ItemQuantityPromotion i "
            + "WHERE i.isActive = TRUE")
    Set<ItemQuantityPromotion> findAllActivePromotions();

}
