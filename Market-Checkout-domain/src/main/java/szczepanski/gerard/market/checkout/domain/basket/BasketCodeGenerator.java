package szczepanski.gerard.market.checkout.domain.basket;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import szczepanski.gerard.market.checkout.domain.common.AbstractCodeGenerator;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.List;
import java.util.Optional;

@Component
class BasketCodeGenerator extends AbstractCodeGenerator {

    public static final String BASKET_CODE_PREFIX = "b-";
    public static final String BASKET_CODE_PATTERN = "%04d";
    public static final Code INIT_BASKET_CODE = Code.of("b-0000");

    private final BasketRepository basketRepository;

    @Autowired
    public BasketCodeGenerator(BasketRepository basketRepository) {
        super(INIT_BASKET_CODE, BASKET_CODE_PREFIX, BASKET_CODE_PATTERN);
        this.basketRepository = basketRepository;
    }

    @Override
    protected Optional<Code> getLatestCodeFromRepository() {
        List<Code> latestCode = basketRepository.getLatestCode(new PageRequest(0, 1));

        if (latestCode.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(latestCode.get(0));
    }

}
