package szczepanski.gerard.market.checkout.domain.basket;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.promotion.*;

import java.util.Set;
import java.util.UUID;

/**
 * This factory creates new Basket aggregate.
 * <br>
 * Created Basket has full informations about
 * Promotions or discounts, but only when it is in OPEN state.
 *
 * @author Gerard Szczepa�ski
 */
@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class BasketFactory {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(BasketFactory.class.getName());

    private final ItemQuantityPromotionRepository itemQuantityPromotionRepository;
    private final BoughtTogetherPromotionRepository boughtTogetherPromotionRepository;
    private final DiscountFromTotalBasketPriceRepository discountFromTotalCheckoutPriceRepository;

    public Basket create() {
        Code code = Code.of(UUID.randomUUID().toString());
        LOG.info("Create new Basket with code: %s", code);

        Set<ItemQuantityPromotion> activeItemQuantityPromotions = itemQuantityPromotionRepository.findAllActivePromotions();
        Set<BoughtTogetherPromotion> activeBoughtTogetherPromotions = boughtTogetherPromotionRepository.findAllActivePromotions();
        Set<DiscountFromTotalBasketPrice> activeTotaPriceDiscounts = discountFromTotalCheckoutPriceRepository.findAllActiveDiscounts();

        return new Basket(code, activeItemQuantityPromotions, activeBoughtTogetherPromotions, activeTotaPriceDiscounts);
    }

}
