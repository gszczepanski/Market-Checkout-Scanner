package szczepanski.gerard.market.checkout.domain.promotion;

import lombok.*;
import szczepanski.gerard.market.checkout.domain.common.AbstractEntity;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.item.Item;

import javax.persistence.*;
import java.util.Set;

/**
 * Promotion for items bought together.
 * <br>
 * For that items discount is being applied.
 * <br><br>
 * "Some items are cheaper when bought together - buy item X with item Y and save Z cents".
 *
 * @author Gerard Szczepa�ski
 */
@Entity
@Table(name = "BOUGHT_TOGETHER_PROMOTION")
@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class BoughtTogetherPromotion extends AbstractEntity {
    private static final long serialVersionUID = 8812088294414173839L;

    @Column(name = "CODE", nullable = false, unique = true)
    private Code code;

    @Column(name = "NAME", nullable = false, unique = true)
    private Name name;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "BOUGHT_TOGETHER_PROMOTION_ITEM_JOIN_TABLE",
            joinColumns = {@JoinColumn(name = "BOUGHT_TOGETHER_PROMO_ID")},
            inverseJoinColumns = {
                    @JoinColumn(name = "ITEM_ID")})
    private Set<Item> requiredItems;

    @Getter
    @Column(name = "DISCOUNT", nullable = false)
    private Discount discount;

    /**
     * Check if givens item are enough for promotion.
     */
    public boolean requiredItemsMet(Set<Item> items) {
        return items.containsAll(requiredItems);
    }

    public boolean isForItem(Item item) {
        return requiredItems.contains(item);
    }

}
