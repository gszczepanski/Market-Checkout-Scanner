package szczepanski.gerard.market.checkout.domain.item;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ItemFactory {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(ItemFactory.class.getName());

    private final ItemCodeGenerator itemCodeGenerator;

    /**
     * Create Item with valid code for persist into DB.
     *
     * @param name         - Name of the item.
     * @param regularPrice - Regular price of the item.
     * @return Item object for DB persist.
     */
    public Item createItem(Name name, MonetaryValue regularPrice) {
        ParamAssertion.guardIsNotNull(name, regularPrice);
        LOG.debug("Create Item for name: %s and regularPrice: %s", name, regularPrice);

        Code code = itemCodeGenerator.generateNextCode();

        return Item.builder()
                .code(code)
                .name(name)
                .regularPrice(regularPrice)
                .build();
    }

}
