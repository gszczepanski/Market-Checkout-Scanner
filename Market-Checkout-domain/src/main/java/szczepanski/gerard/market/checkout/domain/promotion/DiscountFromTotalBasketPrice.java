package szczepanski.gerard.market.checkout.domain.promotion;

import lombok.*;
import szczepanski.gerard.market.checkout.domain.common.AbstractEntity;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Special discount from total Basket Price. If total price with promotions reach
 * some total value (for example is above 600.00 USD), then some discount is applied to total
 * value of the Basket.
 * <br>
 *
 * @author Gerard Szczepa�ski
 */
@Entity
@Table(name = "DISCOUNT_FROM_TOTAL_BASKET_PRICE")
@Getter
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class DiscountFromTotalBasketPrice extends AbstractEntity {
    private static final long serialVersionUID = -7925665682561519003L;

    @Column(name = "CODE", nullable = false, unique = true)
    private Code code;

    @Column(name = "IS_ACTIVE")
    private Boolean isActive = true;

    @Getter
    @Column(name = "REQUIRED_BASKET_TOTAL_VALUE", nullable = false)
    private MonetaryValue requiredBasketTotalValue;

    @Getter
    @Column(name = "DISCOUNT", nullable = false)
    private Discount discount;

    public Boolean areDiscountConditionsMet(MonetaryValue totalValue) {
        return totalValue.compareTo(this.requiredBasketTotalValue) == 1 || totalValue.compareTo(this.requiredBasketTotalValue) == 0;
    }

}
