package szczepanski.gerard.market.checkout.domain.promotion;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.List;
import java.util.Set;

public interface DiscountFromTotalBasketPriceRepository extends JpaRepository<DiscountFromTotalBasketPrice, Long> {

    @Query("SELECT d.code "
            + "FROM DiscountFromTotalBasketPrice d "
            + "ORDER BY d.id DESC")
    List<Code> getLatestCode(Pageable pageable);

    @Query("SELECT d "
            + "FROM DiscountFromTotalBasketPrice d "
            + "WHERE d.isActive = TRUE")
    Set<DiscountFromTotalBasketPrice> findAllActiveDiscounts();
}
