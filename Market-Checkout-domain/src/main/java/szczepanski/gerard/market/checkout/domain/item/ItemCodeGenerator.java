package szczepanski.gerard.market.checkout.domain.item;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Component;
import szczepanski.gerard.market.checkout.domain.common.AbstractCodeGenerator;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.List;
import java.util.Optional;

@Component
class ItemCodeGenerator extends AbstractCodeGenerator {

    public static final String ITEM_CODE_PREFIX = "item-";
    public static final String ITEM_CODE_PATTERN = "%04d";
    public static final Code INIT_ITEM_CODE = Code.of("item-0000");

    private final ItemRepository itemRepository;

    @Autowired
    public ItemCodeGenerator(ItemRepository itemRepository) {
        super(INIT_ITEM_CODE, ITEM_CODE_PREFIX, ITEM_CODE_PATTERN);
        this.itemRepository = itemRepository;
    }

    @Override
    protected Optional<Code> getLatestCodeFromRepository() {
        List<Code> latestCode = itemRepository.getLatestCode(new PageRequest(0, 1));

        if (latestCode.isEmpty()) {
            return Optional.empty();
        }
        return Optional.of(latestCode.get(0));
    }

}
