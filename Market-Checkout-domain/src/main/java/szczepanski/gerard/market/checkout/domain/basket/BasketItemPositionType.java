package szczepanski.gerard.market.checkout.domain.basket;

/**
 * This enum describes whether BasketItemPosition
 * refers to Item or multipriced promotion for that Item.
 *
 * @author Gerard Szczepa�ski
 */
public enum BasketItemPositionType {

    ITEM,
    MULTIPRICED_PROMOTION;

}
