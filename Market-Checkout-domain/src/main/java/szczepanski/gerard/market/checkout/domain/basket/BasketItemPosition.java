package szczepanski.gerard.market.checkout.domain.basket;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.exception.MarketCheckoutRuntimeException;
import szczepanski.gerard.market.checkout.domain.common.AbstractEntity;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValueCalculator;
import szczepanski.gerard.market.checkout.domain.promotion.Discount;
import szczepanski.gerard.market.checkout.domain.promotion.ItemQuantityPromotion;

import javax.persistence.*;

/**
 * BasketItemPosition represents item position in Basket aggregate.
 * <br><br>
 * This position is further printed on recipe.
 * <br><br>
 * BasketItemPosition can have applied ItemQuantityPromotion. Then price for position
 * is being taken from promotion. If promotion is applied for the part of the BasketItemPromotion
 * then BasketItemPosition is being splitted.
 * <br><br>
 * For example:
 * <br>
 * 1) BasketItemPosition for Item 'Milk' | Quantity: 3| Applied promotion for 3 Milks| Price 10.00 USD
 * <br>
 * 2) BasketItemPosition for Item 'Milk' | Quantity: 2| Price 8.00 USD
 * <br><br>
 * First BasketItemPosition for Item 'Milk" has applied promotion form 3 milks. Second BasketItemPosition has
 * milk with regular item price. If next Item 'Milk would be scanned, then promotion would be applied to second
 * BasketItemPosition (gained 3 milks).
 *
 * @author Gerard Szczepanski
 */
@Entity
@Table(name = "CHECKOUT_ITEM_POSITION")
@NoArgsConstructor(access = AccessLevel.PACKAGE)
class BasketItemPosition extends AbstractEntity {
    private static final long serialVersionUID = -6435016375985887486L;

    @OneToOne
    @JoinColumn(name = "CHECKOUT_ID")
    protected Basket basket;

    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    protected Item item;

    @Getter
    @ManyToOne
    @JoinColumn(name = "ITEM_QUANTITY_PROMOTION_ID")
    protected ItemQuantityPromotion promotion;

    @Enumerated(EnumType.STRING)
    @JoinColumn(name = "TYPE")
    protected BasketItemPositionType type;

    @Getter(value = AccessLevel.PROTECTED)
    @Column(name = "QUANTITY")
    protected Integer quantity = 1;

    @Getter(value = AccessLevel.PROTECTED)
    @Column(name = "CHECKOUT_ITEM_PRICE")
    protected MonetaryValue price;

    static BasketItemPosition create(Basket forBasket, Item item) {
        BasketItemPosition checkoutItem = new BasketItemPosition();

        checkoutItem.basket = forBasket;
        checkoutItem.item = item;
        checkoutItem.type = BasketItemPositionType.ITEM;

        return checkoutItem;
    }

    void increaseItemQuantity() {
        this.quantity++;
    }

    void recalcuate() {
        MonetaryValue priceForOneBasketItemPosition = item.getRegularPrice();
        if (isMultipricedPromotionPosition()) {
            priceForOneBasketItemPosition = this.promotion.getPromotionPrice();
        }
        this.price = MonetaryValueCalculator.multiply(priceForOneBasketItemPosition, quantity);
    }

    void applyDiscount(Discount discount) {
        this.price = MonetaryValueCalculator.applyDiscount(this.price, discount);
    }

    Boolean isForItem(Item item) {
        return this.item.getCode().equals(item.getCode());
    }

    Boolean isItemPosition() {
        return this.type == BasketItemPositionType.ITEM;
    }

    Boolean isMultipricedPromotionPosition() {
        return this.type == BasketItemPositionType.MULTIPRICED_PROMOTION;
    }

    Boolean wasDiscountApplied() {
        MonetaryValue priceWithoutDiscounts = calculatePriceWithoutDiscounts();
        return !priceWithoutDiscounts.equals(this.price);
    }

    MonetaryValue getDiscountMonetaryValue() {
        MonetaryValue priceWithoutDiscounts = calculatePriceWithoutDiscounts();
        return MonetaryValueCalculator.subtract(priceWithoutDiscounts, this.price);
    }

    private MonetaryValue calculatePriceWithoutDiscounts() {
        if (isItemPosition()) {
            return MonetaryValueCalculator.multiply(this.item.getRegularPrice(), this.quantity);
        } else {
            return MonetaryValueCalculator.multiply(this.promotion.getPromotionPrice(), this.quantity);
        }
    }

    Name getName() {
        if (isItemPosition()) {
            return this.item.getName();
        }
        return this.promotion.getName();
    }

    Code getCode() {
        if (isItemPosition()) {
            return this.item.getCode();
        }
        return this.promotion.getCode();
    }

    /**
     * Absorb another BasketItemPosition for current BasketItemPosition that represents promotion.
     * <br>
     * BasketItemPosition for absorb must be BasketItemPosition for item!
     * <br>
     * Absorbing BasketItemPosition must be for promotion.
     * <br>
     * Absorbed BasketItemPosition must be removed from Basket positions after absorbing!
     * Absorbing BasketItemPosition quantity will be increased after operation.
     */
    void absorbBasketItemPositionForPromotion(BasketItemPosition basketItemPosition) {
        ParamAssertion.guardIsNotNull(basketItemPosition);
        checkInvariantsForAbsorbing(basketItemPosition);

        this.quantity++;
        recalcuate();
    }

    void checkInvariantsForAbsorbing(BasketItemPosition basketItemPosition) {
        if (this.isItemPosition()) {
            throw new MarketCheckoutRuntimeException("Absorbing BasketItemPosition must represent multipriced promotion!");
        }

        if (basketItemPosition.isMultipricedPromotionPosition()) {
            throw new MarketCheckoutRuntimeException("Absorbed BasketItemPosition must represent standard item!");
        }

        if (!this.isForItem(basketItemPosition.item)) {
            throw new MarketCheckoutRuntimeException("Absorbed and Absorbing BasketItemPosition items do nt match. Operation allowed only for the Item and ItemQuantityPromotion for that Item!");
        }

        if (!this.promotion.areQuantityRequirementsMet(basketItemPosition.getQuantity())) {
            throw new MarketCheckoutRuntimeException("Absorbed BasketItemPosition quantity requirements do not met!");
        }
    }

    /**
     * Convert current BasketItemPosition for standard Item, to
     * BasketItemPosition for ItemQuantityPromotion for that Item.
     * <br><br>
     * For example, if ItemQuantityPromotion is for Item 'Tuna' and has 3 quantity,
     * and we have ItemQuantityPromotion for 3 tunas, then we convert this BasketItemPosition for
     * applied promotion BasketItemPosition. Quantity of that promotion is 1.
     */
    void convertForPromotionBasketItemPosition(ItemQuantityPromotion promotion) {
        checkInvariantsForPromotionConverting(promotion);

        this.promotion = promotion;
        this.quantity = 1;
        this.type = BasketItemPositionType.MULTIPRICED_PROMOTION;
        recalcuate();
    }

    private void checkInvariantsForPromotionConverting(ItemQuantityPromotion promotion) {
        if (this.isMultipricedPromotionPosition()) {
            throw new MarketCheckoutRuntimeException("This BasketItemPosition is already for promotion!");
        }

        if (!promotion.isForItem(this.item)) {
            throw new MarketCheckoutRuntimeException("Promotion and Item for promotion do not match!");
        }

        if (!promotion.areQuantityRequirementsMet(quantity)) {
            throw new MarketCheckoutRuntimeException("BasketItemPosition quantity requirements do not met!");
        }
    }

}
