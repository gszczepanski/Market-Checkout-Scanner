package szczepanski.gerard.market.checkout.domain.common;

import org.springframework.transaction.annotation.Transactional;
import szczepanski.gerard.market.checkout.component.common.utils.MarketCheckoutStringUtils;

import java.util.Optional;

public abstract class AbstractCodeGenerator {

    private final Code initCode;
    private final String codePrefix;
    private final String codePattern;

    public AbstractCodeGenerator(Code initCode, String codePrefix, String codePattern) {
        this.initCode = initCode;
        this.codePrefix = codePrefix;
        this.codePattern = codePattern;
    }

    @Transactional(readOnly = true)
    public Code generateNextCode() {
        Optional<Code> optionalLatestCodeFromRepository = getLatestCodeFromRepository();

        String nextCode;
        if (optionalLatestCodeFromRepository.isPresent()) {
            nextCode = getIncrementedCode(optionalLatestCodeFromRepository.get());
        } else {
            nextCode = getIncrementedCode(initCode);
        }

        return Code.of(nextCode);
    }

    private final String getIncrementedCode(Code latestCode) {
        String codeNumber = latestCode.toString().replace(codePrefix, MarketCheckoutStringUtils.EMPTY);
        Integer codeNumberAsInteger = Integer.valueOf(codeNumber);

        Integer incrementedNumber = codeNumberAsInteger + 1;
        return codePrefix + String.format(codePattern, incrementedNumber);
    }

    protected abstract Optional<Code> getLatestCodeFromRepository();

}
