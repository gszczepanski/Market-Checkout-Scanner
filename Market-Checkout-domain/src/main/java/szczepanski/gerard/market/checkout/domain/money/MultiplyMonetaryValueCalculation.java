package szczepanski.gerard.market.checkout.domain.money;

import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;

import java.math.BigDecimal;

/**
 * This class represents multiply operation of MonetaryValue value.
 *
 * @author Gerard Szczepa�ski
 */
class MultiplyMonetaryValueCalculation implements Calculation {

    private static final Integer MINIMUM_MULTIPLY_TIMES_VALUE = 0;

    private final MonetaryValue monetaryValue;
    private final Integer times;

    public MultiplyMonetaryValueCalculation(MonetaryValue monetaryValue, Integer times) {
        ParamAssertion.guardIsNotNull(monetaryValue, times);
        ParamAssertion.guardIsMinValueValid(times, MINIMUM_MULTIPLY_TIMES_VALUE);
        this.monetaryValue = monetaryValue;
        this.times = times;
    }

    @Override
    public MonetaryValue calculate() {
        BigDecimal baseValue = monetaryValue.getValue();
        BigDecimal resultValue = baseValue.multiply(new BigDecimal(times));

        Currency resultCurrency = monetaryValue.getCurrency();
        return MonetaryValue.of(resultValue, resultCurrency);
    }

}
