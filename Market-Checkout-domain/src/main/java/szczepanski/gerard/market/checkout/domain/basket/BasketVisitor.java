package szczepanski.gerard.market.checkout.domain.basket;

@FunctionalInterface
public interface BasketVisitor {

    void visit(Basket basket);

}
