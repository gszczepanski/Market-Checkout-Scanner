package szczepanski.gerard.market.checkout.domain.basket;

import lombok.Getter;
import szczepanski.gerard.market.checkout.component.common.program.MarketCheckoutConstants;
import szczepanski.gerard.market.checkout.domain.basket.Recipe.BasketRecipeBuilder;
import szczepanski.gerard.market.checkout.domain.promotion.BoughtTogetherPromotion;
import szczepanski.gerard.market.checkout.domain.promotion.DiscountFromTotalBasketPrice;

import java.time.format.DateTimeFormatter;
import java.util.Set;

/**
 * Creates basic Recipe object for given Basket.
 *
 * @author Gerard Szczepa�ski
 */
final class BasketBasicRecipeCreationVisitor implements BasketVisitor {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm");

    private static final String HORIZONTAL_SEPARATOR = "----------";

    private static final String DATE_TEMPLATE = "Date: %s";
    private static final String RECIPE_CODE_TEMPLATE = "Recipe code: %s";
    private static final String CHECKOUT_ITEM_POSITION_TEMPLATE = "# %s x%s | %s\n%s";
    private static final String CHECKOUT_ITEM_POSITION_WITH_APPLIED_DISCOUNT_TEMPLATE = "# %s x%s | %s\n%s discount: -%s";

    private static final String DISCOUNT_LINE = "%s discount %s";
    private static final String TOTAL_PURCHASE_DISCOUNT_LINE = "Purchase above %s discount %s";

    private static final String TOTAL_LINE_TEMPLATE = "TOTAL: %s";

    @Getter
    private Recipe generatedRecipe;

    @Override
    public void visit(Basket basket) {
        BasketRecipeBuilder recipeBuilder = BasketRecipeBuilder.recipe();
        recipeBuilder.line(MarketCheckoutConstants.MARKET_NAME);

        recipeBuilder.emptyLine();
        recipeBuilder.line(String.format(DATE_TEMPLATE, DATE_TIME_FORMATTER.format(basket.date)));
        recipeBuilder.line(String.format(RECIPE_CODE_TEMPLATE, basket.getCode()));

        recipeBuilder.emptyLine();
        basket.basketPositions.forEach(position -> addCheckoutItemPositionLine(recipeBuilder, position));
        recipeBuilder.line(HORIZONTAL_SEPARATOR);

        addDiscountsLinesIfDiscountWasAppliedToCheckout(recipeBuilder, basket);
        recipeBuilder.line(String.format(TOTAL_LINE_TEMPLATE, basket.totalPrice()));

        this.generatedRecipe = recipeBuilder.print();
    }

    private void addCheckoutItemPositionLine(BasketRecipeBuilder recipeBuilder, BasketItemPosition position) {
        String checkoutItemPositionLine = null;
        if (position.wasDiscountApplied()) {
            checkoutItemPositionLine = String.format(CHECKOUT_ITEM_POSITION_WITH_APPLIED_DISCOUNT_TEMPLATE, position.getName(), position.getQuantity(), position.getPrice(), position.getCode(), position.getDiscountMonetaryValue());
        } else {
            checkoutItemPositionLine = String.format(CHECKOUT_ITEM_POSITION_TEMPLATE, position.getName(), position.getQuantity(), position.getPrice(), position.getCode());
        }

        recipeBuilder.line(checkoutItemPositionLine);
    }

    private void addDiscountsLinesIfDiscountWasAppliedToCheckout(BasketRecipeBuilder recipeBuilder, Basket checkout) {
        Set<BoughtTogetherPromotion> appliedBoughtTogetherPromotions = checkout.appliedBoughtTogetherPromotions;
        boolean wasAnyDiscountApplied = !appliedBoughtTogetherPromotions.isEmpty() || checkout.wasTotaPriceDiscountApplied();

        if (!appliedBoughtTogetherPromotions.isEmpty()) {

            appliedBoughtTogetherPromotions.forEach(promotion -> {
                recipeBuilder.line(String.format(DISCOUNT_LINE, promotion.getName(), promotion.getDiscount()));
            });
        }

        if (checkout.wasTotaPriceDiscountApplied()) {
            DiscountFromTotalBasketPrice appliedTotaPriceDiscount = checkout.appliedTotaPriceDiscount;
            recipeBuilder.line(String.format(TOTAL_PURCHASE_DISCOUNT_LINE, appliedTotaPriceDiscount.getRequiredBasketTotalValue(), appliedTotaPriceDiscount.getDiscount()));
        }

        if (wasAnyDiscountApplied) {
            recipeBuilder.line(HORIZONTAL_SEPARATOR);
        }
    }
}
