package szczepanski.gerard.market.checkout.domain.basket;

import lombok.Getter;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.promotion.DiscountFromTotalBasketPrice;
import szczepanski.gerard.market.checkout.domain.warehouse.SalesInformation;

import java.util.HashMap;
import java.util.Map;

public class BasketStorageCounterVisitor implements BasketVisitor {

    @Getter
    private Map<Code, SalesInformation> salesInfoBasedOnBasketMap;

    @Override
    public void visit(Basket basket) {
        ParamAssertion.guardIsNotNull(basket);
        ParamAssertion.guardIsTrue(basket.isClosed());

        this.salesInfoBasedOnBasketMap = new HashMap<>();
        visitBasketItemPoitions(basket);
        visitBoughtTogetherPromotions(basket);
        visitBasketTotalPriceDiscount(basket);
    }

    private void visitBasketItemPoitions(Basket basket) {
        basket.basketPositions.forEach(basketItemPosition -> {
            Code code = basketItemPosition.getCode();

            if (this.salesInfoBasedOnBasketMap.containsKey(code)) {
                updateBasketItemPositionOnMap(basketItemPosition);
            } else {
                addBasketItemPositionToMap(basketItemPosition);
            }
        });
    }

    private void updateBasketItemPositionOnMap(BasketItemPosition basketItemPosition) {
        Code code = basketItemPosition.getCode();
        SalesInformation salesInformation = this.salesInfoBasedOnBasketMap.get(code);
        salesInformation.updateQuantity(basketItemPosition.quantity);
    }

    private void addBasketItemPositionToMap(BasketItemPosition basketItemPosition) {
        Code code = basketItemPosition.getCode();
        SalesInformation salesInformation = null;

        if (basketItemPosition.isItemPosition()) {
            salesInformation = SalesInformation.forItem(basketItemPosition.item, basketItemPosition.quantity);
        } else {
            salesInformation = SalesInformation.forItemQuantityPromotion(basketItemPosition.promotion, basketItemPosition.quantity);
        }

        this.salesInfoBasedOnBasketMap.put(code, salesInformation);
    }

    private void visitBoughtTogetherPromotions(Basket basket) {
        basket.appliedBoughtTogetherPromotions.forEach(boughtTogetherPromotion -> {
            Code code = boughtTogetherPromotion.getCode();
            SalesInformation salesInformation = SalesInformation.forBoughtTogetherPromotion(boughtTogetherPromotion);
            this.salesInfoBasedOnBasketMap.put(code, salesInformation);
        });
    }

    private void visitBasketTotalPriceDiscount(Basket basket) {
        if (basket.wasTotalPriceDiscountApplied()) {
            DiscountFromTotalBasketPrice appliedTotaPriceDiscount = basket.appliedTotaPriceDiscount;
            Code code = appliedTotaPriceDiscount.getCode();
            SalesInformation salesInformation = SalesInformation.forDiscountFromTotalPrice(appliedTotaPriceDiscount);
            this.salesInfoBasedOnBasketMap.put(code, salesInformation);
        }
    }

}
