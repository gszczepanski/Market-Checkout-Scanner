package szczepanski.gerard.market.checkout.domain.warehouse;

public enum SalesInformationType {

    ITEM,
    ITEM_QUANTITY_PROMOTION,
    BOUGHT_TOGETHER_PROMOTION,
    TOTAL_BASKET_VALUE_DISCOUNT;

}
