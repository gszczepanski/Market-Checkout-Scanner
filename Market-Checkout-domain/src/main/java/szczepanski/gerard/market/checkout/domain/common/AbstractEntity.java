package szczepanski.gerard.market.checkout.domain.common;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import java.io.Serializable;

@Setter
@Getter
@MappedSuperclass
public class AbstractEntity implements Serializable {

    private static final long serialVersionUID = 1811205213978726766L;

    @Id
    @GeneratedValue
    protected Long id;

}
