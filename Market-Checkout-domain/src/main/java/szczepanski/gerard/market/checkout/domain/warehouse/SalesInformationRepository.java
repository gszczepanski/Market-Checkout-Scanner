package szczepanski.gerard.market.checkout.domain.warehouse;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import szczepanski.gerard.market.checkout.domain.common.Code;

import java.util.Set;


public interface SalesInformationRepository extends JpaRepository<SalesInformation, Long> {

    @Query("SELECT s FROM SalesInformation s "
            + "LEFT JOIN s.item i "
            + "LEFT JOIN s.itemQuantityPromotion iqp "
            + "LEFT JOIN s.boughtTogetherPromotion btp "
            + "LEFT JOIN s.boughtTogetherPromotion dftp "
            + "WHERE "
            + "(i IS NULL OR i.code IN :codes) "
            + "AND (iqp IS NULL OR iqp.code IN :codes) "
            + "AND (btp IS NULL OR btp.code IN :codes) "
            + "AND (dftp IS NULL OR dftp.code IN :codes)")
    Set<SalesInformation> findAllByCodes(@Param("codes") Set<Code> codes);

}
