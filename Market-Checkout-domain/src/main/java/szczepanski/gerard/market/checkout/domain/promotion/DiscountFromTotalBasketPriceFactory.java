package szczepanski.gerard.market.checkout.domain.promotion;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class DiscountFromTotalBasketPriceFactory {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(DiscountFromTotalBasketPriceFactory.class.getName());

    private final DiscountFromTotalBasketPriceCodeGenerator codeGenerator;

    /**
     * Create DiscountFromTotalBasketPrice with valid code for persist into DB.
     * Created discount is active by default.
     *
     * @param fromBasketPrice - Required Basket total price to apply discount.
     * @param discount        - Discount to apply for Basket.
     * @return DiscountFromTotalBasketPrice object for DB persist.
     */
    public DiscountFromTotalBasketPrice createTotalDiscount(MonetaryValue fromBasketPrice, Discount discount) {
        ParamAssertion.guardIsNotNull(fromBasketPrice, discount);
        LOG.debug("Create DiscountFromTotalBasketPrice from basket price: %s. Discount: %s", fromBasketPrice, discount);

        Code code = codeGenerator.generateNextCode();

        return DiscountFromTotalBasketPrice.builder()
                .code(code)
                .discount(discount)
                .isActive(true)
                .requiredBasketTotalValue(fromBasketPrice)
                .build();
    }

}
