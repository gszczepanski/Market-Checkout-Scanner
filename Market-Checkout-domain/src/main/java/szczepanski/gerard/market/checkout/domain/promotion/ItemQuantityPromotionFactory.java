package szczepanski.gerard.market.checkout.domain.promotion;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.item.ItemRepository;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
public class ItemQuantityPromotionFactory {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(ItemQuantityPromotionFactory.class.getName());

    private final ItemQuantityPromotionCodeGenerator codeGenerator;
    private final ItemRepository itemRepository;

    /**
     * Create ItemQuantityPromotion with valid code for persist into DB.
     * Created promotion is active by default.
     *
     * @param nameOfThePromotion   - Name of the promotion.
     * @param codeOfPromotedItem   - code of promoted item. Promotion is for item. Buy X item and get promotion.
     * @param requiredItemQuantity - Required item quantity to fit into promotion.
     * @param priceForItems        - Price of the X items when promotion is applied.
     * @return ItemQuantityPromotion object for DB persist.
     */
    public ItemQuantityPromotion createItemQuantityPromotion(Name nameOfThePromotion, Code codeOfPromotedItem, Integer requiredItemQuantity, MonetaryValue priceForItems) {
        ParamAssertion.guardIsNotNull(nameOfThePromotion, codeOfPromotedItem, requiredItemQuantity, priceForItems);
        LOG.debug("Create ItemQuantityPromotion with name: '%s', item: %s with required quantity %s. Promo price: %s", nameOfThePromotion, codeOfPromotedItem, requiredItemQuantity, priceForItems);

        Code code = codeGenerator.generateNextCode();
        Item promotedItem = itemRepository.findOneByCode(codeOfPromotedItem).get();

        return ItemQuantityPromotion.builder()
                .code(code)
                .name(nameOfThePromotion)
                .promotedItem(promotedItem)
                .requiredItemQuantity(requiredItemQuantity)
                .promotionPrice(priceForItems)
                .isActive(true)
                .build();
    }

}
