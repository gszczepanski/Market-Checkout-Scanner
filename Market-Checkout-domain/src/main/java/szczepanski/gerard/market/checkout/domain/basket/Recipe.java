package szczepanski.gerard.market.checkout.domain.basket;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.utils.MarketCheckoutStringUtils;

import java.io.Serializable;

/**
 * CheckoutRecipe value object.
 * <br><br>
 * This object is designed for recipe printing. It is created when
 * Checkout object is being closed and stored in Database.
 * <br>
 *
 * @author Gerard Szczepa�ski
 */
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public final class Recipe implements Serializable {
    private static final long serialVersionUID = 4165095513829049938L;

    @Getter
    private final String recipeText;

    /**
     * Use this method only in JPA converter.
     * <br>
     * For creation in Checkout use CheckoutRecipeBuilder!
     */
    public static Recipe of(String recipeText) {
        ParamAssertion.guardIsNotBlank(recipeText);
        return new Recipe(recipeText);
    }

    @Override
    public String toString() {
        return "CheckoutRecipe [" + recipeText + "]";
    }

    static class BasketRecipeBuilder {

        private StringBuilder recipeTextBuilder = new StringBuilder(MarketCheckoutStringUtils.EMPTY);

        public static BasketRecipeBuilder recipe() {
            return new BasketRecipeBuilder();
        }

        public BasketRecipeBuilder line(String text) {
            ParamAssertion.guardIsNotBlank(text);
            this.recipeTextBuilder.append(text).append(MarketCheckoutStringUtils.NEXT_LINE);
            return this;
        }

        public BasketRecipeBuilder emptyLine() {
            this.recipeTextBuilder.append(MarketCheckoutStringUtils.NEXT_LINE);
            return this;
        }

        public Recipe print() {
            return new Recipe(this.recipeTextBuilder.toString());
        }
    }

}
