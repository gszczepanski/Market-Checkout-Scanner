package szczepanski.gerard.market.checkout.domain.item;

import lombok.*;
import szczepanski.gerard.market.checkout.domain.common.AbstractEntity;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Getter
@Entity
@Table(name = "ITEM")
@Builder
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class Item extends AbstractEntity {
    private static final long serialVersionUID = 4432504158384852740L;

    @Getter
    @Column(name = "CODE", nullable = false, unique = true)
    private Code code;

    @Column(name = "NAME", nullable = false)
    private Name name;

    @Column(name = "REGULAR_PRICE", nullable = false)
    private MonetaryValue regularPrice;

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((code == null) ? 0 : code.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Item other = (Item) obj;
        if (code == null) {
            if (other.code != null)
                return false;
        } else if (!code.equals(other.code))
            return false;
        return true;
    }

}
