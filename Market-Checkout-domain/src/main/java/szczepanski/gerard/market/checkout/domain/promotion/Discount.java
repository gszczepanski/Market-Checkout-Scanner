package szczepanski.gerard.market.checkout.domain.promotion;

import lombok.AccessLevel;
import lombok.RequiredArgsConstructor;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.utils.MarketCheckoutStringUtils;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * Value object that represents discount. Discount must be positive.
 * <br>
 * Example discounts are: 10%, 15.00%.
 *
 * @author Gerard Szczepa�ski
 */
@RequiredArgsConstructor(access = AccessLevel.PRIVATE)
public final class Discount implements Serializable {
    private static final long serialVersionUID = 2984494418382643487L;
    private static final BigDecimal ONE_HUNDRED_DECIMAL = new BigDecimal("100");

    private final String value;

    public static Discount of(String value) {
        ParamAssertion.guardIsPercentageStringValue(value);
        return new Discount(value);
    }

    public BigDecimal toDecimalValue() {
        String discountWithoutPercentageSign = value.replace(MarketCheckoutStringUtils.PERCENT_SIGN, MarketCheckoutStringUtils.EMPTY);
        BigDecimal decimalDiscount = new BigDecimal(discountWithoutPercentageSign);

        return decimalDiscount.divide(ONE_HUNDRED_DECIMAL);
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((value == null) ? 0 : value.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        Discount other = (Discount) obj;
        if (value == null) {
            if (other.value != null)
                return false;
        } else if (!value.equals(other.value))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return value;
    }

}
