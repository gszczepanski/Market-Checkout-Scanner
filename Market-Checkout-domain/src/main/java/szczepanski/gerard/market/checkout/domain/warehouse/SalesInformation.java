package szczepanski.gerard.market.checkout.domain.warehouse;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import szczepanski.gerard.market.checkout.domain.common.AbstractEntity;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.promotion.BoughtTogetherPromotion;
import szczepanski.gerard.market.checkout.domain.promotion.DiscountFromTotalBasketPrice;
import szczepanski.gerard.market.checkout.domain.promotion.ItemQuantityPromotion;

import javax.persistence.*;

@Getter
@Entity
@Table(name = "SALES_INFORMATION")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@AllArgsConstructor(access = AccessLevel.PROTECTED)
public class SalesInformation extends AbstractEntity {
    private static final long serialVersionUID = 8622537386553671271L;
    @Enumerated(EnumType.STRING)
    @JoinColumn(name = "TYPE")
    protected SalesInformationType type;
    @ManyToOne
    @JoinColumn(name = "ITEM_ID")
    private Item item;
    @ManyToOne
    @JoinColumn(name = "ITEM_QUANTITY_PROMOTION_ID")
    private ItemQuantityPromotion itemQuantityPromotion;
    @ManyToOne
    @JoinColumn(name = "BOUGHT_TOGETHER_PROMOTION_ID")
    private BoughtTogetherPromotion boughtTogetherPromotion;
    @ManyToOne
    @JoinColumn(name = "DISCOUNT_FROM_TOTAL_BASKET_PRICE_ID")
    private DiscountFromTotalBasketPrice discountFromTotalBasketPrice;
    @Column(name = "QUANTITY")
    private Integer quantity;

    public static SalesInformation forItem(Item item, Integer quantity) {
        SalesInformation salesInformation = new SalesInformation();

        salesInformation.item = item;
        salesInformation.quantity = quantity;
        salesInformation.type = SalesInformationType.ITEM;

        return salesInformation;
    }

    public static SalesInformation forItemQuantityPromotion(ItemQuantityPromotion itemQuantityPromotion, Integer quantity) {
        SalesInformation salesInformation = new SalesInformation();

        salesInformation.itemQuantityPromotion = itemQuantityPromotion;
        salesInformation.quantity = quantity;
        salesInformation.type = SalesInformationType.ITEM_QUANTITY_PROMOTION;

        return salesInformation;
    }

    public static SalesInformation forBoughtTogetherPromotion(BoughtTogetherPromotion boughtTogetherPromotion) {
        SalesInformation salesInformation = new SalesInformation();

        salesInformation.boughtTogetherPromotion = boughtTogetherPromotion;
        salesInformation.quantity = 1;
        salesInformation.type = SalesInformationType.BOUGHT_TOGETHER_PROMOTION;

        return salesInformation;
    }

    public static SalesInformation forDiscountFromTotalPrice(DiscountFromTotalBasketPrice discountFromTotalBasketPrice) {
        SalesInformation salesInformation = new SalesInformation();

        salesInformation.discountFromTotalBasketPrice = discountFromTotalBasketPrice;
        salesInformation.quantity = 1;
        salesInformation.type = SalesInformationType.TOTAL_BASKET_VALUE_DISCOUNT;

        return salesInformation;
    }

    public void updateQuantity(Integer quantity) {
        this.quantity += quantity;
    }

    public Code getCode() {
        Code code = null;
        switch (type) {
            case ITEM:
                code = this.item.getCode();
                break;
            case ITEM_QUANTITY_PROMOTION:
                code = this.itemQuantityPromotion.getCode();
                break;
            case BOUGHT_TOGETHER_PROMOTION:
                code = this.boughtTogetherPromotion.getCode();
                break;
            case TOTAL_BASKET_VALUE_DISCOUNT:
                code = this.discountFromTotalBasketPrice.getCode();
            default:
                break;
        }
        return code;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((boughtTogetherPromotion == null) ? 0 : boughtTogetherPromotion.hashCode());
        result = prime * result + ((discountFromTotalBasketPrice == null) ? 0 : discountFromTotalBasketPrice.hashCode());
        result = prime * result + ((item == null) ? 0 : item.hashCode());
        result = prime * result + ((itemQuantityPromotion == null) ? 0 : itemQuantityPromotion.hashCode());
        result = prime * result + ((quantity == null) ? 0 : quantity.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        SalesInformation other = (SalesInformation) obj;
        if (boughtTogetherPromotion == null) {
            if (other.boughtTogetherPromotion != null)
                return false;
        } else if (!boughtTogetherPromotion.equals(other.boughtTogetherPromotion))
            return false;
        if (discountFromTotalBasketPrice == null) {
            if (other.discountFromTotalBasketPrice != null)
                return false;
        } else if (!discountFromTotalBasketPrice.equals(other.discountFromTotalBasketPrice))
            return false;
        if (item == null) {
            if (other.item != null)
                return false;
        } else if (!item.equals(other.item))
            return false;
        if (itemQuantityPromotion == null) {
            if (other.itemQuantityPromotion != null)
                return false;
        } else if (!itemQuantityPromotion.equals(other.itemQuantityPromotion))
            return false;
        if (quantity == null) {
            if (other.quantity != null)
                return false;
        } else if (!quantity.equals(other.quantity))
            return false;
        if (type != other.type)
            return false;
        return true;
    }

    @Override
    public String toString() {
        return String.format("SalesInformation: code: %s, type: %s, quantity: %s", this.getCode(), this.type, this.quantity);
    }


}
