package szczepanski.gerard.market.checkout.web.item.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.service.common.ValidationException;
import szczepanski.gerard.market.checkout.service.item.ItemCreationDto;
import szczepanski.gerard.market.checkout.service.item.ItemManagmentService;
import szczepanski.gerard.market.checkout.service.item.ItemPresentationDto;

import java.util.Optional;

@RestController
@RequestMapping("/items")
@RequiredArgsConstructor
class MarketItemManagmentController {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(MarketItemManagmentController.class.getName());

    private final ItemManagmentService itemManagmentService;

    @ResponseBody
    @GetMapping(value = "/item/{itemCode}", produces = "application/json")
    public Object findItem(@PathVariable String itemCode) {
        LOG.info("Find item with code: ", itemCode);

        Optional<ItemPresentationDto> optionalItem = itemManagmentService.findOne(itemCode);
        if (optionalItem.isPresent()) {
            ItemPresentationDto item = optionalItem.get();
            return item;
        } else {
            LOG.info("Item with given code %s not found in Market", itemCode);
            return String.format("Item with given code %s not found in Market", itemCode);
        }
    }

    @ResponseBody
    @PostMapping(value = "/item/add", produces = "application/json")
    public Object addItem(@RequestBody ItemCreationDto itemCreationDto) {
        LOG.info("Start Add Item ith creation dto: %s", itemCreationDto);

        try {
            String code = itemManagmentService.addItemToMarket(itemCreationDto);
            LOG.info("Item successfully created with code: %s", code);
            return code;
        } catch (ValidationException e) {
            LOG.error("There are Validator exceptions duting add item process", e);
            return e.getMessage();
        }
    }

}
