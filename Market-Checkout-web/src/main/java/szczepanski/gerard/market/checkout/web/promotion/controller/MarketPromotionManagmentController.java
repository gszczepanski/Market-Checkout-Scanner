package szczepanski.gerard.market.checkout.web.promotion.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.service.common.ValidationException;
import szczepanski.gerard.market.checkout.service.promotion.BoughtTogetherPromotionCreationDto;
import szczepanski.gerard.market.checkout.service.promotion.ItemQuantityPromotionCreationDto;
import szczepanski.gerard.market.checkout.service.promotion.PromotionManagmentFacade;
import szczepanski.gerard.market.checkout.service.promotion.TotalPriceDiscountCreationDto;

@RestController
@RequestMapping("/promotions")
@RequiredArgsConstructor
class MarketPromotionManagmentController {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(MarketPromotionManagmentController.class.getName());

    private final PromotionManagmentFacade promotionManagmentFacade;

    @ResponseBody
    @PostMapping(value = "/itemQuantityPromotion/add", produces = "application/json")
    public Object addItemQuantityPromotion(@RequestBody ItemQuantityPromotionCreationDto itemQuantityPromotionCreationDto) {
        LOG.info("Start Add Item quantity promotion with creation dto: %s", itemQuantityPromotionCreationDto);

        try {
            String code = promotionManagmentFacade.addItemQuantityPromotion(itemQuantityPromotionCreationDto);
            LOG.info("Item quantity promotion successfully created with code: %s", code);
            return code;
        } catch (ValidationException e) {
            LOG.error("There are Validator exceptions duting add item quantity promotion process", e);
            return e.getMessage();
        }
    }

    @ResponseBody
    @PostMapping(value = "/boughtTogetherPromotion/add", produces = "application/json")
    public Object addBoughtTogetherPromotion(@RequestBody BoughtTogetherPromotionCreationDto boughtTogetherPromotionCreationDto) {
        LOG.info("Start Add Bought together item promotion with creation dto: %s", boughtTogetherPromotionCreationDto);

        try {
            String code = promotionManagmentFacade.addBoughtTogetherPromotion(boughtTogetherPromotionCreationDto);
            LOG.info("Bought together item promotion successfully created with code: %s", code);
            return code;
        } catch (ValidationException e) {
            LOG.error("There are Validator exceptions duting add bought together item promotion process", e);
            return e.getMessage();
        }
    }

    @ResponseBody
    @PostMapping(value = "/totalPriceDiscount/add", produces = "application/json")
    public Object addTotalPriceDiscount(@RequestBody TotalPriceDiscountCreationDto discountCreationDto) {
        LOG.info("Start Add Total Price Discount with creation dto: %s", discountCreationDto);

        try {
            String code = promotionManagmentFacade.addCheckoutTotalPriceDiscount(discountCreationDto);
            LOG.info("Discount from Total price successfully created with code: %s", code);
            return code;
        } catch (ValidationException e) {
            LOG.error("There are Validator exceptions duting add discount from total price process", e);
            return e.getMessage();
        }
    }

}
