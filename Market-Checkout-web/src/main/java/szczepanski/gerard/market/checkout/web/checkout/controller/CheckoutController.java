package szczepanski.gerard.market.checkout.web.checkout.controller;

import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.service.checkout.CheckoutService;

@RestController
@RequestMapping("/checkout")
@RequiredArgsConstructor
class CheckoutController {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(CheckoutController.class.getName());

    private final CheckoutService checkoutService;

    @GetMapping(value = "/{checkoutNumber}/scan/{itemCode}")
    public String scanItem(@PathVariable Integer checkoutNumber, @PathVariable String itemCode) {
        LOG.info("Checkout number: %s Scan item with code: %s", checkoutNumber, itemCode);

        String currentCheckoutTotalPrice = checkoutService.scan(checkoutNumber, itemCode);
        return currentCheckoutTotalPrice;
    }

    @GetMapping(value = "/{checkoutNumber}/markaspaid")
    public String registerPaymentForCheckout(@PathVariable Integer checkoutNumber) {
        LOG.info("Register payment for checkout with number: %s", checkoutNumber);

        String recipeForPayment = checkoutService.markAsPaid(checkoutNumber);
        LOG.info("Generated Recipe for checkout with number: %s\n %s", checkoutNumber, recipeForPayment);
        return recipeForPayment;
    }

    @ResponseBody
    @PostMapping(value = "/open/{checkoutNumber}")
    public String addTotalPriceDiscount(@PathVariable Integer checkoutNumber) {
        LOG.info("Open checkout number: %s", checkoutNumber);
        checkoutService.openCheckout(checkoutNumber);

        return String.format("Checkout with number %s opened", checkoutNumber);
    }

}
