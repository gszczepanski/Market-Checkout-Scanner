package szczepanski.gerard.market.checkout.web.exceptionhandler;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import szczepanski.gerard.market.checkout.component.common.exception.MarketCheckoutRuntimeException;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
class MarketExceptionHandler {

    private static final MarketLogger LOG = MarketLoggerFactory.createForName(MarketExceptionHandler.class.getName());

    @ResponseStatus(CONFLICT)
    @ExceptionHandler(MarketCheckoutRuntimeException.class)
    public String handleConflict(MarketCheckoutRuntimeException e) {
        LOG.info("Handle exception %s", e);

        return e.getMessage();
    }

}
