package szczepanski.gerard.market.checkout.web.start;

import org.mockito.internal.util.collections.Sets;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.service.common.PriceDto;
import szczepanski.gerard.market.checkout.service.common.ValidationException;
import szczepanski.gerard.market.checkout.service.item.ItemCreationDto;
import szczepanski.gerard.market.checkout.service.item.ItemManagmentService;
import szczepanski.gerard.market.checkout.service.promotion.BoughtTogetherPromotionCreationDto;
import szczepanski.gerard.market.checkout.service.promotion.ItemQuantityPromotionCreationDto;
import szczepanski.gerard.market.checkout.service.promotion.PromotionManagmentFacade;
import szczepanski.gerard.market.checkout.service.promotion.TotalPriceDiscountCreationDto;

@EnableAutoConfiguration
@ComponentScan(basePackages = "szczepanski.gerard.market.checkout")
@EnableJpaRepositories(basePackages = "szczepanski.gerard.market.checkout.domain")
@EntityScan(basePackages = "szczepanski.gerard.market.checkout.domain")
public class MarketProgram {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(MarketProgram.class.getName());

    public static void main(String... args) {
        SpringApplication.run(MarketProgram.class, args);
    }

    /**
     * Initialize some data
     */
    @Bean
    public CommandLineRunner loadData(ItemManagmentService itemMarketService, PromotionManagmentFacade promotionManagmentService) {
        return (args) -> {
            LOG.info("-- INITIALIZE START DATA ---");

            ItemCreationDto milk = ItemCreationDto.builder()
                    .name("Milk")
                    .price(new PriceDto("2.50", "USD"))
                    .build();

            ItemCreationDto orangeJuice = ItemCreationDto.builder()
                    .name("Orange Juice 1L")
                    .price(new PriceDto("5.99", "USD"))
                    .build();

            ItemCreationDto coffee = ItemCreationDto.builder()
                    .name("Tchibo coffee 500g")
                    .price(new PriceDto("16.99", "USD"))
                    .build();

            ItemCreationDto tea = ItemCreationDto.builder()
                    .name("Lipton tea 100 bags")
                    .price(new PriceDto("10.35", "USD"))
                    .build();

            ItemCreationDto whiskey = ItemCreationDto.builder()
                    .name("Johnny Walker Blue label 1L")
                    .price(new PriceDto("200.00", "USD"))
                    .build();

            ItemCreationDto honey = ItemCreationDto.builder()
                    .name("Bee Honey")
                    .price(new PriceDto("2.00", "USD"))
                    .build();

            ItemQuantityPromotionCreationDto milkQuantityPromotion =
                    ItemQuantityPromotionCreationDto.builder()
                            .name("Buy 5 Milks and pay as 3 Promotion")
                            .itemCode("item-0001")
                            .requiredItemsQuantity(5)
                            .price(new PriceDto("7.50", "USD"))
                            .build();

            ItemQuantityPromotionCreationDto orangeJuicesQuantityPromotion =
                    ItemQuantityPromotionCreationDto.builder()
                            .name("Buy 4 Orange juices and pay as 15.00 Promotion")
                            .itemCode("item-0002")
                            .requiredItemsQuantity(4)
                            .price(new PriceDto("15.00", "USD"))
                            .build();

            ItemQuantityPromotionCreationDto coffeQuantityPromotion =
                    ItemQuantityPromotionCreationDto.builder()
                            .name("Buy 3 Tchibo coffes and pay 40.00 promotion")
                            .itemCode("item-0003")
                            .requiredItemsQuantity(3)
                            .price(new PriceDto("40.00", "USD"))
                            .build();

            BoughtTogetherPromotionCreationDto teaAndCoffeeBoughtTogetherPromotion =
                    BoughtTogetherPromotionCreationDto.builder()
                            .name("Lipton tea and coffee 20% cheaper")
                            .discount("20%")
                            .requiredItemCodes(Sets.newSet("item-0003", "item-0004"))
                            .build();

            TotalPriceDiscountCreationDto checkoutTotalPriceDiscount = TotalPriceDiscountCreationDto.builder()
                    .discount("10%")
                    .fromPriceValue(new PriceDto("600.00", "USD"))
                    .build();

            try {
                itemMarketService.addItemToMarket(milk);
                itemMarketService.addItemToMarket(orangeJuice);
                itemMarketService.addItemToMarket(coffee);
                itemMarketService.addItemToMarket(tea);
                itemMarketService.addItemToMarket(whiskey);

                promotionManagmentService.addItemQuantityPromotion(milkQuantityPromotion);
                promotionManagmentService.addItemQuantityPromotion(orangeJuicesQuantityPromotion);
                promotionManagmentService.addItemQuantityPromotion(coffeQuantityPromotion);
                promotionManagmentService.addBoughtTogetherPromotion(teaAndCoffeeBoughtTogetherPromotion);
                promotionManagmentService.addCheckoutTotalPriceDiscount(checkoutTotalPriceDiscount);
            } catch (ValidationException e) {
                LOG.info("-- UNABLE TO SET DATA ---");
                LOG.info(e.getMessage());
            }
        };
    }

}
