package szczepanski.gerard.market.checkout.automated.tests.item;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;

import java.io.IOException;

@Test(suiteName = "automated-tests", singleThreaded = true)
public class ItemApiAutomatedTest {

    private static final MarketLogger LOG = MarketLoggerFactory.createForName(ItemApiAutomatedTest.class.getName());

    @Test
    public void addValidItemsToMarketReturnsNotTheSameItemCodes() throws ClientProtocolException, IOException {
        // Arrange
        String itemCodePrefix = "item-";

        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        HttpPost addCocaColaRequest = ItemRequestProvider.addItemRequest("Coca cola 2L", "3.49", "USD");
        HttpPost addRaisinsPackRequest = ItemRequestProvider.addItemRequest("Raisins 200g", "1.59", "USD");

        // Act
        LOG.info("Send add Coca Cola item post request");
        CloseableHttpResponse addCocaColaResponse = httpClient.execute(addCocaColaRequest);

        LOG.info("Send add Raisins pack item post request");
        CloseableHttpResponse addRaisinsResponse = httpClient.execute(addRaisinsPackRequest);

        String cocaColaItemCode = EntityUtils.toString(addCocaColaResponse.getEntity(), "UTF-8");
        String raisinsPackItemCode = EntityUtils.toString(addRaisinsResponse.getEntity(), "UTF-8");

        // Assert
        Assert.assertTrue(cocaColaItemCode.contains(itemCodePrefix));
        Assert.assertTrue(raisinsPackItemCode.contains(itemCodePrefix));

        Assert.assertNotEquals(cocaColaItemCode, raisinsPackItemCode);

        LOG.info("Coca Cola code after add to market: %s", cocaColaItemCode);
        LOG.info("Raisins pack code after add to market: %s", raisinsPackItemCode);
    }

    @Test
    public void addInvalidItemToMarketReturnsValidatorNotifications() throws ClientProtocolException, IOException {
        // Arrange
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        String invalidBlankName = "";
        String invalidPriceValue = "not A Price Value";
        String invalidPriceCurrency = "not a currency";

        LOG.info("Send invalid add item data with request. Name: [%s], Price value: [%s], Price currency [%s]", invalidBlankName, invalidPriceValue, invalidPriceCurrency);
        HttpPost addInvalidItemRequest = ItemRequestProvider.addItemRequest(invalidBlankName, invalidPriceValue, invalidPriceCurrency);

        // Act
        LOG.info("Send invalid item post request");
        CloseableHttpResponse addInvalidItemResponse = httpClient.execute(addInvalidItemRequest);
        String validatorNotifications = EntityUtils.toString(addInvalidItemResponse.getEntity(), "UTF-8");

        // Assert
        Assert.assertNotNull(validatorNotifications);

        LOG.info("Validator notifications: \n%s", validatorNotifications);
    }

    @Test
    public void findOneItemByCodeReturnsItem() throws ClientProtocolException, IOException {
        // Arrange
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        String expectedMilkName = "Milk";

        String milkCode = "item-0001";
        HttpGet findMilkRequrst = ItemRequestProvider.findItemRequest(milkCode);

        // Act
        LOG.info("Send find Milk request with Item code %s", milkCode);
        CloseableHttpResponse getMilkItemResponse = httpClient.execute(findMilkRequrst);
        String milkJson = EntityUtils.toString(getMilkItemResponse.getEntity(), "UTF-8");

        // Assert
        LOG.info("Returned Milk Item JSON: %s", milkJson);

        Assert.assertTrue(milkJson.contains(milkCode));
        Assert.assertTrue(milkJson.contains(expectedMilkName));
    }

    @Test
    public void findOneNotFoundItemByCodeReturnsInfoAboutItemNotFound() throws ClientProtocolException, IOException {
        // Arrange
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        String invalidCode = "item-9062";
        HttpGet findNotExistingItemRequrst = ItemRequestProvider.findItemRequest(invalidCode);

        // Act
        LOG.info("Send find not existing item request with Item code %s", invalidCode);
        CloseableHttpResponse getNotFoundItemResponse = httpClient.execute(findNotExistingItemRequrst);
        String responseText = EntityUtils.toString(getNotFoundItemResponse.getEntity(), "UTF-8");

        // Assert
        LOG.info("Returned Not foudn item text: %s", responseText);

        Assert.assertTrue(responseText.contains(invalidCode));
        Assert.assertTrue(responseText.contains("not found"));
    }

}
