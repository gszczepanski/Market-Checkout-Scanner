package szczepanski.gerard.market.checkout.automated.tests.promotion;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import szczepanski.gerard.market.checkout.component.common.utils.MarketCheckoutStringUtils;

import java.util.Set;
import java.util.stream.Collectors;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class PromotionRequestProvider {

    private static final String HOST = "http://localhost:4000";

    public static HttpPost addItemQuantityPromotionRequest(String name, String itemCode, Integer requiredQuantity, String priceValue, String priceCurrency) {
        String uri = HOST + "/promotions/itemQuantityPromotion/add";
        String json = String.format("{\"name\": \"%s\", \"price\": {\"value\": \"%s\", \"currency\": \"%s\"}, \"itemCode\":\"%s\", \"requiredItemsQuantity\": %s}", name, priceValue, priceCurrency, itemCode, requiredQuantity);

        return jsonPost(uri, json);
    }

    public static HttpPost addBoughtTogetherPromotionRequest(String name, String discount, Set<String> itemCodes) {
        String uri = HOST + "/promotions/boughtTogetherPromotion/add";

        Set<String> itemCodesWithQuotations = itemCodes.stream().map(s -> String.format("\"%s\"", s)).collect(Collectors.toSet());
        String itemCodesJson = StringUtils.join(itemCodesWithQuotations, MarketCheckoutStringUtils.COMMA_SEPARATOR);

        String json = String.format("{\"name\": \"%s\", \"discount\":\"%s\", \"requiredItemCodes\": [%s]}", name, discount, itemCodesJson);

        return jsonPost(uri, json);
    }

    public static HttpPost addTotalPriceDiscountRequest(String discount, String fromPriceValue, String fromPriceCurrency) {
        String uri = HOST + "/promotions/totalPriceDiscount/add";
        String json = String.format("{\"discount\": \"%s\", \"fromPriceValue\": {\"value\": \"%s\", \"currency\": \"%s\"}}", discount, fromPriceValue, fromPriceCurrency);

        return jsonPost(uri, json);
    }

    private static HttpPost jsonPost(String uri, String json) {
        StringEntity requestEntity = new StringEntity(
                json,
                ContentType.APPLICATION_JSON);

        HttpPost request = new HttpPost(uri);
        request.setEntity(requestEntity);

        return request;
    }

}
