package szczepanski.gerard.market.checkout.automated.tests.checkout;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.testng.Assert;
import szczepanski.gerard.market.checkout.automated.tests.checkout.CustomerForSimulation.CustomerItem;

import java.io.IOException;
import java.util.List;

public class CashierForSimulation {

    private final Integer checkoutNumber;
    private final List<CustomerForSimulation> customersQueue;

    private final CloseableHttpClient httpClient;

    public CashierForSimulation(Integer checkoutNumber, List<CustomerForSimulation> customersQueue) {
        super();
        this.checkoutNumber = checkoutNumber;
        this.customersQueue = customersQueue;
        this.httpClient = HttpClientBuilder.create().build();
    }

    public void openFirstCheckout(Integer checkoutNumber) {
        log("*** Cashier %s opened has checkout", this.checkoutNumber);

        HttpPost openCheckoutRequest = CheckoutRequestProvider.openCheckoutRequest(checkoutNumber);
        try {
            this.httpClient.execute(openCheckoutRequest);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void startHandlingCustomers() {
        customersQueue.forEach(this::handleCustomer);
    }

    private void handleCustomer(CustomerForSimulation customer) {
        try {
            doHandleCustomer(customer);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private void doHandleCustomer(CustomerForSimulation customer) throws InterruptedException {
        log("*** Start Handling customer %s", customer.getName());
        Thread.sleep(5000);

        scanItems(customer);
        printRecipe(customer);
    }

    private void scanItems(CustomerForSimulation customer) {
        while (customer.hasCustomerItems()) {
            CustomerItem itemToScan = customer.getItemToScan();
            String itemCode = itemToScan.getItemCode();
            HttpGet scanItemRequest = CheckoutRequestProvider.scanItemRequest(this.checkoutNumber, itemToScan.getItemCode());

            try {
                CloseableHttpResponse response = this.httpClient.execute(scanItemRequest);
                String currentBasketPrice = EntityUtils.toString(response.getEntity(), "UTF-8");
                log("Scanned item with code %s. Current Basket price: %s", itemCode, currentBasketPrice);

                Assert.assertEquals(currentBasketPrice, itemToScan.getExpectedPriceAfterItemScan(), "Failed for customer: " + customer.getName());
                Thread.sleep(3000);
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void printRecipe(CustomerForSimulation customer) {
        log("*** Register customer %s payment", customer.getName());
        HttpGet markCheckoutAsPaidRequest = CheckoutRequestProvider.markCheckoutAsPaidRequest(this.checkoutNumber);

        try {
            CloseableHttpResponse response = this.httpClient.execute(markCheckoutAsPaidRequest);
            String recipe = EntityUtils.toString(response.getEntity(), "UTF-8");

            Assert.assertNotNull(recipe);

            log("Recipe: \n%s", recipe);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void log(String message, Object... args) {
        System.out.println(String.format(message, args));
    }

}
