package szczepanski.gerard.market.checkout.automated.tests.checkout;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class CheckoutRequestProvider {

    private static final String HOST = "http://localhost:4000";

    public static HttpPost openCheckoutRequest(Integer checkoutNumber) {
        String URI = HOST + "/checkout/open/" + checkoutNumber;
        return new HttpPost(URI);
    }

    public static HttpGet scanItemRequest(Integer checkoutNumber, String itemCode) {
        String URI = String.format(HOST + "/checkout/%s/scan/%s", checkoutNumber, itemCode);
        return new HttpGet(URI);
    }

    public static HttpGet markCheckoutAsPaidRequest(Integer checkoutNumber) {
        String URI = String.format(HOST + "/checkout/%s/markaspaid", checkoutNumber);
        return new HttpGet(URI);
    }
}
