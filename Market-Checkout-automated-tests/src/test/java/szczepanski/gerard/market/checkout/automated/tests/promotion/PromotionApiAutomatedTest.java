package szczepanski.gerard.market.checkout.automated.tests.promotion;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.mockito.internal.util.collections.Sets;
import org.testng.Assert;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;

import java.io.IOException;

@Test(suiteName = "automated-tests", singleThreaded = true)
public class PromotionApiAutomatedTest {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(PromotionApiAutomatedTest.class.getName());

    @Test
    public void addValidItemQuantityPromotionToMarketReturnsNotTheSameItemCodes() throws ClientProtocolException, IOException {
        // Arrange
        String itemQuantityPromotionCodePrefix = "iqp-";
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        HttpPost addMilkItemQuantityPromotionsRequest = PromotionRequestProvider.addItemQuantityPromotionRequest("buy 400 like 300 milks", "item-0001", 400, "500.00", "USD");
        HttpPost addOrangeJuiceItemQuantityPromotionsRequest = PromotionRequestProvider.addItemQuantityPromotionRequest("buy 100 like 50 juices", "item-0002", 100, "700.00", "USD");

        // Act
        LOG.info("Send add item quantity promotion 400 like 300 milks");
        CloseableHttpResponse addMilkItemQuantityPromotionsResponse = httpClient.execute(addMilkItemQuantityPromotionsRequest);

        LOG.info("Send add item quantity promotion 100 like 50 juices");
        CloseableHttpResponse addOrangeJuiceItemQuantityPromotionsResponse = httpClient.execute(addOrangeJuiceItemQuantityPromotionsRequest);

        String milkItemQuantityPromotionCode = EntityUtils.toString(addMilkItemQuantityPromotionsResponse.getEntity(), "UTF-8");
        String orangeJuiceQuantityPromotionCode = EntityUtils.toString(addOrangeJuiceItemQuantityPromotionsResponse.getEntity(), "UTF-8");

        // Assert
        Assert.assertTrue(milkItemQuantityPromotionCode.contains(itemQuantityPromotionCodePrefix));
        Assert.assertTrue(orangeJuiceQuantityPromotionCode.contains(itemQuantityPromotionCodePrefix));

        Assert.assertNotEquals(milkItemQuantityPromotionCode, orangeJuiceQuantityPromotionCode);

        LOG.info("Item quantity promotion 400 like 300 milks code after add to market: %s", milkItemQuantityPromotionCode);
        LOG.info("Item quantity promotion 100 like 50 juices code after add to market: %s", orangeJuiceQuantityPromotionCode);
    }

    @Test
    public void addInvalidItemQuantityPromotionToMarketReturnsValidatorNotifications() throws ClientProtocolException, IOException {
        // Arrange
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        String invalidName = "";

        LOG.info("Send invalid add Item quantity promotion with request. Name: [%s]", invalidName);
        HttpPost addItemQuantityPromotionWithInvalidDataRequest = PromotionRequestProvider.addItemQuantityPromotionRequest(invalidName, "item-0001", 1, "10000.00", "USD");

        // Act
        LOG.info("Send add invalid discount post request");
        CloseableHttpResponse addInvalidItemQuantityPromotionResponse = httpClient.execute(addItemQuantityPromotionWithInvalidDataRequest);
        String validatorNotifications = EntityUtils.toString(addInvalidItemQuantityPromotionResponse.getEntity(), "UTF-8");

        // Assert
        Assert.assertNotNull(validatorNotifications);

        LOG.info("Validator notifications: \n%s", validatorNotifications);
    }

    @Test
    public void addValidBoughtTogetherPromotionToMarketReturnsNotTheSameItemCodes() throws ClientProtocolException, IOException {
        // Arrange
        String boughtTogetherPromotionCodePrefix = "btp-";
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        HttpPost addMilkAndHoneyPromotionRequest = PromotionRequestProvider.addBoughtTogetherPromotionRequest("Milk and honey together 10% cheaper", "10%", Sets.newSet("item-0001", "item-0006"));

        // Act
        LOG.info("Send add bought together promotion Milk and honey together 10%% cheaper");
        CloseableHttpResponse addMilkAndHoneyPromotionResponse = httpClient.execute(addMilkAndHoneyPromotionRequest);

        String milkAndHoneyBoughtTogetherPromotionCode = EntityUtils.toString(addMilkAndHoneyPromotionResponse.getEntity(), "UTF-8");

        // Assert
        Assert.assertTrue(milkAndHoneyBoughtTogetherPromotionCode.contains(boughtTogetherPromotionCodePrefix));

        LOG.info("Bought together promotion Milk and Honey together 10%% cheaper code after add to market: %s", milkAndHoneyBoughtTogetherPromotionCode);
    }

    @Test
    public void addInvalidBoughtTogetherPromotionToMarketReturnsValidatorNotifications() throws ClientProtocolException, IOException {
        // Arrange
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        String invalidName = "";

        LOG.info("Send invalid add bought together promotion with request. Name: [%s]", invalidName);
        HttpPost addMilkAndJuicePromotionRequestWithInvalidName = PromotionRequestProvider.addBoughtTogetherPromotionRequest(invalidName, "10%", Sets.newSet("item-0001", "item-0002"));

        // Act
        LOG.info("Send add invalid bought together promotion post request");
        CloseableHttpResponse addInvalidPromotionResponse = httpClient.execute(addMilkAndJuicePromotionRequestWithInvalidName);
        String validatorNotifications = EntityUtils.toString(addInvalidPromotionResponse.getEntity(), "UTF-8");

        // Assert
        Assert.assertNotNull(validatorNotifications);

        LOG.info("Validator notifications: \n%s", validatorNotifications);
    }

    @Test
    public void addValidDiscountsFromTotalPriceToMarketReturnsNotTheSameItemCodes() throws ClientProtocolException, IOException {
        // Arrange
        String discountFromTotalPriceCodePrefix = "dftp-";
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        HttpPost addSixPercentTotalPriceDiscountRequest = PromotionRequestProvider.addTotalPriceDiscountRequest("6%", "10000.00", "USD");
        HttpPost addNinePercentTotalPriceDiscountRequest = PromotionRequestProvider.addTotalPriceDiscountRequest("9%", "20000.00", "USD");

        // Act
        LOG.info("Send add 6%% discount from 10 000.00 USD");
        CloseableHttpResponse addSixPercentTotalPriceDiscountResponse = httpClient.execute(addSixPercentTotalPriceDiscountRequest);

        LOG.info("Send add 9%% discount from 20 000.00 USD");
        CloseableHttpResponse addNinePercentTotalPriceDiscountResponse = httpClient.execute(addNinePercentTotalPriceDiscountRequest);

        String sixPercentDiscountCode = EntityUtils.toString(addSixPercentTotalPriceDiscountResponse.getEntity(), "UTF-8");
        String ninePercentDiscountCode = EntityUtils.toString(addNinePercentTotalPriceDiscountResponse.getEntity(), "UTF-8");

        // Assert
        Assert.assertTrue(sixPercentDiscountCode.contains(discountFromTotalPriceCodePrefix));
        Assert.assertTrue(ninePercentDiscountCode.contains(discountFromTotalPriceCodePrefix));

        Assert.assertNotEquals(sixPercentDiscountCode, ninePercentDiscountCode);

        LOG.info("6%% discount from total price code after add to market: %s", sixPercentDiscountCode);
        LOG.info("9%% discount from total price code after add to market: %s", ninePercentDiscountCode);
    }

    @Test
    public void addInvalidDiscountFromTotalPriceToMarketReturnsValidatorNotifications() throws ClientProtocolException, IOException {
        // Arrange
        CloseableHttpClient httpClient = HttpClientBuilder.create().build();

        String invalidPercentageValue = "14.00x";

        LOG.info("Send invalid add Discount from total price ith request. Percentage value: [%s]", invalidPercentageValue);
        HttpPost addDiscountRequestWithInvalidDataRequest = PromotionRequestProvider.addTotalPriceDiscountRequest(invalidPercentageValue, "10000.00", "USD");

        // Act
        LOG.info("Send add invalid discount post request");
        CloseableHttpResponse addInvalidItemResponse = httpClient.execute(addDiscountRequestWithInvalidDataRequest);
        String validatorNotifications = EntityUtils.toString(addInvalidItemResponse.getEntity(), "UTF-8");

        // Assert
        Assert.assertNotNull(validatorNotifications);

        LOG.info("Validator notifications: \n%s", validatorNotifications);
    }

}
