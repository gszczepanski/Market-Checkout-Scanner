package szczepanski.gerard.market.checkout.automated.tests.checkout;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

import java.util.LinkedList;
import java.util.Queue;

public class CustomerForSimulation {

    @Getter
    private final String name;
    private final Queue<CustomerItem> itemsQueue;

    public CustomerForSimulation(String name) {
        this.name = name;
        this.itemsQueue = new LinkedList<>();
    }

    public void addItemToBasket(String itemCode, String expectedPriceAfterScan) {
        this.itemsQueue.add(new CustomerItem(itemCode, expectedPriceAfterScan));
    }

    public CustomerItem getItemToScan() {
        return this.itemsQueue.poll();
    }

    public Boolean hasCustomerItems() {
        return !this.itemsQueue.isEmpty();
    }

    @Getter
    @RequiredArgsConstructor
    public static class CustomerItem {

        private final String itemCode;
        private final String expectedPriceAfterItemScan;
    }
}
