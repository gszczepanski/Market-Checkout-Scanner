package szczepanski.gerard.market.checkout.automated.tests.checkout;

import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;

import java.util.Arrays;
import java.util.List;

/**
 * Main Simulation test.
 * <p>
 * 10 baskets (customers) in 3 active checkouts.
 *
 * @author Gerard Szczepa�ski
 */
@Test(suiteName = "automated-tests")
public class MarketCheckoutSimulationAutomatedTest {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(MarketCheckoutSimulationAutomatedTest.class.getName());

    @Test
    public void checkoutSimulation() throws InterruptedException {
        // Arrange
        LOG.info("!!!!!!!!!!!!!!!!! START MARKET CHECKOUT SIMULATION -> 10 customers for 3 cashiers");

        CustomerForSimulation firstCustomer = new CustomerForSimulation("Mark Knopfler");
        firstCustomer.addItemToBasket("item-0001", "2.50 USD");
        firstCustomer.addItemToBasket("item-0001", "5.00 USD");
        firstCustomer.addItemToBasket("item-0001", "7.50 USD");
        firstCustomer.addItemToBasket("item-0002", "13.49 USD");

        CustomerForSimulation secondCustomer = new CustomerForSimulation("Mike Oldfield");
        secondCustomer.addItemToBasket("item-0005", "200.00 USD");
        secondCustomer.addItemToBasket("item-0005", "400.00 USD");
        secondCustomer.addItemToBasket("item-0005", "540.00 USD"); // 10% discount from total
        secondCustomer.addItemToBasket("item-0005", "720.00 USD"); // 10% discount from total

        CustomerForSimulation thirdCustomer = new CustomerForSimulation("Bruce Dickinson");
        thirdCustomer.addItemToBasket("item-0004", "10.35 USD");
        thirdCustomer.addItemToBasket("item-0001", "12.85 USD");
        thirdCustomer.addItemToBasket("item-0001", "15.35 USD");
        thirdCustomer.addItemToBasket("item-0004", "25.70 USD");
        thirdCustomer.addItemToBasket("item-0003", "35.15 USD"); // 20% discount for coffee and tea

        CustomerForSimulation fourthCustomer = new CustomerForSimulation("Mister Mister");
        fourthCustomer.addItemToBasket("item-0004", "10.35 USD");
        fourthCustomer.addItemToBasket("item-0001", "12.85 USD");
        fourthCustomer.addItemToBasket("item-0001", "15.35 USD");
        fourthCustomer.addItemToBasket("item-0004", "25.70 USD");
        fourthCustomer.addItemToBasket("item-0003", "35.15 USD"); // 20% discount for coffee and tea
        fourthCustomer.addItemToBasket("item-0005", "235.15 USD");
        fourthCustomer.addItemToBasket("item-0005", "435.15 USD");
        fourthCustomer.addItemToBasket("item-0005", "571.63 USD"); // 10% discount from total

        CustomerForSimulation fifthCustomer = new CustomerForSimulation("Mike Stern");
        fifthCustomer.addItemToBasket("item-0001", "2.50 USD");
        fifthCustomer.addItemToBasket("item-0001", "5.00 USD");
        fifthCustomer.addItemToBasket("item-0001", "7.50 USD");
        fifthCustomer.addItemToBasket("item-0001", "10.00 USD");
        fifthCustomer.addItemToBasket("item-0001", "7.50 USD"); // Applied ItemQuantityPromotion for Milk (5 for 3)
        fifthCustomer.addItemToBasket("item-0001", "10.00 USD");

        CustomerForSimulation sixthCustomer = new CustomerForSimulation("Jerry Lewis");
        sixthCustomer.addItemToBasket("item-0001", "2.50 USD");
        sixthCustomer.addItemToBasket("item-0001", "5.00 USD");
        sixthCustomer.addItemToBasket("item-0001", "7.50 USD");
        sixthCustomer.addItemToBasket("item-0001", "10.00 USD");
        sixthCustomer.addItemToBasket("item-0001", "7.50 USD"); // Applied ItemQuantityPromotion for Milk (5 for 3)
        sixthCustomer.addItemToBasket("item-0001", "10.00 USD");
        sixthCustomer.addItemToBasket("item-0001", "12.50 USD");
        sixthCustomer.addItemToBasket("item-0001", "15.00 USD");
        sixthCustomer.addItemToBasket("item-0001", "17.50 USD");
        sixthCustomer.addItemToBasket("item-0001", "15.00 USD"); // Again Applied ItemQuantityPromotion for Milk (5 for 3)

        CustomerForSimulation seventhCustomer = new CustomerForSimulation("Billy Joel");
        seventhCustomer.addItemToBasket("item-0001", "2.50 USD");
        seventhCustomer.addItemToBasket("item-0001", "5.00 USD");
        seventhCustomer.addItemToBasket("item-0001", "7.50 USD");
        seventhCustomer.addItemToBasket("item-0001", "10.00 USD");
        seventhCustomer.addItemToBasket("item-0001", "7.50 USD"); // Applied ItemQuantityPromotion for Milk (5 for 3)
        seventhCustomer.addItemToBasket("item-0002", "13.49 USD");
        seventhCustomer.addItemToBasket("item-0002", "19.48 USD");
        seventhCustomer.addItemToBasket("item-0002", "25.47 USD");
        seventhCustomer.addItemToBasket("item-0002", "22.50 USD"); // Apply buy 4 orange juices for 15.00 promo
        seventhCustomer.addItemToBasket("item-0001", "25.00 USD");
        seventhCustomer.addItemToBasket("item-0001", "27.50 USD");
        seventhCustomer.addItemToBasket("item-0001", "30.00 USD");
        seventhCustomer.addItemToBasket("item-0001", "32.50 USD");
        seventhCustomer.addItemToBasket("item-0001", "30.00 USD"); // Again Applied ItemQuantityPromotion for Milk (5 for 3)

        CustomerForSimulation eighthCustomer = new CustomerForSimulation("Jordan Rudess");
        eighthCustomer.addItemToBasket("item-0003", "16.99 USD");
        eighthCustomer.addItemToBasket("item-0003", "33.98 USD");
        eighthCustomer.addItemToBasket("item-0003", "40.00 USD"); // Third coffee. 3 for 2 promotion applied
        eighthCustomer.addItemToBasket("item-0003", "56.99 USD");
        eighthCustomer.addItemToBasket("item-0004", "53.87 USD"); // Apply bought together coffee and tea 20% discount

        CustomerForSimulation ninthCustomer = new CustomerForSimulation("Lord Vader");
        ninthCustomer.addItemToBasket("item-0004", "10.35 USD");
        ninthCustomer.addItemToBasket("item-0001", "12.85 USD");
        ninthCustomer.addItemToBasket("item-0001", "15.35 USD");
        ninthCustomer.addItemToBasket("item-0004", "25.70 USD");
        ninthCustomer.addItemToBasket("item-0003", "35.15 USD"); // 20% discount for coffee and tea
        ninthCustomer.addItemToBasket("item-0005", "235.15 USD");
        ninthCustomer.addItemToBasket("item-0005", "435.15 USD");
        ninthCustomer.addItemToBasket("item-0005", "571.63 USD"); // 10% discount from total

        CustomerForSimulation tenthCustomer = new CustomerForSimulation("Steve Harris");
        tenthCustomer.addItemToBasket("item-0004", "10.35 USD");
        tenthCustomer.addItemToBasket("item-0001", "12.85 USD");
        tenthCustomer.addItemToBasket("item-0001", "15.35 USD");
        tenthCustomer.addItemToBasket("item-0004", "25.70 USD");
        tenthCustomer.addItemToBasket("item-0003", "35.15 USD"); // 20% discount for coffee and tea
        tenthCustomer.addItemToBasket("item-0005", "235.15 USD");
        tenthCustomer.addItemToBasket("item-0005", "435.15 USD");
        tenthCustomer.addItemToBasket("item-0005", "571.63 USD"); // 10% discount from total

        Integer firstCheckoutNumber = 1;
        List<CustomerForSimulation> firstCustomersQueue = Arrays.asList(firstCustomer, secondCustomer, ninthCustomer, tenthCustomer);
        CashierForSimulation firstCashier = new CashierForSimulation(firstCheckoutNumber, firstCustomersQueue);
        firstCashier.openFirstCheckout(firstCheckoutNumber);

        Integer secondCheckoutNumber = 2;
        List<CustomerForSimulation> secondCustomersQueue = Arrays.asList(thirdCustomer, fourthCustomer, eighthCustomer);
        CashierForSimulation secondCashier = new CashierForSimulation(secondCheckoutNumber, secondCustomersQueue);
        secondCashier.openFirstCheckout(secondCheckoutNumber);

        Integer thirdCheckoutNumber = 3;
        List<CustomerForSimulation> thirdCustomersQueue = Arrays.asList(fifthCustomer, sixthCustomer, seventhCustomer);
        CashierForSimulation thirdCashier = new CashierForSimulation(thirdCheckoutNumber, thirdCustomersQueue);
        thirdCashier.openFirstCheckout(thirdCheckoutNumber);

        Thread firstCashierThread = new Thread(() -> {
            firstCashier.startHandlingCustomers();
        });

        Thread secondCashierThread = new Thread(() -> {
            secondCashier.startHandlingCustomers();
        });

        Thread thirdCashierThread = new Thread(() -> {
            thirdCashier.startHandlingCustomers();
        });

        // Act
        firstCashierThread.start();
        Thread.sleep(2550);
        secondCashierThread.start();
        Thread.sleep(2700);
        thirdCashierThread.start();

        firstCashierThread.join();
        secondCashierThread.join();
        thirdCashierThread.join();
    }
}
