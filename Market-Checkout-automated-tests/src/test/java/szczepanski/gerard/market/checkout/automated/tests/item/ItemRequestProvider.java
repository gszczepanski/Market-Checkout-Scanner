package szczepanski.gerard.market.checkout.automated.tests.item;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ItemRequestProvider {

    private static final String HOST = "http://localhost:4000";

    public static HttpPost addItemRequest(String itemName, String priceValue, String priceCurrency) {
        String URI = HOST + "/items/item/add";
        String JSON_STRING = String.format("{\"name\": \"%s\", \"price\": {\"value\": \"%s\", \"currency\": \"%s\"}}", itemName, priceValue, priceCurrency);

        StringEntity requestEntity = new StringEntity(
                JSON_STRING,
                ContentType.APPLICATION_JSON);

        HttpPost request = new HttpPost(URI);
        request.setEntity(requestEntity);

        return request;
    }

    public static HttpGet findItemRequest(String itemCode) {
        String URI = HOST + "/items/item/" + itemCode;
        return new HttpGet(URI);
    }

}
