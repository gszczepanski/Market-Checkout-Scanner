package szczepanski.gerard.market.checkout.component.common.logger;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.log4j.Logger;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MarketLoggerFactory {

    public static MarketLogger createForName(String name) {
        return new MarketLoggerFactoryImpl(name);
    }

    private static class MarketLoggerFactoryImpl implements MarketLogger {

        private final Logger log;

        private MarketLoggerFactoryImpl(String name) {
            this.log = Logger.getLogger(name);
        }

        @Override
        public void info(String msg) {
            info(msg, new Object[]{});
        }

        @Override
        public void info(String msg, Object... args) {
            log.info(String.format(msg, args));
        }

        @Override
        public void debug(String msg) {
            debug(msg, new Object[]{});
        }

        @Override
        public void debug(String msg, Object... args) {
            log.debug(String.format(msg, args));
        }

        @Override
        public void error(String msg, Throwable error) {
            error(msg, error, new Object[]{});
        }

        @Override
        public void error(String msg, Throwable error, Object... args) {
            log.error(String.format(msg, args), error);
        }

    }

}
