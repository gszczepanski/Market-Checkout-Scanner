package szczepanski.gerard.market.checkout.component.common.exception;

/**
 * This exception represents RunimeException in MarketCheckut component.
 *
 * @author Gerard Szczepa�ski
 */
public class MarketCheckoutRuntimeException extends RuntimeException {
    private static final long serialVersionUID = 275509819620910771L;

    public MarketCheckoutRuntimeException() {
        super();
    }

    public MarketCheckoutRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public MarketCheckoutRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public MarketCheckoutRuntimeException(String message) {
        super(message);
    }

    public MarketCheckoutRuntimeException(Throwable cause) {
        super(cause);
    }

}
