package szczepanski.gerard.market.checkout.component.common.utils;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class MarketCheckoutStringUtils {

    public static final String EMPTY = "";
    public static final String PERCENT_SIGN = "%";
    public static final String SPACE = " ";
    public static final String REGEXP_SPACE = "\\s+";
    public static final String NEXT_LINE = "\n";

}
