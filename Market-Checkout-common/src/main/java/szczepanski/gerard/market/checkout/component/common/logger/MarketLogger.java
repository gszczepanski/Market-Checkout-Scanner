package szczepanski.gerard.market.checkout.component.common.logger;

public interface MarketLogger {

    void info(String msg);

    void info(String msg, Object... args);

    void debug(String msg);

    void debug(String msg, Object... args);

    void error(String msg, Throwable error);

    void error(String msg, Throwable error, Object... args);

}
