package szczepanski.gerard.market.checkout.component.common.program;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

/**
 * This constants can be loaded from file (properties for example) in the future on
 * program load.
 *
 * @author Gerard Szczepa�ski
 */
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class MarketCheckoutConstants {

    public static final String MARKET_NAME = "OLDFIELD GROCERY MARKET";

}
