package szczepanski.gerard.market.checkout.component.common.lock;

import lombok.RequiredArgsConstructor;
import szczepanski.gerard.market.checkout.component.common.exception.MarketCheckoutRuntimeException;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;

/**
 * Decorator for Lock. It adds AutoCloseable functionality
 * to use lock in try with resources statements.
 * <br><br>
 * It also catches all exceptions caused by wrapped Lock implementor.
 * <br><br>
 * This lock will unlock automatically in Try-with-resources statement on close() method from AutoCloseable interface.
 *
 * @author Gerard Szczepanski
 */
@RequiredArgsConstructor
public final class AutoReleaseLock implements AutoCloseable, Lock {

    private final Lock lock;

    /**
     * Unlock lock on finally statement
     */
    @Override
    public void close() {
        this.lock.unlock();
    }

    @Override
    public void lock() {
        this.lock.lock();
    }

    @Override
    public void lockInterruptibly() {
        try {
            this.lock.lockInterruptibly();
        } catch (InterruptedException e) {
            throw new MarketCheckoutRuntimeException(e);
        }
    }

    @Override
    public boolean tryLock() {
        return this.lock.tryLock();
    }

    @Override
    public boolean tryLock(long time, TimeUnit unit) {
        try {
            return this.lock.tryLock(time, unit);
        } catch (InterruptedException e) {
            throw new MarketCheckoutRuntimeException(e);
        }
    }

    @Override
    public void unlock() {
        this.lock.unlock();
    }

    @Override
    public Condition newCondition() {
        return this.lock.newCondition();
    }


}
