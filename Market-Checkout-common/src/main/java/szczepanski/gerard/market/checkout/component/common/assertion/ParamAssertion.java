package szczepanski.gerard.market.checkout.component.common.assertion;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class ParamAssertion {

    public static void guardIsNotNull(Object... object) {
        if (!isNotNull(object)) {
            throw new ParamAssertionException("Assertion failed! Object is null!");
        }
    }

    public static boolean isNotNull(Object... object) {
        for (Object o : object) {
            if (o == null) {
                return false;
            }
        }
        return true;
    }

    public static void guardIsNumberRepresentation(String value) {
        if (!isNumberRepresentation(value)) {
            throw new ParamAssertionException(String.format("Given String value: %s is not numeric!", value));
        }
    }

    public static boolean isNumberRepresentation(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException nfe) {
            return false;
        }
    }

    public static void guardIsNotBlank(String text) {
        if (!isNotBlank(text)) {
            throw new ParamAssertionException("String is blank!");
        }
    }

    public static boolean isNotBlank(String text) {
        return text != null && !"".equals(text);
    }

    public static void guardIsPercentageStringValue(String percentageStringValue) {
        if (!isPercentageStringValue(percentageStringValue)) {
            throw new ParamAssertionException(String.format("Given percentage value [%s] is not percentage value!", percentageStringValue));
        }
    }

    public static boolean isPercentageStringValue(String percentageStringValue) {
        ParamAssertion.guardIsNotBlank(percentageStringValue);
        String percentageStringRegexp = "\\d+(?:\\.\\d+)?%";

        return percentageStringValue.matches(percentageStringRegexp);
    }

    public static void guardMaxLength(String text, int maxLength) {
        if (!isMaxLengthNotExceeded(text, maxLength)) {
            throw new ParamAssertionException(String.format("Given value: %s lenght exceeded max lenght: %s", text, maxLength));
        }
    }

    public static boolean isMaxLengthNotExceeded(String text, int maxLength) {
        return text.length() <= maxLength;
    }

    public static void guardIsMinValueValid(Integer value, Integer minValue) {
        if (value < minValue) {
            throw new ParamAssertionException(String.format("Assertion failed! Required min value for int is %s. Got %s", minValue, value));
        }
    }

    public static void guardIsTrue(Boolean expression) {
        if (!expression) {
            throw new ParamAssertionException("Assertion failed! Required TRUE expression");
        }
    }
}
