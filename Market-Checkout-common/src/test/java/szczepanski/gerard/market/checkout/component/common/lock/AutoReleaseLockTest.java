package szczepanski.gerard.market.checkout.component.common.lock;

import lombok.RequiredArgsConstructor;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Test(suiteName = "unit-tests")
public class AutoReleaseLockTest {

    @Test
    public void synchronizeRequestsAccessedToLockedSection() throws Exception {
        // Arrange
        CodeSupplierStub codeSupplier = new CodeSupplierStub();
        ServiceStub service = new ServiceStub(codeSupplier);

        System.out.println(codeSupplier);

        int expectedCreatedCodesNumber = 10;
        Thread[] concurrentRequests = new Thread[expectedCreatedCodesNumber];

        // Act
        /**
         * Fire requests
         */
        for (int i = 0; i < expectedCreatedCodesNumber; i++) {
            concurrentRequests[i] = new Thread(() -> {
                System.out.println(Thread.currentThread().getName());
                service.saveObject();
            });

            concurrentRequests[i].setName("Request number " + i);
            concurrentRequests[i].start();
        }

        /**
         * Wait for requests finish
         */
        for (int i = 0; i < expectedCreatedCodesNumber; i++) {
            concurrentRequests[i].join();
        }

        // Assert
        List<String> codesCreatedByRequests = codeSupplier.savedCodesInOrder;
        Assert.assertEquals(codesCreatedByRequests.size(), expectedCreatedCodesNumber);

        Assert.assertEquals(codesCreatedByRequests.get(0), "1");
        Assert.assertEquals(codesCreatedByRequests.get(1), "2");
        Assert.assertEquals(codesCreatedByRequests.get(2), "3");
        Assert.assertEquals(codesCreatedByRequests.get(3), "4");
        Assert.assertEquals(codesCreatedByRequests.get(4), "5");
        Assert.assertEquals(codesCreatedByRequests.get(5), "6");
        Assert.assertEquals(codesCreatedByRequests.get(6), "7");
        Assert.assertEquals(codesCreatedByRequests.get(7), "8");
        Assert.assertEquals(codesCreatedByRequests.get(8), "9");
        Assert.assertEquals(codesCreatedByRequests.get(9), "10");
    }

    @RequiredArgsConstructor
    private static class ServiceStub {

        private final CodeSupplierStub codeSupplier;

        private final Lock saveLock = new ReentrantLock();

        public void saveObject() {
            try (AutoReleaseLock lock = new AutoReleaseLock(new LockLogDecorator(this.saveLock))) {
                lock.lock();

                int latestCode = this.codeSupplier.queryForLatestCode();
                latestCode++;
                this.codeSupplier.save(String.valueOf(latestCode));
            }
        }

    }

    private static class CodeSupplierStub {

        private int latestCode = 0;
        private List<String> savedCodesInOrder = Collections.synchronizedList(new ArrayList<>());

        public int queryForLatestCode() {
            return latestCode;
        }

        public void save(String latestCode) {
            System.out.println("Saving code: " + latestCode);
            this.savedCodesInOrder.add(latestCode);
            this.latestCode = Integer.valueOf(latestCode);
        }
    }

    @RequiredArgsConstructor
    private static class LockLogDecorator implements Lock {

        private final Lock lock;

        @Override
        public void lock() {
            System.out.println("Lock");
            this.lock.lock();
        }

        @Override
        public void lockInterruptibly() throws InterruptedException {
            this.lock.lockInterruptibly();
        }

        @Override
        public boolean tryLock() {
            return this.lock.tryLock();
        }

        @Override
        public boolean tryLock(long time, TimeUnit unit) throws InterruptedException {
            return this.lock.tryLock(time, unit);
        }

        @Override
        public void unlock() {
            System.out.println("Unlock");
            this.lock.unlock();
        }

        @Override
        public Condition newCondition() {
            return this.lock.newCondition();
        }

    }

}
