package szczepanski.gerard.market.checkout.service.promotion;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.item.ItemRepository;
import szczepanski.gerard.market.checkout.service.common.AbstractValidator;

import java.util.Set;
import java.util.stream.Collectors;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class BoughtTogetherPromotionCreationValidator extends AbstractValidator<BoughtTogetherPromotionCreationDto> {

    private static final Integer REQUIRED_CODES_MIN_LENGTH = 2;

    private final ItemRepository itemRepository;

    @Override
    protected void doValidateObject(BoughtTogetherPromotionCreationDto object, NotificationContainer notificationContainer) {
        String name = object.getName();
        validateName(name, notificationContainer);

        String discount = object.getDiscount();
        validateDiscount(discount, notificationContainer);

        Set<String> itemCodes = object.getRequiredItemCodes();
        validateRequiredItemCodes(itemCodes, notificationContainer);
    }

    private void validateName(String name, NotificationContainer notificationContainer) {
        if (!ParamAssertion.isNotBlank(name)) {
            notificationContainer.addNotification("Name must be valid text!");
            return;
        }

        if (!ParamAssertion.isMaxLengthNotExceeded(name, Name.MAX_LENGTH)) {
            notificationContainer.addNotification("Name mast be max %s characters long", Name.MAX_LENGTH);
        }
    }

    private void validateDiscount(String discount, NotificationContainer notificationContainer) {
        if (!ParamAssertion.isNotBlank(discount)) {
            notificationContainer.addNotification("Discount must be valid text!");
            return;
        }

        if (!ParamAssertion.isPercentageStringValue(discount)) {
            notificationContainer.addNotification("Discount is invalid. Valid discount formats: 18%% | 18.00%%");
        }
    }

    private void validateRequiredItemCodes(Set<String> itemCodes, NotificationContainer notificationContainer) {
        if (!ParamAssertion.isNotNull(itemCodes)) {
            notificationContainer.addNotification("Missing required promotion item codes");
            return;
        }

        if (itemCodes.size() < REQUIRED_CODES_MIN_LENGTH) {
            notificationContainer.addNotification("You must provide at least %s required item codes to create promotion", REQUIRED_CODES_MIN_LENGTH);
            return;
        }

        Set<Code> codes = itemCodes.stream().map(Code::of).collect(Collectors.toSet());
        Set<Item> itemsFromCodes = itemRepository.findAllForCodes(codes);
        Boolean areAllItemCodesFoundInDb = codes.size() == itemsFromCodes.size();

        if (!areAllItemCodesFoundInDb) {
            Set<Code> foundItemsCodes = itemsFromCodes.stream().map(Item::getCode).collect(Collectors.toSet());
            notificationContainer.addNotification("Not all items found in DB for promotion! Found items codes: %s", foundItemsCodes);
        }
    }
}
