package szczepanski.gerard.market.checkout.service.item;

import lombok.Builder;
import lombok.Getter;

import java.io.Serializable;

@Getter
@Builder
public class ItemPresentationDto implements Serializable {
    private static final long serialVersionUID = 4127558444994660064L;

    private final Long id;
    private final String code;
    private final String name;
    private final String price;

}
