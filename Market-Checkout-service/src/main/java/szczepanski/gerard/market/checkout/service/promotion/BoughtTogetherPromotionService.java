package szczepanski.gerard.market.checkout.service.promotion;

import szczepanski.gerard.market.checkout.service.common.ValidationException;

/**
 * Service for bought together promotions in Market.
 *
 * @author Gerard Szczepa�ski
 */
public interface BoughtTogetherPromotionService {

    /**
     * Add BoughtTogetherPromotion. This promotion is 'Buy Coffee and tea and have 13% discount!'.
     *
     * @param creationDto - BoughtTogetherPromotionCreationDto object.
     * @return Code of the created promotion.
     * @throws ValidationException - Thrown when creationDto is invalid.
     */
    String addBoughtTogetherPromotion(BoughtTogetherPromotionCreationDto creationDto) throws ValidationException;


}
