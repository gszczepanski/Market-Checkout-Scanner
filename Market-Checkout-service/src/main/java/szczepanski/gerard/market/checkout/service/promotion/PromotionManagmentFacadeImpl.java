package szczepanski.gerard.market.checkout.service.promotion;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.service.common.ValidationException;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class PromotionManagmentFacadeImpl implements PromotionManagmentFacade {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(TotalPriceDiscountService.class.getName());

    private final ItemQuantityPromotionService itemQuantityPromotionService;
    private final BoughtTogetherPromotionService boughtTogetherPromotionService;
    private final TotalPriceDiscountService totalPriceDiscountService;

    @Override
    @Transactional
    public String addItemQuantityPromotion(ItemQuantityPromotionCreationDto creationDto) throws ValidationException {
        LOG.debug("Add ItemQuantityPromotion to market with dto: %s", creationDto);
        return itemQuantityPromotionService.addItemQuantityPromotion(creationDto);
    }

    @Override
    @Transactional
    public String addBoughtTogetherPromotion(BoughtTogetherPromotionCreationDto creationDto) throws ValidationException {
        LOG.debug("Add BoughtTogetherPromotion to market with dto: %s", creationDto);
        return boughtTogetherPromotionService.addBoughtTogetherPromotion(creationDto);
    }

    @Override
    @Transactional
    public String addCheckoutTotalPriceDiscount(TotalPriceDiscountCreationDto creationDto) throws ValidationException {
        LOG.debug("Add DiscountFromTotalBasketPrice market with dto: %s", creationDto);
        return totalPriceDiscountService.addCheckoutTotalPriceDiscount(creationDto);
    }

}
