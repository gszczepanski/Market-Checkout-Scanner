package szczepanski.gerard.market.checkout.service.promotion;

import lombok.*;

import java.io.Serializable;
import java.util.Set;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BoughtTogetherPromotionCreationDto implements Serializable {
    private static final long serialVersionUID = 4127558444994660064L;

    private String name;
    private Set<String> requiredItemCodes;
    private String discount;


}
