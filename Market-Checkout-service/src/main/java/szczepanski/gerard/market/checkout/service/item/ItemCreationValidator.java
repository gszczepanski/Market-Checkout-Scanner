package szczepanski.gerard.market.checkout.service.item;

import org.springframework.stereotype.Component;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.service.common.AbstractValidator;
import szczepanski.gerard.market.checkout.service.common.PriceDto;

@Component
class ItemCreationValidator extends AbstractValidator<ItemCreationDto> {

    @Override
    protected void doValidateObject(ItemCreationDto object, NotificationContainer notificationContainer) {

        String name = object.getName();
        validateName(name, notificationContainer);

        PriceDto price = object.getPrice();
        validatePrice(price, notificationContainer);
    }

    private void validateName(String name, NotificationContainer notificationContainer) {
        if (!ParamAssertion.isNotBlank(name)) {
            notificationContainer.addNotification("Name must be valid text!");
            return;
        }

        if (!ParamAssertion.isMaxLengthNotExceeded(name, Name.MAX_LENGTH)) {
            notificationContainer.addNotification("Name mast be max %s characters long", Name.MAX_LENGTH);
        }
    }

    private void validatePrice(PriceDto price, NotificationContainer notificationContainer) {
        if (!ParamAssertion.isNotNull(price)) {
            notificationContainer.addNotification("Price not found!");
            return;
        }

        if (!ParamAssertion.isNumberRepresentation(price.getValue())) {
            notificationContainer.addNotification("Price value must be number representation!");
        }

        if (!Currency.isValidCurrencyRepresentation(price.getCurrency())) {
            notificationContainer.addNotification("Currency representation is invalid.");
        }
    }

}
