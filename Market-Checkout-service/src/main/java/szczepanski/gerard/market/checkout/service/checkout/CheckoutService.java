package szczepanski.gerard.market.checkout.service.checkout;

/**
 * This Service handle the Scanner operations.
 *
 * @author Gerard Szczepa�ski
 */
public interface CheckoutService {

    /**
     * This method handle Scanner scan operation.
     * <br>
     * It tooks itemCode that was scanned by Scanner and adds that Item to
     * currently opened Checkout. If no Checkout is opened, that means that new transaction
     * started and new Checkout is created.
     *
     * @param checkoutNumber - number of the checkout to perform add item.
     * @param itemCode       - code of the scanned item by Scanner. If no item found (code is invalid) then Exception
     *                       is being thrown.
     * @return Current Checkout price as String value i.e. 112.33 USD
     */
    String scan(Integer checkoutNumber, String itemCode);

    /**
     * Tell currently opened Checkout that transaction is over and customer paid for
     * his items. This method close Checkout, persist that Checkout into database and then return
     * generated Reciept for Customer.
     *
     * @param checkoutNumber - number of the checkout to perform close basket and return recipe.
     * @return Reciept for customer as String.
     */
    String markAsPaid(Integer checkoutNumber);

    /**
     * Open checkout in Market.
     *
     * @param checkoutNumber - Number of the Checkout in Market.
     */
    void openCheckout(Integer checkoutNumber);
}
