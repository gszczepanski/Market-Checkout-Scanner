package szczepanski.gerard.market.checkout.service.common;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.io.Serializable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PriceDto implements Serializable {
    private static final long serialVersionUID = 4127558444994660064L;

    private String value;
    private String currency;

    @Override
    public String toString() {
        return "PriceDto [value=" + value + ", currency=" + currency + "]";
    }

}
