package szczepanski.gerard.market.checkout.service.common;

import szczepanski.gerard.market.checkout.component.common.utils.MarketCheckoutStringUtils;

import java.util.ArrayList;
import java.util.List;

public abstract class AbstractValidator<T> {

    public void validate(T object) throws ValidationException {
        NotificationContainer notificationContainer = new NotificationContainer();
        doValidateObject(object, notificationContainer);
        checkNotifications(notificationContainer);
    }

    protected abstract void doValidateObject(T object, NotificationContainer notificationContainer);

    private void checkNotifications(NotificationContainer notificationContainer) throws ValidationException {
        List<String> notifications = notificationContainer.notifications;
        if (notifications.isEmpty()) {
            return;
        }

        StringBuilder errorMessageBuilder = new StringBuilder();
        notifications.forEach(n -> errorMessageBuilder.append(n).append(MarketCheckoutStringUtils.NEXT_LINE));

        throw new ValidationException(errorMessageBuilder.toString());
    }

    public static class NotificationContainer {
        private final List<String> notifications = new ArrayList<>();

        public void addNotification(String text) {
            String notification = String.format(text, new Object[]{});
            notifications.add(notification);
        }

        public void addNotification(String text, Object... args) {
            String notification = String.format(text, args);
            notifications.add(notification);
        }

    }

}
