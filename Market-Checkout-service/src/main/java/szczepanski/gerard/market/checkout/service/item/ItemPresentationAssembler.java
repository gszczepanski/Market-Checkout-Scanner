package szczepanski.gerard.market.checkout.service.item;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import szczepanski.gerard.market.checkout.domain.item.Item;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
final class ItemPresentationAssembler {

    public static ItemPresentationDto toItemPresentationDto(Item item) {
        return ItemPresentationDto.builder()
                .id(item.getId())
                .code(item.getCode().toString())
                .name(item.getName().toString())
                .price(item.getRegularPrice().toString())
                .build();
    }

}
