package szczepanski.gerard.market.checkout.service.item;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.lock.AutoReleaseLock;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.item.ItemFactory;
import szczepanski.gerard.market.checkout.domain.item.ItemRepository;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;
import szczepanski.gerard.market.checkout.service.common.ValidationException;

import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class ItemManagmentServiceImpl implements ItemManagmentService {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(ItemManagmentService.class.getName());

    private static final Lock ADD_ITEM_LOCK = new ReentrantLock();

    private final ItemFactory itemFactory;
    private final ItemRepository itemRepository;
    private final ItemCreationValidator itemCreationValidator;

    @Override
    @Transactional
    public String addItemToMarket(ItemCreationDto creationDto) throws ValidationException {
        ParamAssertion.guardIsNotNull(creationDto);

        try (AutoReleaseLock lock = new AutoReleaseLock(ADD_ITEM_LOCK)) {
            lock.lock();
            return performAddItemToMarket(creationDto);
        }
    }

    private String performAddItemToMarket(ItemCreationDto creationDto) throws ValidationException {
        LOG.debug("Add Item to market storage with dto: %s", creationDto);
        itemCreationValidator.validate(creationDto);

        Name name = Name.of(creationDto.getName());
        MonetaryValue regularPrice = MonetaryValue.of(creationDto.getPrice().getValue(), Currency.valueOf(creationDto.getPrice().getCurrency()));
        Item item = itemFactory.createItem(name, regularPrice);

        itemRepository.save(item);
        return item.getCode().toString();
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<ItemPresentationDto> findOne(String itemCode) {
        ParamAssertion.guardIsNotBlank(itemCode);
        LOG.debug("Find one item with code: %s", itemCode);

        Optional<Item> optionalItem = itemRepository.findOneByCode(Code.of(itemCode));
        if (optionalItem.isPresent()) {
            ItemPresentationDto presentationDto = ItemPresentationAssembler.toItemPresentationDto(optionalItem.get());
            return Optional.of(presentationDto);
        }
        return Optional.empty();
    }

}
