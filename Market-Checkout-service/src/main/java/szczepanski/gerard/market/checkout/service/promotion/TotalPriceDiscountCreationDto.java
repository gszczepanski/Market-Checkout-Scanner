package szczepanski.gerard.market.checkout.service.promotion;

import lombok.*;
import szczepanski.gerard.market.checkout.service.common.PriceDto;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class TotalPriceDiscountCreationDto implements Serializable {
    private static final long serialVersionUID = 4127558444994660064L;

    private PriceDto fromPriceValue;
    private String discount;

    @Override
    public String toString() {
        return "CreateTotalPriceDiscountDto [fromPriceValue=" + fromPriceValue + ", discount=" + discount + "]";
    }

}
