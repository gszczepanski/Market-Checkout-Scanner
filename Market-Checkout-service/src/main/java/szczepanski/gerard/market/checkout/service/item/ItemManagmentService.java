package szczepanski.gerard.market.checkout.service.item;

import szczepanski.gerard.market.checkout.service.common.ValidationException;

import java.util.Optional;

/**
 * This Service component is responsible for
 * adding item to Market and obtaining informations about extisting Market items.
 *
 * @author Gerard Szczepanski
 */
public interface ItemManagmentService {

    /**
     * Add item to Market. If Item is invalid, then proper exception with explicid reason is being
     * thrown to the caller.
     *
     * @param creationDto - DTO object for create item. All fields are required.
     * @return system code of the created item.
     */
    String addItemToMarket(ItemCreationDto creationDto) throws ValidationException;

    /**
     * Find one item by its code. If no item found, then
     * empty Optional object is being returned to caller.
     *
     * @param itemCode - System code of the item to found.
     * @return Optional with item presentation dto.
     */
    Optional<ItemPresentationDto> findOne(String itemCode);

}
