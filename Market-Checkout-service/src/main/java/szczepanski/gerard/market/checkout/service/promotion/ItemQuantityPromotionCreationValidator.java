package szczepanski.gerard.market.checkout.service.promotion;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.item.ItemRepository;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.service.common.AbstractValidator;
import szczepanski.gerard.market.checkout.service.common.PriceDto;

@Component
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class ItemQuantityPromotionCreationValidator extends AbstractValidator<ItemQuantityPromotionCreationDto> {

    private final ItemRepository itemRepository;

    @Override
    protected void doValidateObject(ItemQuantityPromotionCreationDto object, NotificationContainer notificationContainer) {
        String name = object.getName();
        validateName(name, notificationContainer);

        String itemCode = object.getItemCode();
        validateItemCode(itemCode, notificationContainer);

        Integer requiredItemsQuantity = object.getRequiredItemsQuantity();
        validareRequiredItemsQuantity(requiredItemsQuantity, notificationContainer);

        PriceDto price = object.getPrice();
        validatePrice(price, notificationContainer);
    }

    private void validateName(String name, NotificationContainer notificationContainer) {
        if (!ParamAssertion.isNotBlank(name)) {
            notificationContainer.addNotification("Name must be valid text!");
            return;
        }

        if (!ParamAssertion.isMaxLengthNotExceeded(name, Name.MAX_LENGTH)) {
            notificationContainer.addNotification("Name mast be max %s characters long", Name.MAX_LENGTH);
        }
    }

    private void validateItemCode(String itemCode, NotificationContainer notificationContainer) {
        if (!ParamAssertion.isNotBlank(itemCode)) {
            notificationContainer.addNotification("Code of the item must be valid text!");
            return;
        }

        Code code = Code.of(itemCode);
        Boolean isItemWithCodeInMarket = itemRepository.isItemWithCodeInMarket(code);
        if (!isItemWithCodeInMarket) {
            notificationContainer.addNotification("Item with code %s not found in Market!", code);
        }
    }

    private void validatePrice(PriceDto price, NotificationContainer notificationContainer) {
        if (!ParamAssertion.isNotNull(price)) {
            notificationContainer.addNotification("Price not found!");
            return;
        }

        if (!ParamAssertion.isNumberRepresentation(price.getValue())) {
            notificationContainer.addNotification("Price value must be number representation!");
        }

        if (!Currency.isValidCurrencyRepresentation(price.getCurrency())) {
            notificationContainer.addNotification("Currency representation is invalid.");
        }
    }

    private void validareRequiredItemsQuantity(Integer requiredItemsQuantity, NotificationContainer notificationContainer) {
        if (!ParamAssertion.isNotNull(requiredItemsQuantity)) {
            notificationContainer.addNotification("Required items quantity is null!");
            return;
        }

        if (requiredItemsQuantity < 1) {
            notificationContainer.addNotification("Required items quantity musi be positiva value!");
        }
    }

}
