package szczepanski.gerard.market.checkout.service.item;

import lombok.*;
import szczepanski.gerard.market.checkout.service.common.PriceDto;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ItemCreationDto implements Serializable {
    private static final long serialVersionUID = 4127558444994660064L;

    private String name;
    private PriceDto price;

    @Override
    public String toString() {
        return "ItemCreationDto [name=" + name + ", price=" + price + "]";
    }
}
