package szczepanski.gerard.market.checkout.service.common;

public class ValidationException extends Throwable {
    private static final long serialVersionUID = 8281735100533405920L;

    public ValidationException(String message) {
        super(message);
    }

}
