package szczepanski.gerard.market.checkout.service.checkout;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.exception.MarketCheckoutRuntimeException;
import szczepanski.gerard.market.checkout.component.common.lock.AutoReleaseLock;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.domain.basket.Basket;
import szczepanski.gerard.market.checkout.domain.basket.BasketFactory;
import szczepanski.gerard.market.checkout.domain.basket.BasketRepository;
import szczepanski.gerard.market.checkout.domain.checkout.Checkout;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.item.ItemRepository;
import szczepanski.gerard.market.checkout.domain.warehouse.WarehouseUpdater;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class CheckoutServiceImpl implements CheckoutService {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(CheckoutService.class.getName());

    private static final Lock CHECK_BASKET_OPENED_LOCK = new ReentrantLock();
    private static final Lock OPEN_CHECKOUT_LOCK = new ReentrantLock();

    private final BasketFactory basketFactory;
    private final BasketRepository basketRepository;
    private final ItemRepository itemRepository;
    private final WarehouseUpdater warehouseUpdater;

    private Map<Integer, Checkout> checkouts = new HashMap<>();

    @Override
    @Transactional(readOnly = true)
    public String scan(Integer checkoutNumber, String itemCode) {
        ParamAssertion.guardIsNotNull(checkoutNumber);
        ParamAssertion.guardIsNotBlank(itemCode);
        checkIfCheckoutWithNumberIsOpenedInMarket(checkoutNumber);
        LOG.debug("Checkout: %s, scanned item with code: %s", checkoutNumber, itemCode);

        Checkout checkout = this.checkouts.get(checkoutNumber);
        return doScan(checkout, itemCode);
    }

    private String doScan(Checkout checkout, String itemCode) {
        initializeNewBasketIfNeed(checkout);

        Optional<Item> optionalItem = itemRepository.findOneByCode(Code.of(itemCode));
        if (optionalItem.isPresent()) {
            Item scannedItem = optionalItem.get();
            checkout.scanItem(scannedItem);
        } else {
            LOG.debug("Item with code %s not found in Market. No item added to checkout", itemCode);
        }
        return checkout.totalValue().toString();
    }

    private void initializeNewBasketIfNeed(Checkout checkout) {
        try (AutoReleaseLock lock = new AutoReleaseLock(CHECK_BASKET_OPENED_LOCK)) {
            lock.lock();

            if (!checkout.hasOpenedBasket()) {
                Basket newBasket = basketFactory.create();
                checkout.startHandlingNewBasket(newBasket);
            }
        }
    }

    @Override
    @Transactional
    public String markAsPaid(Integer checkoutNumber) {
        ParamAssertion.guardIsNotNull(checkoutNumber);
        checkIfCheckoutWithNumberIsOpenedInMarket(checkoutNumber);
        LOG.debug("Close checkout with numbet: %s", checkoutNumber);

        Checkout checkout = this.checkouts.get(checkoutNumber);
        Basket closedBasketFromCheckout = checkout.closeThenGetCurrentBasket();
        basketRepository.save(closedBasketFromCheckout);
        warehouseUpdater.updateForClosedBasket(closedBasketFromCheckout);

        return closedBasketFromCheckout.recipe().getRecipeText();
    }

    private void checkIfCheckoutWithNumberIsOpenedInMarket(Integer checkoutNumber) {
        if (!this.checkouts.containsKey(checkoutNumber)) {
            throw new MarketCheckoutRuntimeException(String.format("Checkout with number %s not found in Market!", checkoutNumber));
        }
    }

    @Override
    public void openCheckout(Integer checkoutNumber) {
        ParamAssertion.guardIsNotNull(checkoutNumber);

        try (AutoReleaseLock lock = new AutoReleaseLock(OPEN_CHECKOUT_LOCK)) {
            lock.lock();
            doOpenCheckout(checkoutNumber);
        }
    }

    private void doOpenCheckout(Integer checkoutNumber) {
        if (!this.checkouts.containsKey(checkoutNumber)) {
            LOG.debug("Open checkout with number: %s", checkoutNumber);
            Checkout checkout = Checkout.createWithNumber(checkoutNumber);
            this.checkouts.put(checkoutNumber, checkout);
        }
    }
}
