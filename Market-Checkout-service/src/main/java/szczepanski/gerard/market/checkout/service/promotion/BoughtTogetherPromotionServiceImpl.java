package szczepanski.gerard.market.checkout.service.promotion;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.lock.AutoReleaseLock;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.promotion.BoughtTogetherPromotion;
import szczepanski.gerard.market.checkout.domain.promotion.BoughtTogetherPromotionFactory;
import szczepanski.gerard.market.checkout.domain.promotion.BoughtTogetherPromotionRepository;
import szczepanski.gerard.market.checkout.domain.promotion.Discount;
import szczepanski.gerard.market.checkout.service.common.ValidationException;

import java.util.Set;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class BoughtTogetherPromotionServiceImpl implements BoughtTogetherPromotionService {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(BoughtTogetherPromotionService.class.getName());

    private static final Lock ADD_BOUGHT_TOGETHER_PROMOTION_LOCK = new ReentrantLock();

    private final BoughtTogetherPromotionFactory boughtTogetherPromotionFactory;
    private final BoughtTogetherPromotionRepository boughtTogetherPromotionRepository;
    private final BoughtTogetherPromotionCreationValidator boughtTogetherPromotionCreationValidator;

    @Override
    @Transactional
    public String addBoughtTogetherPromotion(BoughtTogetherPromotionCreationDto creationDto) throws ValidationException {
        ParamAssertion.guardIsNotNull(creationDto);

        try (AutoReleaseLock lock = new AutoReleaseLock(ADD_BOUGHT_TOGETHER_PROMOTION_LOCK)) {
            lock.lock();
            return performAddBoughtTogetherPromotion(creationDto);
        }
    }

    private String performAddBoughtTogetherPromotion(BoughtTogetherPromotionCreationDto creationDto) throws ValidationException {
        LOG.debug("Add BoughtTogetherPromotion to market with dto: %s", creationDto);
        boughtTogetherPromotionCreationValidator.validate(creationDto);

        Name name = Name.of(creationDto.getName());
        Discount discount = Discount.of(creationDto.getDiscount());
        Set<Code> promotionItemCodes = creationDto.getRequiredItemCodes().stream().map(Code::of).collect(Collectors.toSet());

        BoughtTogetherPromotion boughtTogetherPromotion = boughtTogetherPromotionFactory.createBoughtTogetherPromotion(name, discount, promotionItemCodes);
        boughtTogetherPromotionRepository.save(boughtTogetherPromotion);

        return boughtTogetherPromotion.getCode().toString();
    }

}
