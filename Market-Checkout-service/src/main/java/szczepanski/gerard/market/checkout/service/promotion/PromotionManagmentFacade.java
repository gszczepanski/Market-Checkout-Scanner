package szczepanski.gerard.market.checkout.service.promotion;

import szczepanski.gerard.market.checkout.service.common.ValidationException;

/**
 * This facade is designed for
 * add promotions to Market.
 * <br><br>
 * There are three kinds of promotions/discount in Market:
 * <br>
 * 1) Buy N items and pay X (3 Milks for 3.99)
 * <br>
 * 2) Buy X,Y,Z item and have X% discount to these items (Milk and Juice bought together 20% cheaper!)
 * <br>
 * 3) If your Checkout total price reach some value (i.e. 600 UDS), then apply 5% discount.
 * <br>
 * <br>
 * All of these promotions/discounts can be added by this facade.
 *
 * @author Gerard Szczepa�ski
 */
public interface PromotionManagmentFacade {

    /**
     * Add ItemQuantityPromotion. This promotion is 'Buy 3 Milks times for 10.99'.
     * <br><br>
     *
     * @param creationDto - ItemQuantityPromotionCreationDto object.
     * @return Code of the created promotion.
     * @throws ValidationException - Thrown when creationDto is invalid.
     */
    String addItemQuantityPromotion(ItemQuantityPromotionCreationDto creationDto) throws ValidationException;

    /**
     * Add BoughtTogetherPromotion. This promotion is 'Buy Coffee and tea and have 13% discount!'.
     *
     * @param creationDto - BoughtTogetherPromotionCreationDto object.
     * @return Code of the created promotion.
     * @throws ValidationException - Thrown when creationDto is invalid.
     */
    String addBoughtTogetherPromotion(BoughtTogetherPromotionCreationDto creationDto) throws ValidationException;

    /**
     * Total checkout discount. 'But products for 600 USD and get 10% discount!'.
     *
     * @param creationDto - TotalPriceDiscountCreationDto object.
     * @return Code of the created discount.
     * @throws ValidationException - Thrown when creationDto is invalid.
     */
    String addCheckoutTotalPriceDiscount(TotalPriceDiscountCreationDto creationDto) throws ValidationException;

}
