package szczepanski.gerard.market.checkout.service.promotion;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.lock.AutoReleaseLock;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;
import szczepanski.gerard.market.checkout.domain.promotion.Discount;
import szczepanski.gerard.market.checkout.domain.promotion.DiscountFromTotalBasketPrice;
import szczepanski.gerard.market.checkout.domain.promotion.DiscountFromTotalBasketPriceFactory;
import szczepanski.gerard.market.checkout.domain.promotion.DiscountFromTotalBasketPriceRepository;
import szczepanski.gerard.market.checkout.service.common.ValidationException;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class TotalPriceDiscountServiceImpl implements TotalPriceDiscountService {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(TotalPriceDiscountService.class.getName());

    private static final Lock ADD_CHECKOUT_TOTAL_DISCOUT_LOCK = new ReentrantLock();

    private final DiscountFromTotalBasketPriceFactory discountFromTotalCheckoutPriceFactory;
    private final DiscountFromTotalBasketPriceRepository discountFromTotalCheckoutPriceRepository;
    private final TotalPriceDiscountCreationValidator totalPriceDiscountCreationValidator;

    @Override
    @Transactional
    public String addCheckoutTotalPriceDiscount(TotalPriceDiscountCreationDto creationDto) throws ValidationException {
        ParamAssertion.guardIsNotNull(creationDto);

        try (AutoReleaseLock lock = new AutoReleaseLock(ADD_CHECKOUT_TOTAL_DISCOUT_LOCK)) {
            lock.lock();
            return performAddCheckoutTotalPriceDiscount(creationDto);
        }
    }

    private String performAddCheckoutTotalPriceDiscount(TotalPriceDiscountCreationDto creationDto) throws ValidationException {
        LOG.debug("Add DiscountFromTotalBasketPrice market with dto: %s", creationDto);
        totalPriceDiscountCreationValidator.validate(creationDto);

        MonetaryValue fromCheckoutPrice = MonetaryValue.of(creationDto.getFromPriceValue().getValue(), Currency.valueOf(creationDto.getFromPriceValue().getCurrency()));
        Discount discount = Discount.of(creationDto.getDiscount());
        DiscountFromTotalBasketPrice createdDiscount = discountFromTotalCheckoutPriceFactory.createTotalDiscount(fromCheckoutPrice, discount);

        discountFromTotalCheckoutPriceRepository.save(createdDiscount);
        return createdDiscount.getCode().toString();
    }

}
