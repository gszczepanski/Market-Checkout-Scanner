package szczepanski.gerard.market.checkout.service.promotion;

import lombok.*;
import szczepanski.gerard.market.checkout.service.common.PriceDto;

import java.io.Serializable;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ItemQuantityPromotionCreationDto implements Serializable {
    private static final long serialVersionUID = 4127558444994660064L;

    private String name;
    private String itemCode;
    private Integer requiredItemsQuantity;
    private PriceDto price;

    @Override
    public String toString() {
        return "ItemQuantityPromotionCreationDto [name=" + name + ", itemCode=" + itemCode + ", requiredItemsQuantity=" + requiredItemsQuantity + ", price="
                + price + "]";
    }

}
