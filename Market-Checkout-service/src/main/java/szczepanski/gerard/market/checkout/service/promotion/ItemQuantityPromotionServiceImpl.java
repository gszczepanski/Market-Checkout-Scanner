package szczepanski.gerard.market.checkout.service.promotion;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.component.common.lock.AutoReleaseLock;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLogger;
import szczepanski.gerard.market.checkout.component.common.logger.MarketLoggerFactory;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;
import szczepanski.gerard.market.checkout.domain.promotion.ItemQuantityPromotion;
import szczepanski.gerard.market.checkout.domain.promotion.ItemQuantityPromotionFactory;
import szczepanski.gerard.market.checkout.domain.promotion.ItemQuantityPromotionRepository;
import szczepanski.gerard.market.checkout.service.common.ValidationException;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@Service
@RequiredArgsConstructor(onConstructor = @__(@Autowired))
class ItemQuantityPromotionServiceImpl implements ItemQuantityPromotionService {
    private static final MarketLogger LOG = MarketLoggerFactory.createForName(TotalPriceDiscountService.class.getName());

    private static final Lock ADD_ITEM_QUANTITY_PROMOTION_LOCK = new ReentrantLock();

    private final ItemQuantityPromotionFactory itemQuantityPromotionFactory;
    private final ItemQuantityPromotionRepository itemQuantityPromotionRepository;
    private final ItemQuantityPromotionCreationValidator itemQuantityPromotionCreationValidator;

    @Override
    @Transactional
    public String addItemQuantityPromotion(ItemQuantityPromotionCreationDto creationDto) throws ValidationException {
        ParamAssertion.guardIsNotNull(creationDto);

        try (AutoReleaseLock lock = new AutoReleaseLock(ADD_ITEM_QUANTITY_PROMOTION_LOCK)) {
            lock.lock();
            return performAddItemQuantityPromotion(creationDto);
        }
    }

    private String performAddItemQuantityPromotion(ItemQuantityPromotionCreationDto creationDto) throws ValidationException {
        LOG.debug("Add ItemQuantityPromotion to market with dto: %s", creationDto);
        itemQuantityPromotionCreationValidator.validate(creationDto);

        Name nameOfThePromotion = Name.of(creationDto.getName());
        Code promotedItemCode = Code.of(creationDto.getItemCode());
        Integer requiredItemQuantity = creationDto.getRequiredItemsQuantity();
        MonetaryValue priceForItems = MonetaryValue.of(creationDto.getPrice().getValue(), Currency.valueOf(creationDto.getPrice().getCurrency()));

        ItemQuantityPromotion itemQuantityPromotion = itemQuantityPromotionFactory.createItemQuantityPromotion(nameOfThePromotion, promotedItemCode, requiredItemQuantity, priceForItems);
        itemQuantityPromotionRepository.save(itemQuantityPromotion);

        return itemQuantityPromotion.getCode().toString();
    }

}
