package szczepanski.gerard.market.checkout.service.promotion;

import szczepanski.gerard.market.checkout.service.common.ValidationException;

/**
 * Service for total discount.
 *
 * @author Gerard Szczepa�ski
 */
public interface TotalPriceDiscountService {

    /**
     * Add Total checkout discount. 'But products for 600 USD and get 10% discount!'.
     *
     * @param creationDto - TotalPriceDiscountCreationDto object.
     * @return Code of the created discount.
     * @throws ValidationException - Thrown when creationDto is invalid.
     */
    String addCheckoutTotalPriceDiscount(TotalPriceDiscountCreationDto creationDto) throws ValidationException;

}
