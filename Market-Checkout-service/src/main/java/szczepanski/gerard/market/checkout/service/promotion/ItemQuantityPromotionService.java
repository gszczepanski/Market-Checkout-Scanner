package szczepanski.gerard.market.checkout.service.promotion;

import szczepanski.gerard.market.checkout.service.common.ValidationException;

/**
 * Service for item quantity promotions in Market.
 *
 * @author Gerard Szczepa�ski
 */
public interface ItemQuantityPromotionService {

    /**
     * Add ItemQuantityPromotion. This promotion is 'Buy 3 Milks times for 10.99'.
     * <br><br>
     *
     * @param creationDto - ItemQuantityPromotionCreationDto object.
     * @return Code of the created promotion.
     * @throws ValidationException - Thrown when creationDto is invalid.
     */
    String addItemQuantityPromotion(ItemQuantityPromotionCreationDto creationDto) throws ValidationException;

}
