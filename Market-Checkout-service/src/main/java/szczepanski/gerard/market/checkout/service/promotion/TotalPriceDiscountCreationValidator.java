package szczepanski.gerard.market.checkout.service.promotion;

import org.springframework.stereotype.Component;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertion;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.service.common.AbstractValidator;
import szczepanski.gerard.market.checkout.service.common.PriceDto;

@Component
class TotalPriceDiscountCreationValidator extends AbstractValidator<TotalPriceDiscountCreationDto> {

    @Override
    protected void doValidateObject(TotalPriceDiscountCreationDto object, NotificationContainer notificationContainer) {
        PriceDto price = object.getFromPriceValue();
        validatePrice(price, notificationContainer);

        String discount = object.getDiscount();
        validateDiscount(discount, notificationContainer);
    }

    private void validateDiscount(String discount, NotificationContainer notificationContainer) {
        if (!ParamAssertion.isNotBlank(discount)) {
            notificationContainer.addNotification("Discount must be valid text!");
            return;
        }

        if (!ParamAssertion.isPercentageStringValue(discount)) {
            notificationContainer.addNotification("Discount is invalid. Valid discount formats: 18%% | 18.00%%");
        }
    }

    private void validatePrice(PriceDto price, NotificationContainer notificationContainer) {
        if (!ParamAssertion.isNotNull(price)) {
            notificationContainer.addNotification("Price not found!");
            return;
        }

        if (!ParamAssertion.isNumberRepresentation(price.getValue())) {
            notificationContainer.addNotification("Price value must be number representation!");
        }

        if (!Currency.isValidCurrencyRepresentation(price.getCurrency())) {
            notificationContainer.addNotification("Currency representation is invalid.");
        }
    }

}
