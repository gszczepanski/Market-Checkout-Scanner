package szczepanski.gerard.market.checkout.service.promotion;

import org.assertj.core.api.Assertions;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;
import szczepanski.gerard.market.checkout.domain.promotion.ItemQuantityPromotion;
import szczepanski.gerard.market.checkout.domain.promotion.ItemQuantityPromotionFactory;
import szczepanski.gerard.market.checkout.domain.promotion.ItemQuantityPromotionRepository;
import szczepanski.gerard.market.checkout.service.common.PriceDto;
import szczepanski.gerard.market.checkout.service.common.ValidationException;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@Test(suiteName = "unit-tests")
public class ItemQuantityPromotionServiceImplTest {

    ItemQuantityPromotionFactory itemQuantityPromotionFactory;
    ItemQuantityPromotionRepository itemQuantityPromotionRepository;
    ItemQuantityPromotionCreationValidator itemQuantityPromotionCreationValidator;
    ItemQuantityPromotionService service;

    @BeforeMethod
    public void beforeMethod() {
        itemQuantityPromotionFactory = mock(ItemQuantityPromotionFactory.class);
        itemQuantityPromotionRepository = mock(ItemQuantityPromotionRepository.class);
        itemQuantityPromotionCreationValidator = mock(ItemQuantityPromotionCreationValidator.class);

        service = new ItemQuantityPromotionServiceImpl(itemQuantityPromotionFactory, itemQuantityPromotionRepository, itemQuantityPromotionCreationValidator);
    }

    @Test
    public void whenPromotionIsValidDuringAddPromotionThenPromotionIsCreatedAndSavedIntoDb() throws ValidationException {
        // Arrange
        String rawItemName = "3 for 2";
        Name name = Name.of(rawItemName);

        String rawCodeForItem = "item-0001";
        Code codeForItem = Code.of(rawCodeForItem);

        Integer requiredItemsQuantity = 1;
        MonetaryValue promotionPrice = MonetaryValue.of("16.99", Currency.USD);

        ItemQuantityPromotionCreationDto creationDto = ItemQuantityPromotionCreationDto.builder()
                .itemCode(rawCodeForItem)
                .name(rawItemName)
                .price(new PriceDto("16.99", "USD"))
                .requiredItemsQuantity(requiredItemsQuantity)
                .build();

        String expectedCreatedPromotionCode = "iqp-0111";
        Code createdPromotionCode = Code.of(expectedCreatedPromotionCode);

        ItemQuantityPromotion createdItemQuantityPromotion = ItemQuantityPromotion.builder()
                .code(createdPromotionCode)
                .build();

        when(itemQuantityPromotionFactory.createItemQuantityPromotion(name, codeForItem, requiredItemsQuantity, promotionPrice)).thenReturn(createdItemQuantityPromotion);

        // Act
        String code = service.addItemQuantityPromotion(creationDto);

        // Assert
        assertThat(code).isEqualTo(expectedCreatedPromotionCode);

        verify(itemQuantityPromotionCreationValidator).validate(creationDto);
        verify(itemQuantityPromotionFactory).createItemQuantityPromotion(name, codeForItem, requiredItemsQuantity, promotionPrice);
        verify(itemQuantityPromotionRepository).save(createdItemQuantityPromotion);
    }


    @Test(expectedExceptions = ValidationException.class)
    public void whenDtoIsInvalidDuringAddPromotionThenThrowValidationException() throws ValidationException {
        // Arrange
        ItemQuantityPromotionCreationDto invalidCreationDto = ItemQuantityPromotionCreationDto.builder()
                .build();

        doThrow(ValidationException.class).when(itemQuantityPromotionCreationValidator).validate(invalidCreationDto);

        // Act
        service.addItemQuantityPromotion(invalidCreationDto);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenCreationDtoIsNullDuringAddPromotionThenThrowParamAssertionException() throws ValidationException {
        // Arrange
        ItemQuantityPromotionCreationDto NULL_CREATION_DTO = null;

        // Act
        service.addItemQuantityPromotion(NULL_CREATION_DTO);
    }

}
