package szczepanski.gerard.market.checkout.service.promotion;

import org.apache.commons.lang3.StringUtils;
import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.item.ItemRepository;
import szczepanski.gerard.market.checkout.service.common.PriceDto;
import szczepanski.gerard.market.checkout.service.common.ValidationException;

import static org.mockito.Mockito.*;

@Test(suiteName = "unit-tests")
public class ItemQuantityPromotionCreationValidatorTest {

    ItemRepository itemRepository;
    ItemQuantityPromotionCreationValidator validator;

    @BeforeMethod
    public void beforeMethod() {
        itemRepository = mock(ItemRepository.class);
        validator = new ItemQuantityPromotionCreationValidator(itemRepository);
    }

    @Test
    public void whenObjectForValidationIsValidThenDoNotThrowException() throws ValidationException {
        // Arrange
        ItemQuantityPromotionCreationDto validObject = validObject();
        String rawItemCode = validObject.getItemCode();
        Code itemCode = Code.of(rawItemCode);

        when(itemRepository.isItemWithCodeInMarket(itemCode)).thenReturn(true);

        // Act
        validator.validate(validObject);

        // Assert
        verify(itemRepository).isItemWithCodeInMarket(itemCode);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenObjectForValidationHasNullNameThenThrowValidationException() throws ValidationException {
        // Arrange
        ItemQuantityPromotionCreationDto validObject = validObject();
        String rawItemCode = validObject.getItemCode();
        Code itemCode = Code.of(rawItemCode);

        String NULL_NAME = null;
        validObject.setName(NULL_NAME);

        when(itemRepository.isItemWithCodeInMarket(itemCode)).thenReturn(true);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenObjectForValidationHasBlankNameThenThrowValidationException() throws ValidationException {
        // Arrange
        ItemQuantityPromotionCreationDto validObject = validObject();
        String rawItemCode = validObject.getItemCode();
        Code itemCode = Code.of(rawItemCode);

        String BLANK_NAME = "";
        validObject.setName(BLANK_NAME);

        when(itemRepository.isItemWithCodeInMarket(itemCode)).thenReturn(true);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenObjectForValidationNameIsTooLongThenThrowValidationException() throws ValidationException {
        // Arrange
        ItemQuantityPromotionCreationDto validObject = validObject();
        String rawItemCode = validObject.getItemCode();
        Code itemCode = Code.of(rawItemCode);

        int exceededLenght = Name.MAX_LENGTH + 1;
        String TOO_LONG_NAME = StringUtils.repeat('a', exceededLenght);
        validObject.setName(TOO_LONG_NAME);

        when(itemRepository.isItemWithCodeInMarket(itemCode)).thenReturn(true);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenObjectForValidationHasNullItemCodeThenThrowValidationException() throws ValidationException {
        // Arrange
        ItemQuantityPromotionCreationDto validObject = validObject();

        String NULL_CODE = null;
        validObject.setItemCode(NULL_CODE);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenObjectForValidationHasClankItemCodeThenThrowValidationException() throws ValidationException {
        // Arrange
        ItemQuantityPromotionCreationDto validObject = validObject();

        String BLANK_CODE = "";
        validObject.setItemCode(BLANK_CODE);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenObjectForValidationHasCodeForItemThatNotExistInDbThenThrowValidationException() throws ValidationException {
        // Arrange
        ItemQuantityPromotionCreationDto validObject = validObject();
        String rawItemCode = validObject.getItemCode();
        Code notExistingItemCode = Code.of(rawItemCode);

        when(itemRepository.isItemWithCodeInMarket(notExistingItemCode)).thenReturn(false);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenObjectForValidationHasNullPriceThenThrowValidationException() throws ValidationException {
        // Arrange
        ItemQuantityPromotionCreationDto validObject = validObject();
        String rawItemCode = validObject.getItemCode();
        Code itemCode = Code.of(rawItemCode);

        PriceDto NULL_PRICE = null;
        validObject.setPrice(NULL_PRICE);

        when(itemRepository.isItemWithCodeInMarket(itemCode)).thenReturn(true);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenObjectForValidationHasInvalidPriceValueThenThrowValidationException() throws ValidationException {
        // Arrange
        ItemQuantityPromotionCreationDto validObject = validObject();
        String rawItemCode = validObject.getItemCode();
        Code itemCode = Code.of(rawItemCode);

        String INVALID_PRICE_VALUE = "invalidPriceValue";
        validObject.getPrice().setValue(INVALID_PRICE_VALUE);

        when(itemRepository.isItemWithCodeInMarket(itemCode)).thenReturn(true);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenObjectForValidationHasInvalidCurrencyValueThenThrowValidationException() throws ValidationException {
        // Arrange
        ItemQuantityPromotionCreationDto validObject = validObject();
        String rawItemCode = validObject.getItemCode();
        Code itemCode = Code.of(rawItemCode);

        String INVALID_CURRENCY_VALUE = "invalidCurrencyValue";
        validObject.getPrice().setCurrency(INVALID_CURRENCY_VALUE);

        when(itemRepository.isItemWithCodeInMarket(itemCode)).thenReturn(true);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenObjectForValidationHasInvalidRequiredQuantityValueThenThrowValidationException() throws ValidationException {
        // Arrange
        ItemQuantityPromotionCreationDto validObject = validObject();
        String rawItemCode = validObject.getItemCode();
        Code itemCode = Code.of(rawItemCode);

        Integer NULL_REQUIRED_QUANTITY_VALUE = null;
        validObject.setRequiredItemsQuantity(NULL_REQUIRED_QUANTITY_VALUE);

        when(itemRepository.isItemWithCodeInMarket(itemCode)).thenReturn(true);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenObjectForValidationHasNegativeRequiredQuantityValueThenThrowValidationException() throws ValidationException {
        // Arrange
        ItemQuantityPromotionCreationDto validObject = validObject();
        String rawItemCode = validObject.getItemCode();
        Code itemCode = Code.of(rawItemCode);

        Integer NEGATIVE_REQUIRED_QUANTITY_VALUE = -1;
        validObject.setRequiredItemsQuantity(NEGATIVE_REQUIRED_QUANTITY_VALUE);

        when(itemRepository.isItemWithCodeInMarket(itemCode)).thenReturn(true);

        // Act
        validator.validate(validObject);
    }

    public ItemQuantityPromotionCreationDto validObject() {
        return ItemQuantityPromotionCreationDto.builder()
                .itemCode("item-0001")
                .name("3 for 2 tunas")
                .price(new PriceDto("2.99", "USD"))
                .requiredItemsQuantity(3)
                .build();
    }
}
