package szczepanski.gerard.market.checkout.service.item;

import java.util.Optional;

import org.assertj.core.api.Assertions;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.item.ItemFactory;
import szczepanski.gerard.market.checkout.domain.item.ItemRepository;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;
import szczepanski.gerard.market.checkout.service.common.PriceDto;
import szczepanski.gerard.market.checkout.service.common.ValidationException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@Test(suiteName = "unit-tests")
public class ItemManagmentServiceImplTest {

    ItemManagmentService itemManagmentService;
    ItemFactory itemFactory;
    ItemRepository itemRepository;
    ItemCreationValidator itemCreationValidator;

    @BeforeMethod
    public void beforeMethod() {
        itemFactory = mock(ItemFactory.class);
        itemRepository = mock(ItemRepository.class);
        itemCreationValidator = mock(ItemCreationValidator.class);

        itemManagmentService = new ItemManagmentServiceImpl(itemFactory, itemRepository, itemCreationValidator);
    }

    @Test
    public void whenItemIsValidDuringAddItemThenItemIsCreatedAndSavedIntoDb() throws ValidationException {
        // Arrange
        String rawItemName = "Milk";

        Name expectedName = Name.of(rawItemName);
        MonetaryValue expectedMonetaryValue = MonetaryValue.of("21.00", Currency.USD);

        ItemCreationDto validCreationDto = ItemCreationDto.builder()
                .name(rawItemName)
                .price(new PriceDto("21.00", "USD"))
                .build();

        String expectedCode = "ITEM-0002";
        Item createdItem = Item.builder()
                .code(Code.of(expectedCode))
                .build();

        when(itemFactory.createItem(expectedName, expectedMonetaryValue)).thenReturn(createdItem);

        // Act
        String codeOfNewlyCreatedItem = itemManagmentService.addItemToMarket(validCreationDto);

        // Assert
        assertThat(codeOfNewlyCreatedItem).isEqualTo(expectedCode);

        verify(itemCreationValidator).validate(validCreationDto);
        verify(itemFactory).createItem(expectedName, expectedMonetaryValue);
        verify(itemRepository).save(createdItem);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenItemIsInvalidDuringAddItemThenThrowValidationException() throws ValidationException {
        // Arrange
        ItemCreationDto invalidCreationItem = ItemCreationDto.builder()
                .build();

        Mockito.doThrow(ValidationException.class).when(itemCreationValidator).validate(invalidCreationItem);

        // Act
        itemManagmentService.addItemToMarket(invalidCreationItem);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenItemIsNullDuringAddItemThenThrowParamAssertionException() throws ValidationException {
        // Arrange
        ItemCreationDto NULL_CREATION_ITEM = null;

        // Act
        itemManagmentService.addItemToMarket(NULL_CREATION_ITEM);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenCodeIsNullDuringFindOneThenThrowParamAssertionException() {
        // Arrange
        String NULL_CODE = null;

        //Act
        itemManagmentService.findOne(NULL_CODE);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenCodeIsBlankDuringFindOneThenThrowParamAssertionException() {
        // Arrange
        String BLANK_CODE = "";

        //Act
        itemManagmentService.findOne(BLANK_CODE);
    }

    @Test
    public void whenItemIsNotFoundDuringFindOneThenReturnEmptyOptional() {
        // Arrange

        String rawCode = "ITEM-9999";
        Code codeofNotExistingItem = Code.of(rawCode);
        Optional<Item> notFoundItemOptional = Optional.empty();

        when(itemRepository.findOneByCode(codeofNotExistingItem)).thenReturn(notFoundItemOptional);

        // Act
        Optional<ItemPresentationDto> optionalFoundItem = itemManagmentService.findOne(rawCode);

        // Assert
        Assert.assertFalse(optionalFoundItem.isPresent());
        verify(itemRepository).findOneByCode(codeofNotExistingItem);
    }

    @Test
    public void whenItemIsFoundDuringFindOneThenReturnOptionalWithPresentationData() {
        // Arrange

        Long expectedId = 1L;
        String expectedName = "Milk";
        String expectedRegularPrice = "21.00 USD";

        String rawCode = "ITEM-0001";
        Code codeOfItem = Code.of(rawCode);

        Item item = Item.builder()
                .code(codeOfItem)
                .name(Name.of(expectedName))
                .regularPrice(MonetaryValue.of("21.00", Currency.USD))
                .build();
        item.setId(expectedId);

        Optional<Item> optionalItem = Optional.of(item);
        when(itemRepository.findOneByCode(codeOfItem)).thenReturn(optionalItem);

        // Act
        Optional<ItemPresentationDto> optionalFoundItem = itemManagmentService.findOne(rawCode);

        // Assert
        Assert.assertTrue(optionalFoundItem.isPresent());

        ItemPresentationDto itemPresentationDto = optionalFoundItem.get();
        Assert.assertEquals(itemPresentationDto.getId(), expectedId);
        Assert.assertEquals(itemPresentationDto.getCode(), rawCode);
        Assert.assertEquals(itemPresentationDto.getName(), expectedName);
        Assert.assertEquals(itemPresentationDto.getPrice(), expectedRegularPrice);

        verify(itemRepository).findOneByCode(codeOfItem);
    }


}
