package szczepanski.gerard.market.checkout.service.promotion;

import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.service.common.PriceDto;
import szczepanski.gerard.market.checkout.service.common.ValidationException;

@Test(suiteName = "unit-tests")
public class TotalPriceDiscountCreationValidatorTest {

    @Test
    public void whenObjectForValidationIsValidThenDoNotThrowException() throws ValidationException {
        // Arrange
        TotalPriceDiscountCreationValidator validator = new TotalPriceDiscountCreationValidator();
        TotalPriceDiscountCreationDto validObject = validObject();

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenPriceIsNullInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        TotalPriceDiscountCreationValidator validator = new TotalPriceDiscountCreationValidator();
        TotalPriceDiscountCreationDto validObject = validObject();

        PriceDto NULL_PRICE = null;
        validObject.setFromPriceValue(NULL_PRICE);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenPriceHasInvalidValueInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        TotalPriceDiscountCreationValidator validator = new TotalPriceDiscountCreationValidator();
        TotalPriceDiscountCreationDto validObject = validObject();

        String INVALID_VALUE = "invalidValue";
        PriceDto price = new PriceDto(INVALID_VALUE, "USD");
        validObject.setFromPriceValue(price);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenPriceHasInvalidCurrencyInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        TotalPriceDiscountCreationValidator validator = new TotalPriceDiscountCreationValidator();
        TotalPriceDiscountCreationDto validObject = validObject();

        String INVALID_CURRENCY = "notCurrency";
        PriceDto price = new PriceDto("21.00", INVALID_CURRENCY);
        validObject.setFromPriceValue(price);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenDiscountIsNullInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        TotalPriceDiscountCreationValidator validator = new TotalPriceDiscountCreationValidator();
        TotalPriceDiscountCreationDto validObject = validObject();

        String NULL_DISCOUNT = null;
        validObject.setDiscount(NULL_DISCOUNT);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenDiscountIsBlankInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        TotalPriceDiscountCreationValidator validator = new TotalPriceDiscountCreationValidator();
        TotalPriceDiscountCreationDto validObject = validObject();

        String BLANK_DISCOUNT = "";
        validObject.setDiscount(BLANK_DISCOUNT);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenDiscountIsNotValidPercentageValueDuringValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        TotalPriceDiscountCreationValidator validator = new TotalPriceDiscountCreationValidator();
        TotalPriceDiscountCreationDto validObject = validObject();

        String NOT_VALID_PERCENTAGE_VALUE = "-999%";
        validObject.setDiscount(NOT_VALID_PERCENTAGE_VALUE);

        // Act
        validator.validate(validObject);
    }

    public TotalPriceDiscountCreationDto validObject() {
        return TotalPriceDiscountCreationDto.builder()
                .discount("10%")
                .fromPriceValue(new PriceDto("21.00", "USD"))
                .build();
    }


}
