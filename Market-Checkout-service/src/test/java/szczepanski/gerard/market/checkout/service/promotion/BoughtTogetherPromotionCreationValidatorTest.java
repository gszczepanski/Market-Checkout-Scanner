package szczepanski.gerard.market.checkout.service.promotion;

import org.apache.commons.lang3.StringUtils;
import org.mockito.Mockito;
import org.mockito.internal.util.collections.Sets;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.domain.item.Item;
import szczepanski.gerard.market.checkout.domain.item.ItemRepository;
import szczepanski.gerard.market.checkout.service.common.ValidationException;

import java.util.HashSet;
import java.util.Set;

import static org.mockito.Mockito.*;

@Test(suiteName = "unit-tests")
public class BoughtTogetherPromotionCreationValidatorTest {

    ItemRepository itemRepository;
    BoughtTogetherPromotionCreationValidator validator;

    @BeforeMethod
    public void beforeMethod() {
        itemRepository = mock(ItemRepository.class);
        validator = new BoughtTogetherPromotionCreationValidator(itemRepository);
    }

    @Test
    public void whenObjectForValidationIsValidThenDoNotThrowException() throws ValidationException {
        // Arrange
        BoughtTogetherPromotionCreationDto validObject = validObject();

        Set<Code> codes = Sets.newSet(Code.of("item-0001"), Code.of("item-0002"));
        Set<Item> foundItems = Sets.newSet(
                Item.builder().code(Code.of("item-0001")).build(),
                Item.builder().code(Code.of("item-0002")).build()
        );
        when(itemRepository.findAllForCodes(codes)).thenReturn(foundItems);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenNameIsNullInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        BoughtTogetherPromotionCreationDto validObject = validObject();

        Set<Code> codes = Sets.newSet(Code.of("item-0001"), Code.of("item-0002"));
        Set<Item> foundItems = Sets.newSet(
                Item.builder().code(Code.of("item-0001")).build(),
                Item.builder().code(Code.of("item-0002")).build()
        );
        when(itemRepository.findAllForCodes(codes)).thenReturn(foundItems);

        String NULL_NAME = "";
        validObject.setName(NULL_NAME);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenNameIsBlankInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        BoughtTogetherPromotionCreationDto validObject = validObject();

        Set<Code> codes = Sets.newSet(Code.of("item-0001"), Code.of("item-0002"));
        Set<Item> foundItems = Sets.newSet(
                Item.builder().code(Code.of("item-0001")).build(),
                Item.builder().code(Code.of("item-0002")).build()
        );
        when(itemRepository.findAllForCodes(codes)).thenReturn(foundItems);

        String BLANK_NAME = "";
        validObject.setName(BLANK_NAME);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenNameIsTooLongInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        BoughtTogetherPromotionCreationDto validObject = validObject();

        Set<Code> codes = Sets.newSet(Code.of("item-0001"), Code.of("item-0002"));
        Set<Item> foundItems = Sets.newSet(
                Item.builder().code(Code.of("item-0001")).build(),
                Item.builder().code(Code.of("item-0002")).build()
        );
        when(itemRepository.findAllForCodes(codes)).thenReturn(foundItems);

        int exceededLenght = Name.MAX_LENGTH + 1;
        String TOO_LONG_NAME = StringUtils.repeat('a', exceededLenght);
        validObject.setName(TOO_LONG_NAME);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenDiscountIsNullInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        BoughtTogetherPromotionCreationDto validObject = validObject();

        Set<Code> codes = Sets.newSet(Code.of("item-0001"), Code.of("item-0002"));
        Set<Item> foundItems = Sets.newSet(
                Item.builder().code(Code.of("item-0001")).build(),
                Item.builder().code(Code.of("item-0002")).build()
        );
        when(itemRepository.findAllForCodes(codes)).thenReturn(foundItems);

        String NULL_DISCOUNT = "";
        validObject.setDiscount(NULL_DISCOUNT);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenDiscountIsBlankInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        BoughtTogetherPromotionCreationDto validObject = validObject();

        Set<Code> codes = Sets.newSet(Code.of("item-0001"), Code.of("item-0002"));
        Set<Item> foundItems = Sets.newSet(
                Item.builder().code(Code.of("item-0001")).build(),
                Item.builder().code(Code.of("item-0002")).build()
        );
        when(itemRepository.findAllForCodes(codes)).thenReturn(foundItems);

        String BLANK_DISCOUNT = "";
        validObject.setDiscount(BLANK_DISCOUNT);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenDiscountIsNotValidPercentageValueDuringValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        BoughtTogetherPromotionCreationDto validObject = validObject();

        Set<Code> codes = Sets.newSet(Code.of("item-0001"), Code.of("item-0002"));
        Set<Item> foundItems = Sets.newSet(
                Item.builder().code(Code.of("item-0001")).build(),
                Item.builder().code(Code.of("item-0002")).build()
        );
        when(itemRepository.findAllForCodes(codes)).thenReturn(foundItems);

        String NOT_VALID_PERCENTAGE_VALUE = "-999%";
        validObject.setDiscount(NOT_VALID_PERCENTAGE_VALUE);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenRequiredItemsSetIsNullDuringValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        BoughtTogetherPromotionCreationDto validObject = validObject();

        Set<Code> codes = Sets.newSet(Code.of("item-0001"), Code.of("item-0002"));
        Set<Item> foundItems = Sets.newSet(
                Item.builder().code(Code.of("item-0001")).build(),
                Item.builder().code(Code.of("item-0002")).build()
        );
        when(itemRepository.findAllForCodes(codes)).thenReturn(foundItems);

        Set<String> NULL_ITEM_CODES = null;
        validObject.setRequiredItemCodes(NULL_ITEM_CODES);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenRequiredItemsSetIsEmptyDuringValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        BoughtTogetherPromotionCreationDto validObject = validObject();

        Set<Code> codes = Sets.newSet(Code.of("item-0001"), Code.of("item-0002"));
        Set<Item> foundItems = Sets.newSet(
                Item.builder().code(Code.of("item-0001")).build(),
                Item.builder().code(Code.of("item-0002")).build()
        );
        when(itemRepository.findAllForCodes(codes)).thenReturn(foundItems);

        Set<String> EMPTY_ITEM_CODES = new HashSet<>();
        validObject.setRequiredItemCodes(EMPTY_ITEM_CODES);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenRequiredItemsSetIsToSmallDuringValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        BoughtTogetherPromotionCreationDto validObject = validObject();

        Set<Code> codes = Sets.newSet(Code.of("item-0001"), Code.of("item-0002"));
        Set<Item> foundItems = Sets.newSet(
                Item.builder().code(Code.of("item-0001")).build(),
                Item.builder().code(Code.of("item-0002")).build()
        );
        when(itemRepository.findAllForCodes(codes)).thenReturn(foundItems);

        Set<String> TOO_SMALL_ITEM_CODES_SET = Sets.newSet("item-0001");
        validObject.setRequiredItemCodes(TOO_SMALL_ITEM_CODES_SET);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenAnyOFRequiredItemsCodesIsNotfoundInDbDuringValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        BoughtTogetherPromotionCreationDto validObject = validObject();

        Set<Code> codes = Sets.newSet(Code.of("item-0001"), Code.of("item-0002"));
        Set<Item> NOT_ALL_ITEMS_FOUND = Sets.newSet(
                Item.builder().code(Code.of("item-0001")).build()
        );
        when(itemRepository.findAllForCodes(codes)).thenReturn(NOT_ALL_ITEMS_FOUND);

        // Act
        validator.validate(validObject);
    }

    public BoughtTogetherPromotionCreationDto validObject() {
        return BoughtTogetherPromotionCreationDto.builder()
                .name("Bought juice with milk with 10% discount")
                .discount("10%")
                .requiredItemCodes(Sets.newSet("item-0001", "item-0002"))
                .build();
    }

}
