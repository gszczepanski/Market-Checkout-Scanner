package szczepanski.gerard.market.checkout.service.item;

import org.apache.commons.lang3.StringUtils;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.domain.common.Name;
import szczepanski.gerard.market.checkout.service.common.PriceDto;
import szczepanski.gerard.market.checkout.service.common.ValidationException;

@Test(suiteName = "unit-tests")
public class ItemCreationValidatorTest {

    @Test
    public void whenObjectForValidationIsValidThenDoNotThrowException() throws ValidationException {
        // Arrange
        ItemCreationValidator validator = new ItemCreationValidator();
        ItemCreationDto validObject = validObject();

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenNameIsNullInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        ItemCreationValidator validator = new ItemCreationValidator();
        ItemCreationDto validObject = validObject();

        String NULL_NAME = null;
        validObject.setName(NULL_NAME);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenNameIsBlankInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        ItemCreationValidator validator = new ItemCreationValidator();
        ItemCreationDto validObject = validObject();

        String BLANK_NAME = "";
        validObject.setName(BLANK_NAME);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenNameIsTooLongInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        ItemCreationValidator validator = new ItemCreationValidator();
        ItemCreationDto validObject = validObject();

        int exceededLenght = Name.MAX_LENGTH + 1;
        String TOO_LONG_NAME = StringUtils.repeat('a', exceededLenght);
        validObject.setName(TOO_LONG_NAME);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenPriceIsNullInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        ItemCreationValidator validator = new ItemCreationValidator();
        ItemCreationDto validObject = validObject();

        PriceDto NULL_PRICE = null;
        validObject.setPrice(NULL_PRICE);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenPriceHasInvalidValueInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        ItemCreationValidator validator = new ItemCreationValidator();
        ItemCreationDto validObject = validObject();

        String INVALID_VALUE = "invalidValue";
        PriceDto price = new PriceDto(INVALID_VALUE, "USD");
        validObject.setPrice(price);

        // Act
        validator.validate(validObject);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenPriceHasInvalidCurrencyInValidatedObjectThrowValidatorException() throws ValidationException {
        // Arrange
        ItemCreationValidator validator = new ItemCreationValidator();
        ItemCreationDto validObject = validObject();

        String INVALID_CURRENCY = "notCurrency";
        PriceDto price = new PriceDto("21.00", INVALID_CURRENCY);
        validObject.setPrice(price);

        // Act
        validator.validate(validObject);
    }

    public ItemCreationDto validObject() {
        return ItemCreationDto.builder()
                .name("Milk")
                .price(new PriceDto("21.00", "USD"))
                .build();
    }

}
