package szczepanski.gerard.market.checkout.service.checkout;

import org.mockito.Mockito;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;
import szczepanski.gerard.market.checkout.domain.basket.BasketFactory;
import szczepanski.gerard.market.checkout.domain.basket.BasketRepository;
import szczepanski.gerard.market.checkout.domain.item.ItemRepository;
import szczepanski.gerard.market.checkout.domain.warehouse.WarehouseUpdater;

import static org.mockito.Mockito.*;

@Test(suiteName = "unit-tests")
public class CheckoutServiceImplTest {

    BasketFactory basketFactory;
    BasketRepository basketRepository;
    ItemRepository itemRepository;
    WarehouseUpdater warehouseUpdater;
    CheckoutService checkoutService;

    @BeforeMethod
    public void beforeMethod() {
        basketFactory = mock(BasketFactory.class);
        basketRepository = mock(BasketRepository.class);
        itemRepository = mock(ItemRepository.class);
        warehouseUpdater = mock(WarehouseUpdater.class);

        checkoutService = new CheckoutServiceImpl(basketFactory, basketRepository, itemRepository, warehouseUpdater);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenScannedItemCodeIsNullThenThrowParamAssertionException() {
        // Arrange
        Integer checkoutNumber = 1;
        checkoutService.openCheckout(checkoutNumber);
        String NULL_ITEM_CODE = null;

        // Act
        checkoutService.scan(checkoutNumber, NULL_ITEM_CODE);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenScannedItemCodeIsBlankThenThrowParamAssertionException() {
        // Arrange
        Integer checkoutNumber = 1;
        checkoutService.openCheckout(checkoutNumber);
        String BLANK_ITEM_CODE = "";

        // Act
        checkoutService.scan(checkoutNumber, BLANK_ITEM_CODE);
    }

}
