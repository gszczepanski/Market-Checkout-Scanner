package szczepanski.gerard.market.checkout.service.promotion;

import org.assertj.core.api.Assertions;
import org.mockito.Mockito;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import szczepanski.gerard.market.checkout.component.common.assertion.ParamAssertionException;
import szczepanski.gerard.market.checkout.domain.common.Code;
import szczepanski.gerard.market.checkout.domain.money.Currency;
import szczepanski.gerard.market.checkout.domain.money.MonetaryValue;
import szczepanski.gerard.market.checkout.domain.promotion.Discount;
import szczepanski.gerard.market.checkout.domain.promotion.DiscountFromTotalBasketPrice;
import szczepanski.gerard.market.checkout.domain.promotion.DiscountFromTotalBasketPriceFactory;
import szczepanski.gerard.market.checkout.domain.promotion.DiscountFromTotalBasketPriceRepository;
import szczepanski.gerard.market.checkout.service.common.PriceDto;
import szczepanski.gerard.market.checkout.service.common.ValidationException;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.Mockito.*;

@Test(suiteName = "unit-tests")
public class TotalPriceDiscountServiceImplTest {

    DiscountFromTotalBasketPriceFactory discountFromTotalCheckoutPriceFactory;
    DiscountFromTotalBasketPriceRepository discountFromTotalCheckoutPriceRepository;
    TotalPriceDiscountCreationValidator totalPriceDiscountCreationValidator;
    TotalPriceDiscountService totalPriceDiscountService;

    @BeforeMethod
    public void beforeMethod() {
        discountFromTotalCheckoutPriceFactory = mock(DiscountFromTotalBasketPriceFactory.class);
        discountFromTotalCheckoutPriceRepository = mock(DiscountFromTotalBasketPriceRepository.class);
        totalPriceDiscountCreationValidator = mock(TotalPriceDiscountCreationValidator.class);

        totalPriceDiscountService = new TotalPriceDiscountServiceImpl(discountFromTotalCheckoutPriceFactory, discountFromTotalCheckoutPriceRepository, totalPriceDiscountCreationValidator);
    }

    @Test
    public void whenDiscountIsValidDuringAddDiscountThenDiscountIsCreatedAndSavedIntoDb() throws ValidationException {
        // Arrange
        String rawDiscount = "10%";
        Discount discount = Discount.of(rawDiscount);
        MonetaryValue fromCheckoutPrice = MonetaryValue.of("16.99", Currency.USD);

        TotalPriceDiscountCreationDto creationDto = TotalPriceDiscountCreationDto.builder()
                .discount(rawDiscount)
                .fromPriceValue(new PriceDto("16.99", "USD"))
                .build();

        String expectedCreatedDiscountCode = "dftp-0999";
        DiscountFromTotalBasketPrice discountFromTotalPrice = DiscountFromTotalBasketPrice.builder()
                .code(Code.of(expectedCreatedDiscountCode))
                .build();

        when(discountFromTotalCheckoutPriceFactory.createTotalDiscount(fromCheckoutPrice, discount)).thenReturn(discountFromTotalPrice);

        // Act
        String code = totalPriceDiscountService.addCheckoutTotalPriceDiscount(creationDto);

        // Assert
        assertThat(code).isEqualTo(expectedCreatedDiscountCode);

        verify(totalPriceDiscountCreationValidator).validate(creationDto);
        verify(discountFromTotalCheckoutPriceFactory).createTotalDiscount(fromCheckoutPrice, discount);
        verify(discountFromTotalCheckoutPriceRepository).save(discountFromTotalPrice);
    }

    @Test(expectedExceptions = ValidationException.class)
    public void whenDtoIsInvalidDuringAddDiscountThenThrowValidationException() throws ValidationException {
        // Arrange
        TotalPriceDiscountCreationDto invalidCreationDto = TotalPriceDiscountCreationDto.builder()
                .build();

        doThrow(ValidationException.class).when(totalPriceDiscountCreationValidator).validate(invalidCreationDto);

        // Act
        totalPriceDiscountService.addCheckoutTotalPriceDiscount(invalidCreationDto);
    }

    @Test(expectedExceptions = ParamAssertionException.class)
    public void whenCreationDtoIsNullDuringAddDiscountThenThrowParamAssertionException() throws ValidationException {
        // Arrange
        TotalPriceDiscountCreationDto NULL_CREATION_DTO = null;

        // Act
        totalPriceDiscountService.addCheckoutTotalPriceDiscount(NULL_CREATION_DTO);
    }


}
